package com.smartspot.font;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;


/**
 * Created by iyyapparajr on 8/31/2017.
 */

@SuppressLint("AppCompatCustomView")
public class ProximaNovaEdit extends EditText {

    public ProximaNovaEdit(Context context) {
        super(context);
        setTypeface(ProximaNova.getInstance(context).getTypeFace());
    }

    public ProximaNovaEdit(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(ProximaNova.getInstance(context).getTypeFace());
    }

    public ProximaNovaEdit(Context context, AttributeSet attrs,
                     int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(ProximaNova.getInstance(context).getTypeFace());
    }

}