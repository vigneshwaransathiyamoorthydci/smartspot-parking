package com.smartspot.widget;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by kirubakaranj on 10/28/2017.
 */

public class volleyrequestforstring extends Request<String> {
  //  private Listener<JSONObject> listener;
    private Response.Listener<String> listener;
    private Map<String, String> params;
    public volleyrequestforstring(int method, String url, Map<String, String> params, Response.Listener<String> reponseListener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
    }

    @Override
    protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
        return params;
    }

    @Override
    protected void deliverResponse(String response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            Log.e("Response calling", "calling response");

            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.e("error", jsonString);
           // JSONObject jsonObject = new JSONObject(jsonString);
            //jsonObject.put("status_code", response.statusCode);
            Log.e("statuscode", "" + response.statusCode);
            return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (Exception je) {
            return Response.error(new ParseError(je));
        }
    }
}
