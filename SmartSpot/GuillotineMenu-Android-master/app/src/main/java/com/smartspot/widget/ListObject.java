package com.smartspot.widget;

/**
 * Created by iyyapparajr on 8/31/2017.
 */

public class ListObject {

    String company;
    String sub;
    String CityNameObj;
    String city_idObj;
    String LocationNameObj;
    String Location_idObj;
    String Apply_Hourly_Charges;
    String Hourly_Charges;

    public String getApply_Hourly_Charges() {
        return Apply_Hourly_Charges;
    }

    public void setApply_Hourly_Charges(String apply_Hourly_Charges) {
        Apply_Hourly_Charges = apply_Hourly_Charges;
    }

    public String getHourly_Charges() {
        return Hourly_Charges;
    }

    public void setHourly_Charges(String hourly_Charges) {
        Hourly_Charges = hourly_Charges;
    }

    public String getLocation_timezone() {
        return location_timezone;
    }

    public void setLocation_timezone(String location_timezone) {
        this.location_timezone = location_timezone;
    }

    String location_timezone;
    int image_id;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public void setAll( String title) {

        setCompany(title);

    }



    public String getCityName()
    {
        return CityNameObj;
    }
    public void setCityName(String cityName)
    {
        CityNameObj=cityName;
    }



    public String getCity_id() {
        return city_idObj;
    }

    public void setCity_id(String category_id) {
        this.city_idObj = category_id;
    }



    public String getLocationName()
    {
        return LocationNameObj;
    }
    public void setLocationName(String cityName)
    {
        LocationNameObj=cityName;
    }


    public String getLocation_id() {
        return Location_idObj;
    }

    public void setLocation_id(String category_id) {
        this.Location_idObj = category_id;
    }


}