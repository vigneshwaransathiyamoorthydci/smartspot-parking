package com.smartspot.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartspot.R;
import com.smartspot.widget.BookingPojo;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 * Created by iyyapparajr on 12/26/2017.
 */

public class SavedCardListAdapter extends BaseAdapter {
    public View view;
    public int currPosition = 0;
    Context context;

    int layoutId;
    ProgressDialog pd;
    Holder holder;
    ArrayList<NameValuePair> skills_nv = new ArrayList<NameValuePair>();
    ArrayList<CardPojo> plan_list_adap = new ArrayList<CardPojo>();

    public SavedCardListAdapter(Context context, int textViewResourceId,
                                ArrayList<CardPojo> list) {

        System.out.println("Valuesin adapter if contructor");
        this.context = context;
        plan_list_adap = list;
        layoutId = textViewResourceId;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }


    @Override
    public int getCount() {
        return plan_list_adap.size();
    }

    @Override
    public CardPojo getItem(int position) {
        return plan_list_adap.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        LinearLayout layout;
//        layout = (LinearLayout) View.inflate(context, layoutId, null);
//        holder = new Holder();
        System.out.println("Valuesin adapter convertview");
        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.cardlist_item, parent, false);
            holder = new Holder();

            holder.card_type=(TextView)convertView.findViewById(R.id.card_type);
            holder.card_no=(TextView)convertView.findViewById(R.id.card_no);
        //    holder.date_ans=(TextView)convertView.findViewById(R.id.date);
            convertView.setTag(holder);
//            holder.amount.setText(plan_list_adap.get(position).getWalletMoney());
            holder.card_type.setText(plan_list_adap.get(position).getPayMethod());
            holder.card_no.setText("XXXX-XXXX-XXXX-"+plan_list_adap.get(position).getCardno());

            System.out.println("Values in adapter if section"+plan_list_adap.get(position).getHoldername());
            Log.e("Values ","Wallet"+plan_list_adap.get(position).getMonth()+""+plan_list_adap.get(position).getYear()+""+plan_list_adap.get(position).getCardno());


        } else {
            convertView = (RelativeLayout) convertView;
            holder = (Holder) convertView.getTag();
        }

      /*  holder.amount.setText("Aed "+plan_list_adap.get(position).getWalletMoney());
        holder.status.setText(plan_list_adap.get(position).getWallwallet_type());
        holder.date_ans.setText(plan_list_adap.get(position).getWalletDAte());*/
       // Log.e("Wallet","Status"+plan_list_adap.get(position).getwalletStatus());
        return convertView;
    }

    private class Holder {

        TextView   card_type,card_no,date_ans;

    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return plan_list_adap.indexOf(getItem(position));
    }

}
