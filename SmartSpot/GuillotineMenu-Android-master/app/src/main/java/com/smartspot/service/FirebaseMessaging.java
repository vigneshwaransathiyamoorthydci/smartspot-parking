package com.smartspot.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.google.firebase.messaging.RemoteMessage;
import com.smartspot.R;
import com.smartspot.activity.MainActivity;

import java.util.Date;

/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseMessaging extends com.google.firebase.messaging.FirebaseMessagingService {

    Intent intent;
    String navID, typeID;
    private NotificationManager notificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate icon_notification
        sendNotification(remoteMessage);
        Log.d("FirebaseMessaging", "Message data payload: " + remoteMessage.getData());


    }

    private void sendNotification(RemoteMessage remoteMessage) {


        String channelId = "SMART_SPOT_CHANNEL";
        String channelName = "smartsport Notification";
        notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.smartspot_logo);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        intent=new Intent( this , MainActivity. class );
        intent.putExtra("type",remoteMessage.getData().get("title"));
        intent.putExtra("navid",remoteMessage.getData().get("message"));
        intent.putExtra("fromnotification",true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent =  PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.drawable.smartspot_logo)
                .setContentTitle(remoteMessage.getData().get("title") != null ? remoteMessage.getData().get("title") : "")
                .setContentText(remoteMessage.getData().get("message") != null ? remoteMessage.getData().get("message") : "")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);
        int notificationID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(notificationID, notificationBuilder.build());





    }


    @Override
    public void onCreate() {
        super.onCreate();

    }
}
