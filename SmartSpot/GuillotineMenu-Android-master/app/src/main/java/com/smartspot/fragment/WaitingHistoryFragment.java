package com.smartspot.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smartspot.R;
import com.smartspot.adapter.WaitingHistoryAdapter;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.BookingPojo;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class WaitingHistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String booking_id,booking_time,booking_location_id,booking_city_id,booking_location_name,
            booking_city_name;
    String booking_waiting_until;
    WaitingHistoryAdapter reservation_history_adapter;
    ListView reservation_list_view;
    TextView emptyTExt;
    ArrayList<BookingPojo> BookingAray=new ArrayList<>();
    WS_CallService service_ReservationHistory;
    ArrayList<NameValuePair> ReservationHistory_NVP=new ArrayList<>();
    ReservationHistory_WS ReservationHistory_WebService;

    private OnFragmentInteractionListener mListener;

    public WaitingHistoryFragment() {
        // Required empty public constructor
    }
    // TODO: Rename and change types and number of parameters
    public static WaitingHistoryFragment newInstance(String param1, String param2) {
        WaitingHistoryFragment fragment = new WaitingHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.reservation_list, container, false);

        reservation_list_view=(ListView)view.findViewById(R.id.reservation_list_view);

        emptyTExt=(TextView)view.findViewById(R.id.emptyTExt);
        AppPreferences.putStringIntoStore(Preference_Constants.CURRENT_FRAGMENT,"3");
        if(isInternetOn())
        hitReservationHistory();


//        id_one=(TextView)view.findViewById(R.id.id_one);
//        id_one.setTextColor(Color.parseColor("#ffffff"));
//
//
//        amount_four=(TextView)view.findViewById(R.id.amount_four);
//        amount_one=(TextView)view.findViewById(R.id.amount_one);
//        amount_two=(TextView)view.findViewById(R.id.amount_two);
//        amount_three=(TextView)view.findViewById(R.id.amount_three);
//
//
//        amount_four.setText(R.string.rupee_symbol);
//        amount_one.setText(R.string.rupee_symbol);
//        amount_two.setText(R.string.rupee_symbol);amount_three.setText(R.string.rupee_symbol);


        return view;



    }

    private void hitReservationHistory() {

        ReservationHistory_NVP=new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:waitingHistory|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println("Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_ReservationHistory = new WS_CallService(getActivity());
            ReservationHistory_NVP.add(new BasicNameValuePair("WS", base64_register));
            ReservationHistory_WebService = new ReservationHistory_WS(getActivity(), ReservationHistory_NVP);
            ReservationHistory_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    public class ReservationHistory_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;
        Dialog dialog;
        Context context_aact;
        String booking_barrier_name,booking_barrier_id;

        public ReservationHistory_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();



//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_ReservationHistory = new WS_CallService(context_aact);
                jsonResponseString = service_ReservationHistory.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            BookingAray.clear();

            try {
                dialog.dismiss();
            } catch (Exception e) {

            }

            if(jsonResponse!=null) {
                System.out.println("POST EXECUTE");
                Log.e("Reservation", "Response" + jsonResponse);
                try {
                    JSONObject jObj = new JSONObject(jsonResponse);


                    String status = jObj.getString("Status");
                    if (status.toString().equalsIgnoreCase("Success")) {
                        JSONArray ReservJson = jObj.getJSONArray("response");


                        for (int i = 0; i < ReservJson.length(); i++) {


                            BookingPojo bookingPojo = new BookingPojo();

                            booking_id = jObj.getJSONArray("response").getJSONObject(i).getString("waiting_id");
//                       booking_user_id=jObj.getJSONArray("response").getJSONObject(i).getString("booking_user_id");
                            booking_time = jObj.getJSONArray("response").getJSONObject(i).getString("waiting_time");
                            booking_location_id = jObj.getJSONArray("response").getJSONObject(i).getString("waiting_location_id");
                            booking_location_name = jObj.getJSONArray("response").getJSONObject(i).getString("waiting_location_name");

                            booking_city_id = jObj.getJSONArray("response").getJSONObject(i).getString("waiting_city_id");
                            booking_city_name = jObj.getJSONArray("response").getJSONObject(i).getString("waiting_city_name");
                            booking_waiting_until = jObj.getJSONArray("response").getJSONObject(i).getString("waiting_until");

                            // booking_reservation_code=jObj.getJSONArray("response").getJSONObject(i).getString("booking_reservation_code");


                            bookingPojo.setBookingId(booking_id);
                            bookingPojo.setBookingTime(booking_time);
                            bookingPojo.setBookingLocationId(booking_location_id);
                            bookingPojo.setBookingLocationName(booking_location_name);
                            bookingPojo.setBookingCityName(booking_city_name);
                            bookingPojo.setBookingCityId(booking_city_id);

                            bookingPojo.setW_booking_id(booking_id);
                            bookingPojo.setW_booking_time(booking_time);
                            bookingPojo.setW_booking_location_id(booking_location_id);
                            bookingPojo.setW_booking_location_name(booking_location_name);
                            bookingPojo.setW_booking_city_name(booking_city_name);
                            bookingPojo.setW_booking_city_id(booking_city_id);
                            bookingPojo.setBooking_waiting_until(booking_waiting_until);


                            BookingAray.add(bookingPojo);

                        }
                        System.out.println(Preference_Constants.PREF_KEY_Service + "Size of an adapter ==>" + BookingAray.size());

                        Log.e("adaBookied", "slot Id" + booking_barrier_id);
                        Log.e("adaBookied", "Barrier Name" + booking_barrier_name);
                        reservation_history_adapter = new WaitingHistoryAdapter(context_aact, R.layout.reservaqtion_history_item, BookingAray);
                        reservation_list_view.setAdapter(reservation_history_adapter);


                        System.out.println("success");
                    } else {
                        System.out.println("failed");
                        String Message = jObj.getString("Message");
                        emptyTExt.setVisibility(View.VISIBLE);
                        emptyTExt.setText(Message);
//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                    }


//                hitLocationService();


                } catch (Exception e) {

                    System.out.println(e.toString() + "zcx");

                    Toast.makeText(getActivity().getApplicationContext(), "Please try again later." , Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(getActivity().getApplicationContext(),"Please check your Data/WiFi connection.",Toast.LENGTH_LONG).show();
            }

        }

    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getActivity().getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }
}
