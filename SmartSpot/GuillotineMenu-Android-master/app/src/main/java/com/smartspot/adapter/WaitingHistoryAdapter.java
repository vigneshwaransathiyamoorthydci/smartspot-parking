package com.smartspot.adapter;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smartspot.App;
import com.smartspot.R;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.BookingPojo;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class WaitingHistoryAdapter extends BaseAdapter {
    public View view;
    public int currPosition = 0;
    Context context;

    int layoutId;
    ProgressDialog pd;
    Holder holder;
    ArrayList<NameValuePair> skills_nv = new ArrayList<NameValuePair>();
    ArrayList<BookingPojo> plan_list_adap = new ArrayList<BookingPojo>();


    WS_CallService OptOutService;
    ArrayList<NameValuePair> OptOutNVP=new ArrayList<>();
    OptOut_WS      OptOut_WebService;

    public WaitingHistoryAdapter(Context context, int textViewResourceId,
                                 ArrayList<BookingPojo> list) {

        System.out.println("Valuesin adapter if contructor");
        this.context = context;
        plan_list_adap = list;
        layoutId = textViewResourceId;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }


    @Override
    public int getCount() {
        return plan_list_adap.size();
    }

    @Override
    public BookingPojo getItem(int position) {
        return plan_list_adap.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        System.out.println("Valuesin adapter convertview");
        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.waiting_history_item, parent, false);
            holder = new Holder();


            holder.city_name = (TextView) convertView.findViewById(R.id.city_name);

            holder.date_ans = (TextView) convertView.findViewById(R.id.date_ans);
            holder.slot_id = (TextView) convertView.findViewById(R.id.slot_id);
            holder.payment_status = (TextView) convertView.findViewById(R.id.payment_status);
            holder.city_name_waiting=(TextView)convertView.findViewById(R.id.city_name_waiting);
            holder.optout_text=(TextView)convertView.findViewById(R.id.optout_text);

            holder.optout_text.setTag(position);

            System.out.println("Values in adapter if section" + plan_list_adap.get(position).getBookingSlotId());
            Log.e("Bookied", "slot Id" + plan_list_adap.get(position).getBookingSlotId());
            Log.e("Bookied", "Barrier Name" + plan_list_adap.get(position).getBookingBarrierName());
            convertView.setTag(holder);

        } else {
            convertView = (LinearLayout) convertView;
            holder = (Holder) convertView.getTag();

        }
        holder.slot_id.setText(": "+plan_list_adap.get(position).getW_booking_id());

        holder. payment_status.setText(": Pending");
        holder.city_name.setText(plan_list_adap.get(position).getW_booking_location_name());
        holder.date_ans.setText(": "+plan_list_adap.get(position).getW_booking_time());
        holder.city_name_waiting.setText(plan_list_adap.get(position).getW_booking_city_name());

        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                holder.optout_text,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();

        holder.optout_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ViewGroup vg=(ViewGroup)view.getParent();
                TextView optouttxt=(TextView)vg.findViewById(R.id.optout_text);

                int clickPosition=(int)optouttxt.getTag();
                Log.e("positionBookId","->"+plan_list_adap.get(clickPosition).getW_booking_id());
                Log.e("position","->"+clickPosition);


                hitOptOutService(plan_list_adap.get(clickPosition).getW_booking_city_id(), plan_list_adap.get(clickPosition).getW_booking_location_id(),plan_list_adap.get(clickPosition).getW_booking_id(),clickPosition);
            }
        });

        return convertView;
    }

    private class Holder {

        TextView city_name, optout_text, amount,  status, date_ans;
        TextView city_name_waiting, barrier_up;
        TextView slot_id, payment_status;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return plan_list_adap.indexOf(getItem(position));
    }




    public void hitOptOutService(String city_id, String location_id, String wait_id,int clickPos)
    {

        byte[] data;
        try {
//          String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:optOut|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)+"|city_id:"+city_id +"|location_id:"+location_id+"|waiting_id:"+wait_id;
            System.out.println("Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            OptOutService = new WS_CallService(context);
            OptOutNVP.add(new BasicNameValuePair("WS", base64_register));
            OptOut_WebService = new OptOut_WS(context, OptOutNVP,clickPos);
            OptOut_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public class OptOut_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Dialog dialog;
        Context context_aact;
        String booking_barrier_name,booking_barrier_id;
        int clickPosition;

        public OptOut_WS(Context context_ws, ArrayList<NameValuePair> loginws, int clickPosition) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;
            this.clickPosition=clickPosition;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(context_aact);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(context_aact)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();



//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                OptOutService = new WS_CallService(context_aact);
                jsonResponseString = OptOutService.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
            Log.e("Reservation", "Response" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("Status");

//                hitLocationService();

                if(status.equalsIgnoreCase("Success")){
                    plan_list_adap.remove(clickPosition);
                    notifyDataSetChanged();
                }




                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");

                Toast.makeText(context_aact,"ERROR: "+e,Toast.LENGTH_LONG).show();
            }

        }

    }


}