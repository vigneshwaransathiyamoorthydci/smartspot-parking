package com.smartspot.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class MontserratBlackEdit extends EditText {

    public MontserratBlackEdit(Context context) {
        super(context);
        setTypeface(MontserratBlack.getInstance(context).getTypeFace());
    }

    public MontserratBlackEdit(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(MontserratBlack.getInstance(context).getTypeFace());
    }

    public MontserratBlackEdit(Context context, AttributeSet attrs,
                           int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(MontserratBlack.getInstance(context).getTypeFace());
    }

}