package com.smartspot.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by iyyapparajr on 1/5/2018.
 */

public class MontserratCheck extends CheckBox {

    public MontserratCheck(Context context) {
        super(context);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

    public MontserratCheck(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

    public MontserratCheck(Context context, AttributeSet attrs,
                             int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

}