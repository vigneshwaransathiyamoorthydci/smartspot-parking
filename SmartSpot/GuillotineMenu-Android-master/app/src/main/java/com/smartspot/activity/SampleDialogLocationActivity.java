package com.smartspot.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.smartspot.R;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.ListObject;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by iyyapparajr on 9/12/2017.
 */

public class SampleDialogLocationActivity extends Activity {

    ListView dialogList;
    WS_CallService service_ReservationHistory;
    ArrayList<NameValuePair> ReservationHistory_NVP=new ArrayList<>();
    ReservationHistory_WS ReservationHistory_WebService;
    private ArrayList<ListObject>originalData = null;
    AutoCompleteTextView search_netwrok;
    String entered_text;
    ArrayList<ListObject> filteredData=new ArrayList<>();
    SearchableAdapter circle_list_item_adapter;
    ArrayList<ListObject> CityArrayList=new ArrayList<>();
    RelativeLayout search_layout;
    TextView empty_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.city_layout_dialog);


        dialogList=(ListView)findViewById(R.id.listview);
        hitReservationHistory();
        search_netwrok=(AutoCompleteTextView)findViewById(R.id.search_auto);
        filteredData=new ArrayList<ListObject>();
        empty_view=(TextView)findViewById(R.id.empty_view);
        search_layout=(RelativeLayout)findViewById(R.id.search_layout);

        search_netwrok.setHint("Search Location");
        search_netwrok.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
//                search_progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                entered_text=s.toString();
try {
    circle_list_item_adapter.getFilter().filter(entered_text);
}catch (Exception e){}
            }
        });




    }


    private void hitReservationHistory() {

        ReservationHistory_NVP=new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY);
            AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION);

            String login_string= "Function:SelectLocation|city_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY)+ "|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println("Fucntionals login request-->" + login_string);

            Log.e("Location","Id-->"+login_string);


            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_ReservationHistory = new WS_CallService(this);
            ReservationHistory_NVP.add(new BasicNameValuePair("WS", base64_register));
            ReservationHistory_WebService = new ReservationHistory_WS(this, ReservationHistory_NVP);
            ReservationHistory_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public class SearchableAdapter extends BaseAdapter implements Filterable {

        private LayoutInflater mInflater;
        ItemFilter mFilter = new ItemFilter();

        Context actContext;
        public SearchableAdapter(Context context, ArrayList<ListObject> data) {

            System.out.println("Constructor-->"+filteredData.size());
            this.actContext=context;
            originalData = data ;
            mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return filteredData.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            Holder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_item, null);

                holder = new Holder();

                holder.Group_firstName=(TextView)convertView.findViewById(R.id.textview);
                System.out.println("Values in adapter if section");

                convertView.setTag(holder);
            } else {

                holder = (Holder) convertView.getTag();
            }

            if (convertView == null) {

                System.out.println("ADADPTER DESC--->"+ filteredData.get(position).getCityName());

            } else {

                holder = (Holder) convertView.getTag();
            }



//            holder.  Group_firstName.setText(plan_list_adap.get(position).getGroupOwnerFirstName()+" "+plan_list_adap.get(position).getGroupOwnerLastName());
//            holder.Group_firstName.setText(filteredData.get(position).getGroupOwnerFirstName());
            holder.Group_firstName.setText(filteredData.get(position).getCityName());







            return convertView;
        }
        class ViewHolder {
            TextView text;
        }

        public Filter getFilter() {
            return mFilter;
        }

        private class Holder {



            TextView Group_firstName,Group_lastName,ownerEmail;
        }

        private class ItemFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final List<ListObject> list = originalData;

                int count = list.size();
                final ArrayList<ListObject> nlist = new ArrayList<>(count);

                ListObject filterableString ;

                for (int i = 0; i < count; i++) {
                    filterableString = list.get(i);
                    if (filterableString.getCityName().toLowerCase().contains(filterString)) {
                        nlist.add(filterableString);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (ArrayList<ListObject>) results.values;
                circle_list_item_adapter. notifyDataSetChanged();
            }

        }
    }


    public class ReservationHistory_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;
        Context context_aact;
        Dialog dialog;

        public ReservationHistory_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(SampleDialogLocationActivity.this);
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();
            dialog = new Dialog(SampleDialogLocationActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");

            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SampleDialogLocationActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_ReservationHistory = new WS_CallService(context_aact);
                jsonResponseString = service_ReservationHistory.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            try {
                dialog.dismiss();
            } catch (Exception e) {

            }

            System.out.println("POST EXECUTE");
            if(jsonResponse!=null) {
                Log.e("jsonResponse", "CITY LIST-->" + jsonResponse);
                try {
                    JSONObject jObj = new JSONObject(jsonResponse);

                    System.out.println(Preference_Constants.PREF_KEY_Service + "Values-->" + jsonResponse);

                    String status = jObj.getString("Status");
                    if (status.toString().equalsIgnoreCase("Success")) {

                        String futurebookAmt = "";
                        JSONArray ReservJson = jObj.getJSONArray("Location");
//                  JSONArray jObj1 = new JSONArray(jsonResponse);
                        String futureAftHrs = jObj.getString("$FutureBookingAfterHrs");
                        String currentGMT = jObj.getString("currentGMT");

                        if (jObj.has("futureBookingAmt"))
                            futurebookAmt = jObj.getString("futureBookingAmt");

                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_AFTERHOUR, futureAftHrs);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_CURRENT_GMT, currentGMT);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_BOOKING_PRICE, futurebookAmt);

                        Log.e("currentGmt", "->" + currentGMT);
                        Log.e("FutureAfterHrs", "->" + futureAftHrs);
                        Log.e("FuturebookAmt", "->" + futurebookAmt);


                        for (int i = 0; i < ReservJson.length(); i++) {
                            ListObject CityArrayListObj = new ListObject();

                            String city_id = jObj.getJSONArray("Location").getJSONObject(i).getString("location_id");
                            String city_name = jObj.getJSONArray("Location").getJSONObject(i).getString("location_name");
                            String location_timezone = jObj.getJSONArray("Location").getJSONObject(i).getString("location_timezone");
                            String Apply_Hourly_Charges=jObj.getJSONArray("Location").getJSONObject(i).getString("Apply_Hourly_Charges");
                            String Hourly_Charges=jObj.getJSONArray("Location").getJSONObject(i).getString("Hourly_Charges");
                            AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_LOCATION_TIMEZONE, location_timezone);
                            Log.e("Responselocation", "->" + city_id + " , " + city_name + ", " + location_timezone);
                            System.out.println("Response city list ==>" + city_id);

                            CityArrayListObj.setCity_id(city_id);
                            CityArrayListObj.setCityName(city_name);
                            CityArrayListObj.setLocation_timezone(location_timezone);
                            CityArrayListObj.setApply_Hourly_Charges(Apply_Hourly_Charges);
                            CityArrayListObj.setHourly_Charges(Hourly_Charges);


                            filteredData.add(CityArrayListObj);
                        }
                        //Collections.sort(filteredData, Comparator.comparing(ListObject::getCityName));
                        Collections.sort(filteredData, new Comparator<ListObject>() {
                            public int compare(ListObject v1, ListObject v2) {
                                return v1.getCityName().compareTo(v2.getCityName());
                            }
                        });
                        System.out.println(Preference_Constants.PREF_KEY_Service + "Size of an adapter ==>" + CityArrayList.size());

                        circle_list_item_adapter = new SearchableAdapter(context_aact, filteredData);
                        dialogList.setAdapter(circle_list_item_adapter);


                        dialogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                System.out.println(Preference_Constants.PREF_KEY_Service + "On item selected Listener==>" + filteredData.get(i).getCity_id());

                                System.out.println(Preference_Constants.PREF_KEY_Service + "On item selected Listener==>" + filteredData.get(i).getCityName());
                                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION, filteredData.get(i).getCity_id());
                                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME, filteredData.get(i).getCityName());

                                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges, filteredData.get(i).getApply_Hourly_Charges());
                                if (filteredData.get(i).getApply_Hourly_Charges().equals("1"))
                                {
                                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Hourly_Charges, filteredData.get(i).getHourly_Charges());
                                }
                                else
                                {
                                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Hourly_Charges, "3");
                                }





                                Intent broadcastIntent = new Intent("Closebackactivity");
//                broadcastIntent.setAction("CreateGroupFollower");
                                broadcastIntent.putExtra("Data", "fromReseller");
                                sendBroadcast(broadcastIntent);
                                MainActivity.fromcreategroup = true;
                                finish();



                            }
                        });

                        System.out.println("success");
                    } else {
                        System.out.println("failed");
                        String Message="";
                        if(jObj.has("Message")) {
                            Message = jObj.getString("Message");
                            Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                        }

                        search_layout.setVisibility(View.GONE);
                        dialogList.setVisibility(View.GONE);
                        empty_view.setVisibility(View.VISIBLE);
                        empty_view.setText(Message);

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                    }


//                hitLocationService();




                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),"Please try again later.",Toast.LENGTH_LONG).show();
                    System.out.println(e.toString() + "zcx");
                }
            }
            else{
                PopupMessage("Please check your Data/WiFi connection.");
            }
        }

    }


    public void PopupMessage(final String msgText){
        final Dialog dialog1 = new Dialog(SampleDialogLocationActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView text = (TextView) dialog1.findViewById(R.id.text);
        text.setText(""+msgText);
        text.setTextColor(Color.parseColor("#000000"));
        dialog1.show();
        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                finish();
            }
        });

    }

}