package com.smartspot.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class MontserratBoldText extends TextView {

    public MontserratBoldText(Context context) {
        super(context);
        setTypeface(MontserratBold.getInstance(context).getTypeFace());
    }

    public MontserratBoldText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(MontserratBold.getInstance(context).getTypeFace());
    }

    public MontserratBoldText(Context context, AttributeSet attrs,
                             int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(MontserratBold.getInstance(context).getTypeFace());
    }

}