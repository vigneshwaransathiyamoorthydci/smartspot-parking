package com.smartspot.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smartspot.R;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;

import java.util.regex.Pattern;


/**
 * Created by iyyapparajr on 11/7/2017.
 */

public class WalletActivity extends Activity {

    TextView payment_one,payment_two,payment_three,payment_four,balance_text,add_money_button;
ImageView add_money_back;
EditText passwordEdit, emailEdit;
LinearLayout email_layout;
    String cliking="empty";
    LinearLayout parentLayout;
    TextView payment_one_filled,payment_two_filed,payment_three_filed,payment_four_filled,available_text, email_txt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.add_money_layout);

        payment_one=(TextView)findViewById(R.id.payment_one);
        payment_two=(TextView)findViewById(R.id.payment_two);
        payment_three=(TextView)findViewById(R.id.payment_three);
        payment_four=(TextView)findViewById(R.id.payment_four);
        available_text=(TextView)findViewById(R.id.available_text);

        email_txt=(TextView)findViewById(R.id.email_text);
        emailEdit=(EditText)findViewById(R.id.email_edit);

        email_layout=(LinearLayout) findViewById(R.id.email_layout);
//        payment_one_filled=(TextView)findViewById(R.id.payment_one_filled);
//        payment_two_filed=(TextView)findViewById(R.id.payment_two_filled);
//        payment_three_filed=(TextView)findViewById(R.id.payment_three_filled);
//        payment_four_filled=(TextView)findViewById(R.id.payment_four_filled);


        if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL).equalsIgnoreCase("")){
            //Toast.makeText(getApplicationContext(),"Please update profile",Toast.LENGTH_LONG).show();
            email_layout.setVisibility(View.VISIBLE);
            email_txt.setVisibility(View.GONE);
        }else {
            email_txt.setText("Email : "+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL));
            email_layout.setVisibility(View.GONE);
            email_txt.setVisibility(View.VISIBLE);
        }

        String availbleAmount=getIntent().getStringExtra("AvailbaleAmount");

        available_text.setText("Available Amount : "+availbleAmount);

        passwordEdit=(EditText)findViewById(R.id.payment_amount);

        balance_text=(TextView)findViewById(R.id.amount);
        add_money_button=(TextView)findViewById(R.id.add_money_button);
        add_money_back=(ImageView)findViewById(R.id.add_money_back);

        add_money_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        balance_text.setText(" 9");

        payment_one.setText(" 10");
        payment_two.setText(" 25");
        payment_three.setText(" 50");
        payment_four.setText(" 100");

        LinearLayout parentLayout=(LinearLayout)findViewById(R.id.parentLayout);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
        alphaAnimation.setDuration(3000);
        alphaAnimation.setFillAfter(true);
        parentLayout.startAnimation(alphaAnimation);

        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                add_money_button,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();


        add_money_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if(emailEdit.getText().toString().equalsIgnoreCase("")){
                    Toast.makeText(getApplicationContext(),"Please update profile",Toast.LENGTH_LONG).show();
                }else {*/

                  String firstchar = "";
                  if (!passwordEdit.getText().toString().trim().isEmpty()) {
                      String cc = passwordEdit.getText().toString().trim();
                      firstchar = String.valueOf(cc.charAt(0));
                  }

                  if (passwordEdit.getText().toString().trim().isEmpty() || firstchar.equalsIgnoreCase("0")) {
                      Toast.makeText(getApplicationContext(), "Please enter or select amount", Toast.LENGTH_LONG).show();
                  } else {

                      if(Validation()) {
                      Intent PaymentIntent = new Intent(WalletActivity.this, PaymentActivity.class);
                      PaymentIntent.putExtra("PaymentMoney", passwordEdit.getText().toString().trim());
                      if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL).equalsIgnoreCase("")){
                          PaymentIntent.putExtra("Email", emailEdit.getText().toString().trim());
                      }else {
                          PaymentIntent.putExtra("Email", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL));
                      }

                      startActivity(PaymentIntent);
//                    finish();
                  }
              }
              //  }
            }
        });

        payment_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Clicnking ","color"+cliking);

                passwordEdit.setSelection(passwordEdit.getText().length());

                passwordEdit.setText(" 10");
                passwordEdit.setTextSize(getResources().getDimension(R.dimen.text_size_13));
                passwordEdit.setSelection(passwordEdit.getText().length());
                payment_one.setBackgroundResource(R.drawable.payment_border_filled);


                payment_three.setBackgroundResource(R.drawable.payment_border);
                payment_four.setBackgroundResource(R.drawable.payment_border);
                payment_two.setBackgroundResource(R.drawable.payment_border);
//                payment_one.setBackgroundColor(Color.parseColor("#51d1d6"));
                payment_one.setPadding(25,20,20,25);
                payment_two.setPadding(25,20,20,25);
                payment_three.setPadding(25,20,20,25);
                payment_four.setPadding(25,20,20,25);
                payment_one.setTextColor(getResources().getColor(R.color.app_black));

            }
        });

        payment_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordEdit.setText(" 25");
                passwordEdit.setTextSize(getResources().getDimension(R.dimen.text_size_13));
                passwordEdit.setSelection(passwordEdit.getText().length());
//                payment_two.setBackgroundColor(Color.parseColor("#51d1d6"));


                payment_one.setBackgroundResource(R.drawable.payment_border);
                payment_three.setBackgroundResource(R.drawable.payment_border);
                payment_four.setBackgroundResource(R.drawable.payment_border);
                payment_two.setBackgroundResource(R.drawable.payment_border_filled);
                payment_one.setPadding(25,20,20,25);
                payment_two.setPadding(25,20,20,25);
                payment_three.setPadding(25,20,20,25);
                payment_four.setPadding(25,20,20,25);
            }
        });
        payment_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordEdit.setText(" 50");
                passwordEdit.setTextSize(getResources().getDimension(R.dimen.text_size_13));
                passwordEdit.setSelection(passwordEdit.getText().length());
                //payment_three.setBackgroundColor(Color.parseColor("#51d1d6"));


                payment_one.setBackgroundResource(R.drawable.payment_border);
                payment_three.setBackgroundResource(R.drawable.payment_border_filled);
                payment_four.setBackgroundResource(R.drawable.payment_border);
                payment_two.setBackgroundResource(R.drawable.payment_border);
                payment_one.setPadding(25,20,20,25);
                payment_two.setPadding(25,20,20,25);
                payment_three.setPadding(25,20,20,25);
                payment_four.setPadding(25,20,20,25);


            }
        });
        payment_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordEdit.setText(" 100" );
                passwordEdit.setTextSize(getResources().getDimension(R.dimen.text_size_13));
                passwordEdit.setSelection(passwordEdit.getText().length());
                payment_four.setBackgroundColor(Color.parseColor("#51d1d6"));

                payment_one.setBackgroundResource(R.drawable.payment_border);
                payment_three.setBackgroundResource(R.drawable.payment_border);
                payment_four.setBackgroundResource(R.drawable.payment_border_filled);
                payment_two.setBackgroundResource(R.drawable.payment_border);
                payment_one.setPadding(25,20,20,25);
                payment_two.setPadding(25,20,20,25);
                payment_three.setPadding(25,20,20,25);
                payment_four.setPadding(25,20,20,25);

            }
        });


    }


    public boolean Validation(){
        boolean check=true;
        int v=0;
        String error="";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailString = emailEdit.getText().toString().trim();

        if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL).equalsIgnoreCase("")) {
            if (!isValidEmaillId(emailString)) {
                error = "Enter a Valid Email";
                check = false;
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                return check;
            }
        }

        return check;
    }

    private boolean isValidEmaillId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }



}
