package com.smartspot.font;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by iyyapparajr on 8/31/2017.
 */

@SuppressLint("AppCompatCustomView")
public class RoximaNovaBold extends TextView {

    public RoximaNovaBold(Context context) {
        super(context);
        setTypeface(ProximaNovaBold.getInstance(context).getTypeFace());
    }

    public RoximaNovaBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(ProximaNovaBold.getInstance(context).getTypeFace());
    }

    public RoximaNovaBold(Context context, AttributeSet attrs,
                            int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(ProximaNovaBold.getInstance(context).getTypeFace());
    }

}