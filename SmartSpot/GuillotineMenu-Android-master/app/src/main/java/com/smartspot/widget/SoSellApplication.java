package com.smartspot.widget;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by manojg on 28-Jun-17.
 */

public class SoSellApplication extends Application {

    public final static boolean DEBUG_LOGGING = true;
    public final static boolean DEBUG_MODE = true;
    public final static String APP_STORE = "google";

    public static final String TAG = "SoSellApplication";
    private static SoSellApplication sInstance;
    private static String sVersion = null;
    private static int sVersionCode = 0;

    private static Activity activity;
   /* public static final String TAG = AppController.class
            .getSimpleName();
*/
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    public SoSellApplication() {
        sInstance = this;
    }

    public static synchronized SoSellApplication getInstance() {
        return sInstance;
    }

    /**
     * Helper method to setup the default strict mode if running 2.3+ and running in debug mode
     */


    public static String getVersion() {
        if (sVersion != null) {
            return sVersion;
        }
        try {
            SoSellApplication application = SoSellApplication.getInstance();
            ComponentName comp = new ComponentName(application, application.getClass());
            PackageInfo pinfo = application.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
            sVersion = pinfo.versionName;
            return sVersion;
        } catch (Exception ex) {
            Log.w(TAG, "Problem getting version name.");
            sVersion = "unknown";
            return sVersion;
        }
    }

    public static boolean needsUpgrade() {
        return false;
    }

    public static int getVersionCode() {
        if (sVersionCode != 0) {
            return sVersionCode;
        }
        try {
            SoSellApplication application = SoSellApplication.getInstance();
            ComponentName comp = new ComponentName(application, application.getClass());
            PackageInfo pinfo = application.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
            sVersionCode = pinfo.versionCode;
            return sVersionCode;
        } catch (Exception ex) {
            Log.w(TAG, "Problem getting version code.");
            sVersionCode = -1;
            return sVersionCode;
        }
    }

    public static void setCurrentActivity(Activity currentActivity) {
        activity = currentActivity;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        MultiDex.install(this);
       // setupStrictMode();
        init();
       // AppPreferences.setStoreContext(SoSellApplication.this);

    }

    private void init() {
        FragmentTransactionUtils.getInstance().setContext(this);
      //  AppPreferences.setStoreContext(getApplicationContext());
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(newBase);
    }

    @Override
    public void onLowMemory() {
        if (DEBUG_LOGGING) {
            Log.d(TAG, "LowMemory, clearing bitmap cache");
        }
        super.onLowMemory();
    }

    @TargetApi(14)
    @Override
    public void onTrimMemory(int level) {
        if (level != ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            if (DEBUG_LOGGING) {
                Log.d(TAG, "TrimMemory, clearing bitmap cache");
            }
        }
        super.onTrimMemory(level);
    }


   /* public static synchronized SoSellApplication getInstance() {
        return mInstance;
    }
*/
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }



    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
