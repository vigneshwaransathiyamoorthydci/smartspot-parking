package com.smartspot.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

import com.smartspot.R;
import com.google.firebase.FirebaseApp;
import com.smartspot.notification.Config;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;

import java.io.File;
import java.util.List;

/**
 * Created by iyyapparajr on 9/1/2017.
 */

public class SplashNewActivity extends Activity {

    private static int SPLASH_TIME_OUT = 3000;
//    TextView app_name_text;
    private SurfaceView mSurfaceView;
    private MediaPlayer mMediaPlayer;
    SurfaceHolder mFirstSurface;
    private View overlayScreen;
    private Uri mVideoUri;
    RelativeLayout loginscreenlayout;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_new_layout);
        FirebaseApp.initializeApp(this);
//        app_name_text=(TextView)findViewById(R.id.app_name_text);
//        Typeface font = Typeface.createFromAsset(getAssets(), "PROXIMANOVA_BOLD_WEBFONT.TTF");
//
//        app_name_text.setTypeface(font);
        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        overlayScreen = findViewById(R.id.scrollOverLay);
//        loadVideo();

      //  Log.d("DANIEL","EDU"+isRooted());


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    //  FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    //  Toast.makeText(getApplicationContext(),intent.getStringExtra("token"),Toast.LENGTH_LONG).show();



                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                  /*  String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
*/
                    //txtMessage.setText(message);
                }
            }
        };
//        app_name_textapp_name_text = (RoximaNovaBold) findViewById(R.id.app_name_text);


//        String text = "<font color=#ffffff>Smart</font><font color=#51d1d6>Spot</font>";
//        app_name_text.setText(Html.fromHtml(text));
        AppPreferences.putStringIntoStore("BAckPressed","null");

        loginscreenlayout = (RelativeLayout) findViewById(R.id.loginscreenlayout);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity


                if (AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID).isEmpty()) {

                    Intent i = new Intent(SplashNewActivity.this, SignUpLoginActivity.class);
//                  Intent i = new Intent(SplashNewActivity.this, SignUpActivity.class);
                    startActivity(i);
                    finish();

                } else {


                Intent i = new Intent(SplashNewActivity.this, MainActivity.class);
                    startActivity(i);

                    // close this activity
                    finish();
                }
//                app_name_text.setVisibility(View.GONE);
//                loginscreenlayout.setVisibility(View.VISIBLE);
//                loginscreenlayout.startAnimation(inFromRightAnimation());


            }
        }, SPLASH_TIME_OUT);



//        shared=getSharedPreferences("FCM_Shared",MODE_PRIVATE);
//        String fcmToken=   shared.getString(Preference_Constants.PREF_KEY_FCMTOKEN_NEW,"");

        AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
            Log.e("Stored","Value Splash"+AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW));

//        AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_FCMTOKEN);
//        Log.e("Stored","Value SplashOld"+AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN));



    }

    private Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }


    public static boolean isRooted() {

        // get from build info
        String buildTags = android.os.Build.TAGS;
        if (buildTags != null && buildTags.contains("test-keys")) {
            return true;
        }

        // check if /system/app/Superuser.apk is present
        try {
            File file = new File("/system/app/Superuser.apk");
            if (file.exists()) {
                return true;
            }
        } catch (Exception e1) {
            // ignore
        }

        // try executing commands
        return canExecuteCommand("/system/xbin/which su")
                || canExecuteCommand("/system/bin/which su") || canExecuteCommand("which su");
    }

    // executes a command on the system
    private static boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }
}
