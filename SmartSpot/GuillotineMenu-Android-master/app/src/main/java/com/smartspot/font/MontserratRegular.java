package com.smartspot.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class MontserratRegular extends TextView {

    public MontserratRegular(Context context) {
        super(context);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

    public MontserratRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

    public MontserratRegular(Context context, AttributeSet attrs,
                            int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

}