package com.smartspot.adapter;

/**
 * Created by iyyapparajr on 12/13/2017.
 */

public class CountryPojo {

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String code,id;
}
