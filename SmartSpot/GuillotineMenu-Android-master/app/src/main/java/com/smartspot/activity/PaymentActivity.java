package com.smartspot.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.checkout.CardValidator;
import com.checkout.CheckoutKit;
import com.checkout.exceptions.CardException;
import com.checkout.exceptions.CheckoutException;
import com.checkout.httpconnector.Response;
import com.checkout.models.Card;
import com.checkout.models.CardToken;
import com.checkout.models.CardTokenResponse;

import com.smartspot.R;
import com.smartspot.adapter.CardPojo;
import com.smartspot.adapter.CountryPojo;
import com.smartspot.adapter.SavedCardListAdapter;
import com.smartspot.adapter.Typeadapter;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.Ndspinner;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by iyyapparajr on 11/7/2017.
 */

public class PaymentActivity extends Activity {

    TextView buttonMain,  cvv,aed_txt;
    EditText name, numberField;
    RelativeLayout paylayout;
    private static int SPLASH_TIME_OUT = 3000;
    Dialog dialog;
    String getToken;

    WS_CallService service_IFMoneyInWallet;
    ArrayList<NameValuePair> IFMoneyInWallet_NVP = new ArrayList<>();
    Payment_WS IFMoneyInWallet_WebService;

    WS_CallService getCard_service;
    ArrayList<NameValuePair> getCard_NameValuepair = new ArrayList<>();
    GetCard_WS getCard_WS;

    String Amount,GetEmail="";

    String saveCard="0";

   // private String publicKey = "pk_test_eddfe922-9bd2-4877-ad3b-9db0acd4b435";
   private String publicKey = "pk_7b0fb574-55cb-4629-8e2c-e1cb3839763e";
    ImageView image_rotate;
    RelativeLayout payment_first_layout, payment_process_layout;

    WS_CallService service_Payment;
    ArrayList<NameValuePair> Paymet_NVP = new ArrayList<>();
    Payment_WS payment_WebService;
  //  Ndspinner selectCardSpin;
    Typeadapter typeadapter;
    ArrayList<CountryPojo> typeList=new ArrayList<>();

    LinearLayout save_cardclick;
    CheckBox save_card;
    ListView listView;
    SavedCardListAdapter listadapter;
    ArrayList<CardPojo> SelectedCardList=new ArrayList<>();
    String cardNo="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.credit_card_lay);

        buttonMain = (TextView) findViewById(R.id.buttonMain);
        save_cardclick = (LinearLayout) findViewById(R.id.save_cardclick);
        aed_txt= (TextView) findViewById(R.id.aed_txt);
        save_card= (CheckBox)findViewById(R.id.flatcheck) ;
       // paylayout = (RelativeLayout) findViewById(R.id.paylayout);
     //   selectCardSpin=(Ndspinner)findViewById(R.id.cardSpin);
        final Spinner spinnerM = (Spinner) findViewById(R.id.spinnerMonth);
        final Spinner spinnerY = (Spinner) findViewById(R.id.spinnerYear);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.months, R.layout.spinner_item_payment);
        adapter.setDropDownViewResource(R.layout.spinner_item_payment);
        spinnerM.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.years, R.layout.spinner_item_payment);
        adapter.setDropDownViewResource(R.layout.spinner_item_payment);
        spinnerY.setAdapter(adapter2);


        Amount = getIntent().getStringExtra("PaymentMoney");
        GetEmail=getIntent().getStringExtra("Email");

        payment_first_layout = (RelativeLayout) findViewById(R.id.payment_first_layout);
        payment_process_layout = (RelativeLayout) findViewById(R.id.payment_process_layout);
        image_rotate = (ImageView) findViewById(R.id.rotate_image);


        name = (EditText) findViewById(R.id.name);
        numberField = (EditText) findViewById(R.id.number);

        cvv = (TextView) findViewById(R.id.cvv);

        aed_txt.setText("You are now adding AED "+Amount+" to the SmartSpot Wallet.");

        numberField.addTextChangedListener(new FourDigitCardFormatWatcher());

        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                buttonMain,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();

        buttonMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(numberField.getText().toString().equalsIgnoreCase("")){

                }else {
                    cardNo="";
                    String[] items = numberField.getText().toString().split("-");
                    for (String item : items) {
                        cardNo=cardNo+item;

                    }
                }
                System.out.println("cardbno = " + cardNo);
//                dialog=new ProgressDialog(PaymentActivity.this);
//                dialog.setMessage("Processing");
//                dialog.show();
//
//                payment_first_layout.setVisibility(View.GONE);
//                payment_process_layout.setVisibility(View.VISIBLE);
//                RotateAnimation rotateAnimation = new RotateAnimation(0, 360f,
//                        Animation.RELATIVE_TO_SELF, 0.5f,
//                        Animation.RELATIVE_TO_SELF, 0.5f);
//
//                rotateAnimation.setInterpolator(new LinearInterpolator());
//                rotateAnimation.setDuration(900);
//                rotateAnimation.setRepeatCount(Animation.INFINITE);
//
//                findViewById(R.id.rotate_image).startAnimation(rotateAnimation);


                if (name.getText().toString().isEmpty() || cvv.getText().toString().isEmpty() || numberField.getText().toString().isEmpty() || spinnerM.getSelectedItem().toString().equalsIgnoreCase("MM") || spinnerY.getSelectedItem().toString().equalsIgnoreCase("YYYY")) {
                    Toast.makeText(getApplicationContext(), "Please enter all details", Toast.LENGTH_LONG).show();

                } else {

                    dialog = new Dialog(PaymentActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.custom_progress_layout);
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

                    Glide.with(PaymentActivity.this)
                            .load(R.drawable.loading)
                            .asGif()
                            .placeholder(R.drawable.loading)
                            .crossFade()
                            .into(image);
                    dialog.show();

                    if(save_card.isChecked()){
                        saveCard="1";
                    }else if (!save_card.isChecked()){
                        saveCard="0";
                    }

                    if(isInternetOn())
                        new ConnectionTask().execute("");

                }
//                new Handler().postDelayed(new Runnable() {
//
//
//                    @Override
//                    public void run() {
//
//                        finish();
//                    }
//                }, SPLASH_TIME_OUT);


            }
        });


       save_cardclick.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(isInternetOn())
                   GetCardListData();

           }
       });

     //  EditWatcher();


    }

    public void CardListPopup(String message){

        final Dialog dialog1 = new Dialog(PaymentActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.cardlist_popup);

        TextView messagetxt=(TextView)dialog1.findViewById(R.id.text);

        messagetxt.setText(""+message);
        dialog1.setCancelable(true);
        dialog1.setCanceledOnTouchOutside(true);

        dialog1.show();
        listView=(ListView)dialog1.findViewById(R.id.listview);


        listadapter = new SavedCardListAdapter(getApplicationContext(), R.layout.reservaqtion_history_item, SelectedCardList);
        listView.setAdapter(listadapter);

        ImageView close_icon=(ImageView)dialog1.findViewById(R.id.close);
        close_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ConfirmPayPopup("Are you sure want to pay?",SelectedCardList.get(i).getCard_id());
            }
        });

    }

    public void ConfirmPayPopup(String Message, final String cardid)
    {
        final Dialog dialog1 = new Dialog(PaymentActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.cvv_popup);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        final  EditText   cvv_edit=(EditText)dialog1.findViewById(R.id.cvvtxt);
        //text.setText(Message);
        cvv_edit.setTextColor(Color.parseColor("#000000"));
        dialog1.show();

        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        TextView  noPopup=(TextView)dialog1.findViewById(R.id.noPopup);
        noPopup.setVisibility(View.VISIBLE);
        noPopup.setText("CANCEL");
        okay_popup.setText("OK");
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(cvv_edit.getText().toString().equalsIgnoreCase("")|| cvv_edit.getText().toString().isEmpty()){
                  Toast.makeText(getApplicationContext(),"Invalid cvv number",Toast.LENGTH_LONG).show();
                }else {
                    dialog1.dismiss();

                    Intent PaymentIntent = new Intent(PaymentActivity.this, WalletProcessingActivity.class);
                    PaymentIntent.putExtra("PaymentToken", "");
                    PaymentIntent.putExtra("CardId", cardid);
                    PaymentIntent.putExtra("Email", GetEmail);
                    PaymentIntent.putExtra("PaymentAmount", Amount);
                    PaymentIntent.putExtra("card_number", "");
                    PaymentIntent.putExtra("card_name", "");
                    PaymentIntent.putExtra("card_expiry_month", "");
                    PaymentIntent.putExtra("card_expiry_year", "");
                    PaymentIntent.putExtra("card_cvv", "" + cvv_edit.getText().toString().trim());
                    PaymentIntent.putExtra("save_card", "0");


                    Log.e("Email", "->" + GetEmail);
                    Log.e("CardId", "->" + cardid);
                    Log.e("PaymentAmount", "->" + Amount);
                    Log.e("card_cvv", "->" + cvv_edit.getText().toString().trim());


                    startActivity(PaymentIntent);
                }

            }
        });

        noPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });

    }

    private void GetCardListData() {


        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:getCardList|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println(Preference_Constants.PREF_KEY_Service + " service  -->" + login_string);
            Log.e("Service","Context"+login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            getCard_service = new WS_CallService(PaymentActivity.this);
            getCard_NameValuepair.add(new BasicNameValuePair("WS", base64_register));
            getCard_WS = new GetCard_WS(PaymentActivity.this, getCard_NameValuepair);
            getCard_WS.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }





    public class GetCard_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

//        Dialog dialog;

        public GetCard_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(PaymentActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(PaymentActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                getCard_service= new WS_CallService(context_aact);
                jsonResponseString = getCard_service.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");

            SelectedCardList.clear();

            try{
                dialog.dismiss();
            }catch (Exception e){

            }
            if(jsonResponse!=null) {
            Log.e("getCardList", "response " + jsonResponse);
            try {

                    JSONObject jObj = new JSONObject(jsonResponse);

                 if(jObj.getString("Status").equalsIgnoreCase("Success")){

                    JSONArray GetCardArray=null;
                     if(jObj.has("response")){
                         GetCardArray=jObj.getJSONArray("response");
                    }
                    Log.e("ArrayLenth","->"+GetCardArray.length());

                    for(int i=0;i<GetCardArray.length();i++){
                        CardPojo pojo=new CardPojo();
                        pojo.setCard_id(GetCardArray.getJSONObject(i).getString("card_id"));
                        pojo.setCardno(GetCardArray.getJSONObject(i).getString("last4"));
                        pojo.setHoldername(GetCardArray.getJSONObject(i).getString("name"));
                        pojo.setMonth(GetCardArray.getJSONObject(i).getString("expiryMonth"));
                        pojo.setYear(GetCardArray.getJSONObject(i).getString("expiryYear"));
                        pojo.setPayMethod(GetCardArray.getJSONObject(i).getString("paymentMethod"));
                        SelectedCardList.add(pojo);
                    }

                     if(SelectedCardList.size()>0) {
                         CardListPopup("");
                     }else{
                         Toast.makeText(getApplicationContext(), "No Saved Cards", Toast.LENGTH_SHORT).show();
                     }

                    }
                    else{
                        if(SelectedCardList.size()>0) {
                           CardListPopup("");
                        }else{
                            Toast.makeText(getApplicationContext(), "No Saved Cards", Toast.LENGTH_SHORT).show();
                         }
                      }



            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),"Please try again later.",Toast.LENGTH_LONG).show();
                if(e!=null)
                System.out.println(e.toString() + "zcx");
            }
            }else if(jsonResponse==null){
                PopupMessage("Please check your Data/WiFi connection.");
            }

        }

    }




    private void hitPaymentService() {


        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:payment|token:" + getToken + "|email:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL) + "|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID) + "|amount:" + Amount;
            System.out.println(Preference_Constants.PREF_KEY_Service + " service  -->" + login_string);
            Log.e("Service", "Context" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_IFMoneyInWallet = new WS_CallService(PaymentActivity.this);
            IFMoneyInWallet_NVP.add(new BasicNameValuePair("WS", base64_register));
            IFMoneyInWallet_WebService = new Payment_WS(PaymentActivity.this, IFMoneyInWallet_NVP);
            IFMoneyInWallet_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    class ConnectionTask extends AsyncTask<String, Void, String> {

        final EditText name = (EditText) findViewById(R.id.name);
      //  final EditText numberField = (EditText) findViewById(R.id.number);
        final EditText cvvField = (EditText) findViewById(R.id.cvv);
        final Spinner spinMonth = (Spinner) findViewById(R.id.spinnerMonth);
        final Spinner spinYear = (Spinner) findViewById(R.id.spinnerYear);
        final int errorColor = Color.rgb(204, 0, 51);

        private boolean validateCardFields(final String number, final String month, final String year, final String cvv) {
//            boolean error = false;
//            clearFieldsError();
//
//            if (!CardValidator.validateCardNumber(number)) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try{
//                            dialog.dismiss();
//                        }catch (Exception e){}
//                        numberField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
//                    }
//                });
//                error = true;
//            }
//            if (!CardValidator.validateExpiryDate(month, year)) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try{
//                            dialog.dismiss();
//                        }catch (Exception e){}
//                        spinMonth.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
//                        spinYear.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
//                    }
//                });
//
//                error = true;
//            }
//            if (cvv.equals("")) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try{
//                            dialog.dismiss();
//                        }catch (Exception e){}
//                        cvvField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
//                    }
//                });
//                error = true;
//            }
            return true;
        }

        private void clearFieldsError() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cvvField.getBackground().clearColorFilter();
                    spinMonth.getBackground().clearColorFilter();
                    spinYear.getBackground().clearColorFilter();
                    numberField.getBackground().clearColorFilter();
                }
            });
        }

        @Override
        protected String doInBackground(String... urls) {



                try {
                    Card card = new Card(cardNo, name.getText().toString(), spinMonth.getSelectedItem().toString(), spinYear.getSelectedItem().toString(), cvvField.getText().toString());
                    CheckoutKit ck = CheckoutKit.getInstance(publicKey, CheckoutKit.Environment.LIVE);
//                    CheckoutKit ck = CheckoutKit.getInstance(publicKey);
                    final Response<CardTokenResponse> resp = ck.createCardToken(card);
                    if (resp.hasError) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                goToError();
                                try {
                                    dialog.dismiss();
                                }catch (Exception e){}
                            }
                        });
                    } else {
                        CardToken ct = resp.model.getCard();

                        Log.e("Card", "Name" + ct.getName());
                        Log.e("Card", "month" + ct.getExpiryMonth());
                        Log.e("Card", "APyment method" + ct.getJson());
                        resp.model.getJson();
//                      goToSuccess();
                        Log.e("Response", "CheckoutKit" + resp.model.getCardToken());
                        try {
                            dialog.dismiss();
                        }catch (Exception e){}

                        getToken = resp.model.getCardToken();

                        Intent PaymentIntent = new Intent(PaymentActivity.this, WalletProcessingActivity.class);
                        PaymentIntent.putExtra("PaymentToken", getToken);
                        PaymentIntent.putExtra("PaymentAmount", Amount);
                        PaymentIntent.putExtra("Email", GetEmail);
                        PaymentIntent.putExtra("card_number", cardNo);
                        PaymentIntent.putExtra("CardId", "");
                        PaymentIntent.putExtra("card_name", name.getText().toString());
                        PaymentIntent.putExtra("card_expiry_month", spinMonth.getSelectedItem().toString());
                        PaymentIntent.putExtra("card_expiry_year", spinYear.getSelectedItem().toString());
                        PaymentIntent.putExtra("card_cvv", cvvField.getText().toString());
                        PaymentIntent.putExtra("save_card", saveCard);

                        Log.e("Email","->"+GetEmail);
                        Log.e("card_number","->"+cardNo);
                        Log.e("card_name","->"+name.getText().toString());
                        Log.e("card_expiry_month","->"+spinMonth.getSelectedItem().toString());
                        Log.e("card_expiry_year","->"+spinYear.getSelectedItem().toString());
                        Log.e("card_cvv","->"+cvvField.getText().toString());
                        Log.e("save_card","->"+saveCard);



                        startActivity(PaymentIntent);

//                        hitPaymentService();
//                        finish();

                        return resp.model.getCardToken();
                    }
                } catch (final CardException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                dialog.dismiss();
                            }catch (Exception e){}
                            if (e.getType().equals(CardException.CardExceptionType.INVALID_CVV)) {
                                cvvField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            } else if (e.getType().equals(CardException.CardExceptionType.INVALID_EXPIRY_DATE)) {
                                spinMonth.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                                spinYear.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            } else if (e.getType().equals(CardException.CardExceptionType.INVALID_NUMBER)) {
                                numberField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            }
                        }
                    });
                } catch (CheckoutException | IOException e2) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            goToError();


                            try {
                                dialog.dismiss();
                            }catch (Exception e){}
                            if(e2!=null) {
                                OkayDialog("No Internet connection");
                            }else{
                                OkayDialog(e2.toString());
                            }
                        }
                    });
                }

            return "";
        }

    }

    public void OkayDialog(String Message)
    {
        final Dialog dialog1 = new Dialog(PaymentActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView   text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(Message);
        text.setTextColor(Color.parseColor("#000000"));
        dialog1.show();
        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                finish();
            }
        });
    }


    public class Payment_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

//        Dialog dialog;

        public Payment_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            dialog = new Dialog(PaymentActivity.this);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setCancelable(false);
//            dialog.setCanceledOnTouchOutside(false);
//            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);
//
//            Glide.with(PaymentActivity.this)
//                    .load(R.drawable.loading)
//                    .asGif()
//                    .placeholder(R.drawable.loading)
//                    .crossFade()
//                    .into(image);
//            dialog.show();
//            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Payment = new WS_CallService(context_aact);
                jsonResponseString = service_Payment.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            System.out.println("POST EXECUTE");
//            dialog.dismiss();
            Log.e("Payment", "response" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                Log.e("If Monye", "Response" + jsonResponse);


                if (jObj.getString("Status").equalsIgnoreCase("Success")) {
                    Intent newInten = new Intent(PaymentActivity.this, WalletProcessingActivity.class);
                    newInten.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(newInten);

                    finish();
                } else {

                }

//                hitLocationService();

//                try {
//                    dialog.dismiss();
//                } catch (Exception e) {
//
//                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = '-';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }

            try {
                numberField.setBackground(getResources().getDrawable(R.drawable.grey_border));
            }catch (Exception e){}
        }
    }
    private  void EditWatcher() {

        numberField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

              //  number.getBackground().setColorFilter(getResources().getDrawable(R.drawable.grey_border), PorterDuff.Mode.SRC_ATOP);
               /* try {
                    numberField.setBackground(getResources().getDrawable(R.drawable.grey_border));
                }catch (Exception e){}*/
                /*if (number.getText().length() <= 6 || number.getText().toString().equalsIgnoreCase("") || number.getText().toString().isEmpty()) {
                    //smsVerificationButton.setEnabled(false);
                } else if(number.getText().toString().trim().length()>=7){
                   // smsVerificationButton.setEnabled(true);
                }*/
            }
        });
    }

    public void PopupMessage(final String msgText){
        final Dialog dialog1 = new Dialog(PaymentActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView text = (TextView) dialog1.findViewById(R.id.text);
        text.setText(""+msgText);
        text.setTextColor(Color.parseColor("#000000"));
        dialog1.show();
        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                finish();
            }
        });

    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

}
