package com.smartspot.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.smartspot.notification.Config;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;


/**
 * Created by iyyapparajr on 8/1/2017.
 */


public class MyFirebaseInstanceIDService  extends FirebaseInstanceIdService {

    private static final String TAG = "MyAndroidFCMIIDService";
    SharedPreferences shared;
    SharedPreferences.Editor edit;


    @Override
    public void onTokenRefresh() {
        //Get hold of the registration token
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log the token
       /* Log.d(TAG, "Refreshed token: " + refreshedToken);
        System.out.println("FCM token in firebase id servcice -->"+refreshedToken);*/
        //storeRegIdInPref(refreshedToken);




        // sending reg id to your server
        Log.e("Value", "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

        AppPreferences.putStringIntoStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW,refreshedToken);

        Log.e("Value","InstanceService"+refreshedToken);

//        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_FCMTOKEN,refreshedToken.toString());
//        Log.e("Value","Old service"+refreshedToken);
        /*if(!refreshedToken.equalsIgnoreCase(""))
        {
            AppPreferences.putStringIntoStoreOne(Preference_Constants.PREF_KEY_USER_FCM_KEY, refreshedToken.toString());
        }
*/



    }
    private void sendRegistrationToServer(String token) {
        //Implement this method if you want to store the token on your server
        System.out.println("FCM toekn"+token.toString());
        Log.e("WOWOWOWOW", "Refreshed token: " + token);
        Log.e("Value","InstanceService"+token);
//        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FCM_KEY,token);

        if(!token.equalsIgnoreCase("")) {
//            AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_FCMTOKEN,token);
            AppPreferences.putStringIntoStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW,token);

        }
    }
}
