package com.smartspot.font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by iyyapparajr on 8/31/2017.
 */

public class ProximaNova {

    private static ProximaNova instance;
    private static Typeface typeface;

    public static ProximaNova getInstance(Context context) {
        synchronized (ProximaNova.class) {
            if (instance == null) {
                instance = new ProximaNova();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "PROXIMANOVA_LIGHT_WEBFONT.TTF");
            }
            return instance;
        }
    }
    public static ProximaNova getInstance(Context context , int i) {
        synchronized (ProximaNova.class) {
            if (instance == null) {
                instance = new ProximaNova();
                if(i==0)
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "PROXIMANOVA_LIGHT_WEBFONT.TTF");
                else
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "PROXIMANOVA_BOLD_WEBFONT.TTF");

            }
            return instance;
        }
    }






    public Typeface getTypeFace() {
        return typeface;
    }
}
