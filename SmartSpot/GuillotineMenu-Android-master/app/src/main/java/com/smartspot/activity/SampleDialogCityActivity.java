package com.smartspot.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.smartspot.App;
import com.smartspot.R;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.ListObject;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by iyyapparajr on 9/12/2017.
 */

public class SampleDialogCityActivity extends Activity  {

    ListView dialogList;
    WS_CallService service_ReservationHistory;
    ArrayList<NameValuePair> ReservationHistory_NVP=new ArrayList<>();
    ReservationHistory_WS ReservationHistory_WebService;
    private ArrayList<ListObject>originalData = null;
    AutoCompleteTextView search_netwrok;
    String entered_text;
    ArrayList<ListObject> filteredData=new ArrayList<>();
    SearchableAdapter circle_list_item_adapter;
    ArrayList<ListObject> CityArrayList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.city_layout_dialog);

        dialogList=(ListView)findViewById(R.id.listview);
        hitSelectCity();
        search_netwrok=(AutoCompleteTextView)findViewById(R.id.search_auto);
        filteredData=new ArrayList<ListObject>();



        search_netwrok.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
//                search_progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                entered_text=s.toString();
                if (circle_list_item_adapter!=null) {
                    if (filteredData.size()>0) {
                        circle_list_item_adapter.getFilter().filter(entered_text);
                    }
                }

            }
        });


    }


    private void hitSelectCity() {

        ReservationHistory_NVP=new ArrayList<>();

        byte[] data;
        try {
//          String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Select City|StateID:1|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println("Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_ReservationHistory = new WS_CallService(this);
            ReservationHistory_NVP.add(new BasicNameValuePair("WS", base64_register));
            ReservationHistory_WebService = new ReservationHistory_WS(this, ReservationHistory_NVP);
            ReservationHistory_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


    public class SearchableAdapter extends BaseAdapter implements Filterable {
        private LayoutInflater mInflater;
        ItemFilter mFilter = new ItemFilter();

        Context actContext;
        public SearchableAdapter(Context context, ArrayList<ListObject> data) {
//            this.filteredData = data ;
            System.out.println("Constructor-->"+filteredData.size());
            this.actContext=context;
            originalData = data ;
            mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return filteredData.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // A ViewHolder keeps references to children views to avoid unnecessary calls
            // to findViewById() on each row.
            Holder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_item, null);


//                convertView = inflater.inflate(layoutId, null);
                holder = new Holder();

                holder.Group_firstName=(TextView)convertView.findViewById(R.id.textview);


                System.out.println("Values in adapter if section");


                convertView.setTag(holder);
            } else {

                holder = (Holder) convertView.getTag();
            }

            if (convertView == null) {

                System.out.println("ADADPTER DESC--->"+ filteredData.get(position).getCityName());

            } else {

                holder = (Holder) convertView.getTag();
            }



//            holder.  Group_firstName.setText(plan_list_adap.get(position).getGroupOwnerFirstName()+" "+plan_list_adap.get(position).getGroupOwnerLastName());
//            holder.Group_firstName.setText(filteredData.get(position).getGroupOwnerFirstName());
            holder.Group_firstName.setText(filteredData.get(position).getCityName());







            return convertView;
        }
        class ViewHolder {
            TextView text;
        }

        public Filter getFilter() {
            return mFilter;
        }

        private class Holder {



            TextView Group_firstName,Group_lastName,ownerEmail;
        }

        private class ItemFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final List<ListObject> list = originalData;

                int count = list.size();
                final ArrayList<ListObject> nlist = new ArrayList<>(count);

                ListObject filterableString ;

                for (int i = 0; i < count; i++) {
                    filterableString = list.get(i);
                    if (filterableString.getCityName().toLowerCase().contains(filterString)) {
                        nlist.add(filterableString);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (ArrayList<ListObject>) results.values;
                circle_list_item_adapter. notifyDataSetChanged();
            }

        }
    }


    public class ReservationHistory_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;
        Context context_aact;
        Dialog dialog;



        public ReservationHistory_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(SampleDialogCityActivity.this);
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();


            dialog = new Dialog(SampleDialogCityActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);


//            dialog.setTitle("Title...");

            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SampleDialogCityActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();



//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_ReservationHistory = new WS_CallService(context_aact);
                jsonResponseString = service_ReservationHistory.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            try {
                dialog.dismiss();
            } catch (Exception e) {

            }


            System.out.println("POST EXECUTE");
//            Log.e("jsonResponse", "Login" + jsonResponse);
            if(jsonResponse!=null) {
                try {
                    JSONObject jObj = new JSONObject(jsonResponse);

                    System.out.println(Preference_Constants.PREF_KEY_Service + "   Values-->" + jsonResponse);
                    Log.e("City", "Response" + jsonResponse);
                    String status = jObj.getString("Status");
                    if (status.toString().equalsIgnoreCase("Success")) {

                        JSONArray ReservJson = jObj.getJSONArray("city");

                        String futurebookAmt = "";
                        String futureAfthrs = jObj.getString("$FutureBookingAfterHrs");
                        String currentGMT = jObj.getString("currentGMT");

                        if (jObj.has("futureBookingAmt"))
                            futurebookAmt = jObj.getString("futureBookingAmt");

                        String user_wallet_amount = jObj.getString("user_wallet_amount");
                        String BookingPrice = jObj.getString("BookingPrice");
                        String user_free_parking=jObj.getString("user_free_parking");
                        AppPreferences.putStringIntoStore(Preference_Constants.user_wallet_amount,user_wallet_amount);
                        AppPreferences.putStringIntoStore(Preference_Constants.user_free_parking,user_free_parking);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_AFTERHOUR, futureAfthrs);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_CURRENT_GMT, currentGMT);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_BOOKING_PRICE, futurebookAmt);

                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY, user_wallet_amount);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOKING_PRICE, BookingPrice);
                        Log.e("futureAfterHrs", "" + futureAfthrs);
                        Log.e("currentGMT", "" + currentGMT);
                        Log.e("BookPrice", "" + BookingPrice);
                        Log.e("Wallet", "Amount" + user_wallet_amount);
                        Log.e("Wallet", "PrefAmount" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));
                        Log.e("futureBookAmt", "" + futurebookAmt);

                        for (int i = 0; i < ReservJson.length(); i++) {
                            ListObject CityArrayListObj = new ListObject();


                            String city_id = jObj.getJSONArray("city").getJSONObject(i).getString("city_id");
                            String city_name = jObj.getJSONArray("city").getJSONObject(i).getString("city_name");

                            System.out.println("Response city list ==>" + city_id);

                            CityArrayListObj.setCity_id(city_id);
                            CityArrayListObj.setCityName(city_name);
//                        CityArrayListnew.add(CityArrayListObj);

                            filteredData.add(CityArrayListObj);
                        }


                        System.out.println(Preference_Constants.PREF_KEY_Service + "Size of an adapter ==>" + CityArrayList.size());
                        if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty()) {
                            for (int i = 0; i < filteredData.size(); i++) {
                                String defaultCity = filteredData.get(i).getCityName().toLowerCase().toString();
                                if (defaultCity.equals("dubai")) {
                                    Log.d("vignesh-PARK", "dubai" + i);
                                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_CITY, filteredData.get(i).getCity_id());
                                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME, filteredData.get(i).getCityName());

                                    //    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_CITY,"");
                                    //     AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME,"");
                                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION, "");
                                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME, "");

                                    break;
                                }
                            }
                        }
                        circle_list_item_adapter = new SearchableAdapter(context_aact, filteredData);
                        dialogList.setAdapter(circle_list_item_adapter);


                        dialogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                System.out.println(Preference_Constants.PREF_KEY_Service + "On item selected Listener==>" + filteredData.get(i).getCity_id());

                                System.out.println(Preference_Constants.PREF_KEY_Service + "On item selected Listener==>" + filteredData.get(i).getCityName());
                                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_CITY, filteredData.get(i).getCity_id());
                                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME, filteredData.get(i).getCityName());

                                //    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_CITY,"");
                                //     AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME,"");
                                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION, "");
                                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME, "");


                                Intent broadcastIntent = new Intent("Closebackactivity");
//                broadcastIntent.setAction("CreateGroupFollower");
                                broadcastIntent.putExtra("Data", "fromReseller");
                                sendBroadcast(broadcastIntent);
                                MainActivity.fromcreategroup = true;
                                finish();
//


                            }


                        });




                        System.out.println("success");
                    } else {
                        System.out.println("failed");
                        String Message = jObj.getString("Message");

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                    }


//                hitLocationService();


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Please try again later.", Toast.LENGTH_LONG).show();
                    System.out.println(e.toString() + "zcx");
                }
            }else{
                PopupMessage("Please check your Data/WiFi connection.");
            }
        }

    }


    public void PopupMessage(final String msgText){
        final Dialog dialog1 = new Dialog(SampleDialogCityActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        TextView text = (TextView) dialog1.findViewById(R.id.text);
        text.setText(""+msgText);
        text.setTextColor(Color.parseColor("#000000"));
        dialog1.show();
        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                finish();
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent LoginIntent = new Intent(SampleDialogCityActivity.this, MainActivity.class);
//        startActivity(LoginIntent);
//        finish();
    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;


    }
}