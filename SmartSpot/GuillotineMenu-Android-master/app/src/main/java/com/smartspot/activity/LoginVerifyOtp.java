package com.smartspot.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.smartspot.R;
import com.smartspot.adapter.CountryPojo;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.sharedpreferences.SmartSpaceApplication;
import com.smartspot.widget.WS_CallService;
import com.smartspot.widget.volleyrequestforstring;
import com.yqritc.scalablevideoview.ScalableVideoView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by iyyapparajr on 12/14/2017.
 */

public class LoginVerifyOtp extends Activity {

    TextView phoneNumberText, skiptext, termsText;
    EditText inputCode;
    Button verifyBtn;
    TextView resendOtpText;

    String MobileNumber = "", CountryCode = "";
    String userID, UserName, Email, Mobile;
    SharedPreferences sharedpreferences;
    HashMap<String, String> addCommentParam = new HashMap<>();

    WS_CallService service_Login;
    ArrayList<NameValuePair> login_NVP = new ArrayList<>();
    Load_Login_WS Login_Hitservice;
    volleyrequestforstring request;
    ScalableVideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.loginverify_otp);

        phoneNumberText = (TextView) findViewById(R.id.phoneNumber);
        resendOtpText = (TextView) findViewById(R.id.resendotp);
        inputCode = (EditText) findViewById(R.id.inputCode);
        verifyBtn = (Button) findViewById(R.id.confirm);
        skiptext = (TextView) findViewById(R.id.skiptext);
        termsText = (TextView) findViewById(R.id.termsText);

        verifyBtn.setEnabled(false);

        if (getIntent() != null) {
            MobileNumber = getIntent().getStringExtra("Mobile");
            CountryCode = getIntent().getStringExtra("CountryCode");

            phoneNumberText.setText(MobileNumber);
        }
        Log.e("Mobile", "->" + MobileNumber);
        Log.e("CountryCode", "->" + CountryCode);

        EditWatcher();


        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                verifyBtn,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();


        resendOtpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isInternetOn())
                    ResendOtp();

            }
        });

        skiptext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginVerifyOtp.this, HowItWorks.class));
            }
        });

        termsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginVerifyOtp.this, TermsAndCondition.class));
            }
        });


    }

    public void onSubmitClicked(View view) {
        if (isInternetOn())
            VerfifyOtp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(LoginVerifyOtp.this, LoginOtp.class));
        LoginVerifyOtp.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadVideo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mVideoView.stop();
            mVideoView.reset();
        } catch (Exception e) {

        }
    }

    public void VerfifyOtp() {
        //isInternetOn();
        dialog = new Dialog(LoginVerifyOtp.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

        Glide.with(LoginVerifyOtp.this)
                .load(R.drawable.loading)
                .asGif()
                .placeholder(R.drawable.loading)
                .crossFade()
                .into(image);
        dialog.show();
        addCommentParam.clear();
        addCommentParam.put("authkey", getResources().getString(R.string.sendotp_key));
        addCommentParam.put("mobile", MobileNumber);
        addCommentParam.put("otp", inputCode.getText().toString().trim());

        Log.e("VerifyOtp", "->" + MobileNumber);
        Log.e("addcomment", "->" + addCommentParam.toString());


        request = new volleyrequestforstring(Request.Method.POST,
                "https://control.msg91.com/api/verifyRequestOTP.php", addCommentParam,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String result) {
                        Log.d("Verifyresponse", result.toString());
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            Log.e("Exception", "->" + e.toString());
                        }

                        if (result != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(result);

                                String typeResponse = "";
                                if (jsonObject.has("type"))
                                    typeResponse = jsonObject.getString("type");

                                if (typeResponse.equalsIgnoreCase("success")) {
                                    // Toast.makeText(getApplicationContext(),"Verified Successfully",Toast.LENGTH_LONG).show();
                                    LoginService();
                                } else {

                                    String errorResponse = "";
                                    if (jsonObject.has("message"))
                                        errorResponse = jsonObject.getString("message");
                                    if (errorResponse.equalsIgnoreCase("otp_not_verified")) {
                                        errorPopup("Otp is incorrect");
                                    } else {
                                        errorPopup("Please try again later.");
                                    }
                                }


                            } catch (Exception e) {
                                Log.e("ex", e.toString());
                                errorPopup("Please try again later.");

                            }

                        } else if (result == null) {
                            errorPopup("Please try again later.");
                        }

                        // pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    dialog.dismiss();
                } catch (Exception e) {
                    Log.e("Exception", "" + e.toString());
                }

                Log.e("Exception", "" + error.toString());
                // VolleyLog.d(TAG, "Error: " + error.getMessage());
                //pDialog.hide();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> headers = new HashMap<String, String>();
                String username = "ck_5360f98f4d5117380e025cbfc997631349cf1d8a";
                String password = "cs_d308bd539a20479501975e9dfda93f03c6fdb9d0";
                String auth = new String(Base64.encode((username + ":" + password).getBytes(), Base64.URL_SAFE | Base64.NO_WRAP));
                // String auth =new String(Base64.encode(( username + ":" + password).getBytes(),Base64.URL_SAFE|Base64.NO_WRAP));

                headers.put("WSH", "U2VjcmV0OlBhcmtpbmdBcHB8UGFzc3dvcmQ6RkJGLklTSEpOLlhraw==");

                for (Map.Entry<String, String> entry : addCommentParam.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    headers.put(key, value);
                   /* if(entry.getKey().equalsIgnoreCase("page")){
                        entry.setValue(""+pagecount);
                    }*/
                    // ...
                }

                //headers.put("Content-Type", "application/json");
                // headers.put("apiKey", "xxxxxxxxxxxxxxx");
                return headers;
                // return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(15000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        SmartSpaceApplication.getInstance().addToRequestQueue(request);
        //  SoSellApplication.getInstance().addToRequestQueue(jsonObjReq, "String");
    }

    private void LoginService() {

        login_NVP = new ArrayList<>();
        byte[] data;
        try {

            String login_string = "Function:LoginWithMobile|Mobile Number:" + MobileNumber + "|LoginType:Manual|fcm_key:" + AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
            System.out.println("Fucntionals login request-->" + login_string);

            Log.e("Login", "Service" + login_string);
            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_Login = new WS_CallService(getApplicationContext());
            login_NVP.add(new BasicNameValuePair("WS", base64_register));
            Login_Hitservice = new Load_Login_WS(LoginVerifyOtp.this, login_NVP);
            Login_Hitservice.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;


        Dialog dialog;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = new Dialog(LoginVerifyOtp.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(LoginVerifyOtp.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {

                    String Message = jObj.getString("Response");
                    String user_wallet_amount = jObj.getString("user_wallet_amount");
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY, user_wallet_amount);
                    System.out.println("Login response-->" + Message);

                    userID = jObj.getJSONArray("Response").getJSONObject(0).getString("UserID");
                    UserName = jObj.getJSONArray("Response").getJSONObject(0).getString("UserName");
                    Email = jObj.getJSONArray("Response").getJSONObject(0).getString("Email");
                    Mobile = jObj.getJSONArray("Response").getJSONObject(0).getString("Mobile");
                    String user_free_parking = jObj.getString("user_free_parking");
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_ID, userID);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME, UserName);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_EMAIL, Email);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_MOBILE, Mobile);
                    AppPreferences.putStringIntoStore(Preference_Constants.user_free_parking, user_free_parking);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREY_KEY_INTENT_TYPE, "Login");
                    int value = 0;
                    sharedpreferences = getSharedPreferences("HOWITWORKS", MODE_PRIVATE);
                    value = sharedpreferences.getInt("how_it_flag", 0);
                    value++;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("how_it_flag", value);
                    editor.commit();

                    Intent LoginIntent = new Intent(LoginVerifyOtp.this, MainActivity.class);
                    startActivity(LoginIntent);
                    LoginVerifyOtp.this.finish();

                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_LONG).show();
                }

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {
                errorPopup("Please try again later.");
                System.out.println(e.toString() + "zcx");
            }

        }

    }

    Dialog dialog;

    public void ResendOtp() {
        //isInternetOn();
        dialog = new Dialog(LoginVerifyOtp.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

        Glide.with(LoginVerifyOtp.this)
                .load(R.drawable.loading)
                .asGif()
                .placeholder(R.drawable.loading)
                .crossFade()
                .into(image);
        dialog.show();

        addCommentParam.clear();
        addCommentParam.put("authkey", getResources().getString(R.string.sendotp_key));
        addCommentParam.put("mobile", MobileNumber);
        //addCommentParam.put("retrytype","text");
        addCommentParam.put("retrytype", "voice");

        Log.e("VerifyOtp", "->" + MobileNumber);
        Log.e("addcomment", "->" + addCommentParam.toString());
        //http://control.msg91.com/api/retryotp.php

        request = new volleyrequestforstring(Request.Method.POST,
                " http://api.msg91.com/api/retryotp.php", addCommentParam,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String result) {
                        Log.d("ReOtpresponse", result.toString());

                        try {
                            dialog.dismiss();
                        } catch (Exception e) {

                        }

                        if (result != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(result);

                                String typeResponse = "";
                                if (jsonObject.has("type"))
                                    typeResponse = jsonObject.getString("type");

                                if (typeResponse.equalsIgnoreCase("success")) {
                                    Toast.makeText(getApplicationContext(), "OTP sent successfully", Toast.LENGTH_LONG).show();
                                } else {

                                    String errorResponse = "";
                                    if (jsonObject.has("message"))
                                        errorResponse = jsonObject.getString("message");
                                    if (errorResponse.equalsIgnoreCase("Please Enter valid mobile no")) {
                                        errorPopup("Invalid mobile number");
                                    } else {
                                        errorPopup(errorResponse);
                                    }
                                }


                            } catch (Exception e) {
                                Log.e("ex", e.toString());
                                errorPopup("Please try again later.");

                            }

                        } else if (result == null) {
                            errorPopup("Please try again later.");

                        }

                        // pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


                Log.e("Exception", "" + error.toString());
                // VolleyLog.d(TAG, "Error: " + error.getMessage());
                //pDialog.hide();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> headers = new HashMap<String, String>();
                String username = "ck_5360f98f4d5117380e025cbfc997631349cf1d8a";
                String password = "cs_d308bd539a20479501975e9dfda93f03c6fdb9d0";
                String auth = new String(Base64.encode((username + ":" + password).getBytes(), Base64.URL_SAFE | Base64.NO_WRAP));
                // String auth =new String(Base64.encode(( username + ":" + password).getBytes(),Base64.URL_SAFE|Base64.NO_WRAP));

                headers.put("WSH", "U2VjcmV0OlBhcmtpbmdBcHB8UGFzc3dvcmQ6RkJGLklTSEpOLlhraw==");

                for (Map.Entry<String, String> entry : addCommentParam.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    headers.put(key, value);
                   /* if(entry.getKey().equalsIgnoreCase("page")){
                        entry.setValue(""+pagecount);
                    }*/
                    // ...
                }

                //headers.put("Content-Type", "application/json");
                // headers.put("apiKey", "xxxxxxxxxxxxxxx");
                return headers;
                // return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(15000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        SmartSpaceApplication.getInstance().addToRequestQueue(request);
        //  SoSellApplication.getInstance().addToRequestQueue(jsonObjReq, "String");
    }


    public void errorPopup(String message) {

        final Dialog dialog1 = new Dialog(LoginVerifyOtp.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);

        TextView messagetxt = (TextView) dialog1.findViewById(R.id.text);
        messagetxt.setText("" + message);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        dialog1.show();


        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();


            }
        });

    }


    private void EditWatcher() {

        inputCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                Log.e("length", "->" + inputCode.getText().toString().trim().length());
                if (inputCode.getText().length() <= 3 || inputCode.getText().toString().equalsIgnoreCase("") || inputCode.getText().toString().isEmpty()) {
                    verifyBtn.setEnabled(false);
                } else if (inputCode.getText().toString().trim().length() == 4) {
                    verifyBtn.setEnabled(true);
                }
            }
        });

    }

    public void ValidatePopup() {

        final Dialog dialog1 = new Dialog(LoginVerifyOtp.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);

        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        dialog1.show();


        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();

                Intent LoginIntent = new Intent(LoginVerifyOtp.this, MainActivity.class);
                startActivity(LoginIntent);
                finish();
            }
        });


    }


    private void LoadVideo() {


        mVideoView = (ScalableVideoView) findViewById(R.id.surfaceViewFrame);
        // findViewById(R.id.btn_next).setOnClickListener(this);

        try {
            mVideoView.setRawData(R.raw.smart);
            mVideoView.setVolume(0, 0);
            mVideoView.setLooping(true);
            mVideoView.prepare(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mVideoView.start();
                }
            });
        } catch (Exception e) {
            //ignore
            Log.e("error", e.getStackTrace().toString());
        }

    }


    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            Toast.makeText(getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

}
