package com.smartspot.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.smartspot.R;
import com.smartspot.adapter.CountryPojo;
import com.smartspot.adapter.WeightspinnerAdapter;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.Service2;
import com.smartspot.widget.WS_CallService;
import com.smartspot.widget.volleyrequestforstring;
import com.yqritc.scalablevideoview.ScalableVideoView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class RegisterOtp extends Activity {

    RequestQueue queue;


    WS_CallService service_Login;

    ArrayList<NameValuePair> login_NVP = new ArrayList<>();
    Load_Login_WS Login_Hitservice;

    ScalableVideoView mVideoView;
    String countryStr="";
    Spinner CountrySpinner;
    WeightspinnerAdapter CountryAdapter;
    ArrayList<CountryPojo> CountryList=new ArrayList<>();
    Button smsVerificationButton;
    EditText otp,UsernameEdit, MobilenumberEdit;
    TextView skiptext, termsText;
    volleyrequestforstring request;

    WS_CallService service_SignUp;
    ArrayList<NameValuePair> SignUp_NVP = new ArrayList<>();
    Load_SignUp_WS signUp_Hitservice;

    ArrayList<NameValuePair> phoneNameValuePair = new ArrayList<>();
    WS_CallService checkPhone_service;
    IsPhoneWS Isphonews;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.register_otp);

        queue = Volley.newRequestQueue(RegisterOtp.this);


        UsernameEdit=(EditText)findViewById(R.id.name);
        MobilenumberEdit=(EditText)findViewById(R.id.phoneNumber);
        CountrySpinner=(Spinner)findViewById(R.id.countrySpin);
        smsVerificationButton=(Button)findViewById(R.id.smsVerificationButton);
        skiptext=(TextView) findViewById(R.id.skiptext);
        termsText=(TextView) findViewById(R.id.termsText);
        smsVerificationButton.setEnabled(false);

        UsernameEdit.requestFocus();
        smsVerificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isInternetOn())
                IsPhoneNumer();

            }
        });


        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                smsVerificationButton,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();

       // LoadVideo();

        EditWatcher();


        CountryPojo pojo=new CountryPojo();
        pojo.setCode("91");
        CountryList.add(pojo);
        CountryPojo pojo1=new CountryPojo();
        pojo1.setCode("971");
        CountryList.add(pojo1);
       /* CountryPojo pojo1=new CountryPojo();
        pojo1.setCode("971-2");
        CountryList.add(pojo1);
        CountryPojo pojo2=new CountryPojo();
        pojo2.setCode("971-6");
        CountryList.add(pojo2);
        CountryPojo pojo3=new CountryPojo();
        pojo3.setCode("971-3");
        CountryList.add(pojo3);
        CountryPojo pojo4=new CountryPojo();
        pojo4.setCode("971-58");
        CountryList.add(pojo4);
        CountryPojo pojo5=new CountryPojo();
        pojo5.setCode("971-6");
        CountryList.add(pojo5);
        CountryPojo pojo6=new CountryPojo();
        pojo6.setCode("971-9");
        CountryList.add(pojo6);
        CountryPojo pojo7=new CountryPojo();
        pojo7.setCode("971-4");
        CountryList.add(pojo7);
        CountryPojo pojo8=new CountryPojo();
        pojo8.setCode("971-70");
        CountryList.add(pojo8);
        CountryPojo pojo9=new CountryPojo();
        pojo9.setCode("971-48");
        CountryList.add(pojo9);
        CountryPojo pojo10=new CountryPojo();
        pojo10.setCode("971-77");
        CountryList.add(pojo10);
        CountryPojo pojo11=new CountryPojo();
        pojo11.setCode("971-6");
        CountryList.add(pojo11);
        CountryPojo pojo12=new CountryPojo();
        pojo12.setCode("971-88");
        CountryList.add(pojo12);
*/

        CountryAdapter = new WeightspinnerAdapter(RegisterOtp.this,
                android.R.layout.simple_spinner_item, CountryList);
        // dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        CountrySpinner.setAdapter(CountryAdapter);


        CountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    countryStr=CountryList.get(position).getCode();
                   //Toast.makeText(getApplicationContext(),CountryList.get(position).getCode(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        skiptext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        termsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterOtp.this,TermsAndCondition.class));
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadVideo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try
        {
            mVideoView.stop();
            mVideoView.reset();
        }
        catch (Exception e)
        {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegisterOtp.this,SignUpLoginActivity.class));
        RegisterOtp.this.finish();
    }


    private void LoadVideo(){


        mVideoView = (ScalableVideoView) findViewById(R.id.surfaceViewFrame);
        // findViewById(R.id.btn_next).setOnClickListener(this);


        try {
            mVideoView.setRawData(R.raw.smart);
            mVideoView.setVolume(0, 0);
            mVideoView.setLooping(true);
            mVideoView.prepare(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mVideoView.start();
                }
            });
        } catch (Exception e) {
            //ignore
            Log.e("error",e.getStackTrace().toString());
        }

    }

    private void IsPhoneNumer() {

        phoneNameValuePair=new ArrayList<>();
        byte[] data;
        try {

            String login_string = "Function:CheckMobileNumber|user_mobile_number:"+MobilenumberEdit.getText().toString().trim().replace("+","");
            System.out.println("Fucntionals IsPhoneNo request-->" + login_string);

            Log.e("Login","Service"+login_string);
            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            phoneNameValuePair.add(new BasicNameValuePair("WS", base64_register));

            Isphonews = new IsPhoneWS(RegisterOtp.this, phoneNameValuePair);
            Isphonews.execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public class IsPhoneWS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;


        Dialog dialog;

        public IsPhoneWS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = new Dialog(RegisterOtp.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(RegisterOtp.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.


            try {
                checkPhone_service = new WS_CallService(context_aact);
                jsonResponseString = checkPhone_service.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            try {
                dialog.dismiss();
            } catch (Exception e) {

            }

            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "IsPhoneNumber" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String typeResponse="";
                if(jObj.has("Status"))
                    typeResponse=jObj.getString("Status");

                if(typeResponse.equalsIgnoreCase("Success")){

                    SendOtp();

                }else{

                    String errorResponse="";
                    if(jObj.has("Message"))
                        errorResponse=jObj.getString("Message");
                    if(errorResponse.equalsIgnoreCase("Mobile number already exists.")) {
                        errorPopup("Mobile number already exists.");
                    }else{
                        errorPopup(errorResponse);
                    }
                }

            } catch (Exception e) {
                errorPopup("Please try again later.");
            }
        }

    }




    private void SendOtp() {

        login_NVP=new ArrayList<>();
        byte[] data;
        try {

            login_NVP.add(new BasicNameValuePair("authkey", getResources().getString(R.string.sendotp_key)));
            login_NVP.add(new BasicNameValuePair("message", "Your verification code is ##OTP##."));
            login_NVP.add(new BasicNameValuePair("sender", "SMSPOT"));
            login_NVP.add(new BasicNameValuePair("mobile",MobilenumberEdit.getText().toString().trim().replace("+","")));

            Log.e("SendOtp","->"+countryStr+MobilenumberEdit.getText().toString().trim());
            Login_Hitservice = new Load_Login_WS(RegisterOtp.this, login_NVP);
            Login_Hitservice.execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;


        Dialog dialog;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = new Dialog(RegisterOtp.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(RegisterOtp.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.


            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        "http://control.msg91.com/api/sendotp.php");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            try {
                dialog.dismiss();
            } catch (Exception e) {

            }

            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "Otp" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String typeResponse="";
                if(jObj.has("type"))
                    typeResponse=jObj.getString("type");

                if(typeResponse.equalsIgnoreCase("success")){

                    Intent intent=new Intent(RegisterOtp.this,RegisterVerifyOtp.class);
                    intent.putExtra("Name",UsernameEdit.getText().toString());
                    intent.putExtra("Mobile",MobilenumberEdit.getText().toString().trim().replace("+",""));
                    intent.putExtra("CountryCode",countryStr);
                    startActivity(intent);
                    RegisterOtp.this.finish();
                }else{

                    String errorResponse="";
                    if(jObj.has("message"))
                        errorResponse=jObj.getString("message");
                    if(errorResponse.equalsIgnoreCase("Please Enter valid mobile no")) {
                        errorPopup("Invalid mobile number");
                    }else{
                        errorPopup(errorResponse);
                    }
                }

            } catch (Exception e) {

            }
        }

    }

    public void errorPopup(String message){

        final Dialog dialog1 = new Dialog(RegisterOtp.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);

        TextView messagetxt=(TextView)dialog1.findViewById(R.id.text);
        messagetxt.setText(""+message);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        dialog1.show();


        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();


            }
        });

    }





    private  void EditWatcher(){

        MobilenumberEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if(MobilenumberEdit.getText().length() <= 6  || MobilenumberEdit.getText().toString().equalsIgnoreCase("") || MobilenumberEdit.getText().toString().isEmpty()
                        ||UsernameEdit.getText().length()==0 || UsernameEdit.getText().toString().equalsIgnoreCase("") || UsernameEdit.getText().toString().isEmpty()){
                    smsVerificationButton.setEnabled(false);
                } else if(MobilenumberEdit.getText().toString().trim().length()>=7){
                    smsVerificationButton.setEnabled(true);
                }
            }
        });



        UsernameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if(UsernameEdit.getText().length()==0 || UsernameEdit.getText().toString().equalsIgnoreCase("") || UsernameEdit.getText().toString().isEmpty()
                        ||MobilenumberEdit.getText().length() <= 6  || MobilenumberEdit.getText().toString().equalsIgnoreCase("") || MobilenumberEdit.getText().toString().isEmpty()){
                    smsVerificationButton.setEnabled(false);
                } else if(MobilenumberEdit.getText().toString().trim().length()>=7){
                    smsVerificationButton.setEnabled(true);
                }
            }
        });


    }


    private void hitService() {

        byte[] data;
        try {

            String login_string = "Function:RegistrationwithPhonenumber|Mobile Number:" + "971"+MobilenumberEdit.getText().toString().trim()  + "|Name:" + UsernameEdit.getText().toString() + "|device id:|SignupType:Manual|device_type:2|fcm_key:"+AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
            System.out.println(Preference_Constants.PREF_KEY_Service + "Fucntionals Signup request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_SignUp = new WS_CallService(getApplicationContext());
            SignUp_NVP.add(new BasicNameValuePair("WS", base64_register));
            signUp_Hitservice = new Load_SignUp_WS(RegisterOtp.this, SignUp_NVP);
            signUp_Hitservice.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class Load_SignUp_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;

        Context context_aact;

        Dialog dialog;
    public Load_SignUp_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
        // TODO Auto-generated constructor stub

        this.loginact = loginws;
        this.context_aact = context_ws;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


        dialog = new Dialog(RegisterOtp.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

        Glide.with(RegisterOtp.this)
                .load(R.drawable.loading)
                .asGif()
                .placeholder(R.drawable.loading)
                .crossFade()
                .into(image);
        dialog.show();
//            isInternetOn();
        System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub.

        try {
            service_SignUp = new WS_CallService(getApplicationContext());
            jsonResponseString = service_SignUp.getJSONObjestString(loginact,
                    WebserviceUrl.mainurl);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonResponseString;
    }

    @Override
    protected void onPostExecute(String jsonResponse) {
        super.onPostExecute(jsonResponse);


        try {
            dialog.dismiss();
        } catch (Exception e) {

        }
        Log.e("response","->"+jsonResponse);

        try {

            JSONObject jsonObject=new JSONObject(jsonResponse);

            String statusString="";
            if(jsonObject.has("Status"))
            statusString=jsonObject.getString("Status");

            if(statusString.equalsIgnoreCase("Success")){
                Toast.makeText(getApplicationContext(),"Success Register",Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){

        }
    }
}

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }


    }
