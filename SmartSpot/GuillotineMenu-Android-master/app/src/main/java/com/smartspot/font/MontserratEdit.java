package com.smartspot.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class MontserratEdit extends EditText {

    public MontserratEdit(Context context) {
        super(context);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

    public MontserratEdit(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

    public MontserratEdit(Context context, AttributeSet attrs,
                           int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(Montserrat.getInstance(context).getTypeFace());
    }

}