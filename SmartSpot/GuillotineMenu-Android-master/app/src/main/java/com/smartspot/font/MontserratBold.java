package com.smartspot.font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class MontserratBold {

    private static MontserratBold instance;
    private static Typeface typeface;

    public static MontserratBold getInstance(Context context) {
        synchronized (MontserratBold.class) {
            if (instance == null) {
                instance = new MontserratBold();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Bold.otf");
            }
            return instance;
        }
    }
    public static MontserratBold getInstance(Context context , int i) {
        synchronized (MontserratBold.class) {
            if (instance == null) {
                instance = new MontserratBold();
                if(i==0)
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Bold.otf");
                else
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Bold.otf");

            }
            return instance;
        }
    }






    public Typeface getTypeFace() {
        return typeface;
    }
}
