package com.smartspot.font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class MontserratBlack {

    private static MontserratBlack instance;
    private static Typeface typeface;

    public static MontserratBlack getInstance(Context context) {
        synchronized (MontserratBlack.class) {
            if (instance == null) {
                instance = new MontserratBlack();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Black.otf");
            }
            return instance;
        }
    }
    public static MontserratBlack getInstance(Context context , int i) {
        synchronized (MontserratBlack.class) {
            if (instance == null) {
                instance = new MontserratBlack();
                if(i==0)
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Black.otf");
                else
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Black.otf");

            }
            return instance;
        }
    }






    public Typeface getTypeFace() {
        return typeface;
    }
}
