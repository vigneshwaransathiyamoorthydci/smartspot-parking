package com.smartspot.font;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by iyyapparajr on 8/31/2017.
 */

@SuppressLint("AppCompatCustomView")
public class ProximaNovaBlack extends TextView {

    public ProximaNovaBlack(Context context) {
        super(context);
        setTypeface(ProximaNova.getInstance(context).getTypeFace());
    }

    public ProximaNovaBlack(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(ProximaNova.getInstance(context).getTypeFace());
    }

    public ProximaNovaBlack(Context context, AttributeSet attrs,
                 int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(ProximaNova.getInstance(context).getTypeFace());
    }

}