package com.smartspot.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.smartspot.R;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by iyyapparajr on 8/30/2017.
 */

public class LoginActivity extends Activity {

    TextView loginText;
    ImageView back;
    String userID, UserName, Email, Mobile;

    WS_CallService service_Login;
    ArrayList<NameValuePair> login_NVP = new ArrayList<>();
    Load_Login_WS Login_Hitservice;
    EditText mobile_no,passwordEdit;
    SharedPreferences shared;
    SharedPreferences.Editor edit;
    String fcmToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_up_layout);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        mobile_no=(EditText)findViewById(R.id.mobile_no);
                passwordEdit=(EditText) findViewById(R.id.passwordEdit);

        loginText=(TextView)findViewById(R.id.loginText);
        Spinner city_spinner=(Spinner)findViewById(R.id.city_spinner1);


        shared=getSharedPreferences("FCM_Shared",MODE_PRIVATE);
//        fcmToken=   shared.getString(Preference_Constants.PREF_KEY_FCMTOKEN_NEW,fcmToken);

        AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);

        Log.e("Stored","Value Login"+AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW));

//        AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_FCMTOKEN);
//        Log.e("Stored","Value LoginOld"+AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN));

        ArrayList<String> countryList=new ArrayList<>();
        countryList.add("971-2");
        countryList.add("971-6");
        countryList.add("971-3");
        countryList.add("971-58");
        countryList.add("971-6");
        countryList.add("971-9");
        countryList.add("971-4");
        countryList.add("971-6");
        countryList.add("971-70");
        countryList.add("971-48");
        countryList.add("971-77");
        countryList.add("971-6");
        countryList.add("971-88");


        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.country_ids, R.layout.spinner_item_country);
        adapter2.setDropDownViewResource(R.layout.spinner_item_country);
        city_spinner.setAdapter(adapter2);

        back=(ImageView)findViewById(R.id.back);

        loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent navigationIntent=new Intent(LoginActivity.this,MainActivity.class);
//                startActivity(navigationIntent);

                if (mobile_no.getText().toString().trim().isEmpty() || passwordEdit.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter the required fields for a successful login.", Toast.LENGTH_LONG).show();
                } else {

                    hitServiceLogin();
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        mobile_no.setText("+971");
//        Selection.setSelection(mobile_no.getText(), mobile_no.getText().length());

        mobile_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable s) {

//                if(!s.toString().startsWith("+971")){
//                    mobile_no.setText("+971");
//                    Selection.setSelection(mobile_no.getText(), mobile_no.getText().length());
//
//                }
            }
        });
    }

    private void hitServiceLogin() {


        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String mobi= mobile_no.getText().toString().substring(1, mobile_no.getText().toString().length());
            String login_string = "Function:Login|Mobile Number:" + mobile_no.getText().toString() + "|Password:" + passwordEdit.getText().toString() + "|LoginType:Manual|fcm_key:"+ AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
            System.out.println("Fucntionals login request-->" + login_string);

            Log.e("Login","Service"+login_string);
            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_Login = new WS_CallService(getApplicationContext());
            login_NVP.add(new BasicNameValuePair("WS", base64_register));
            Login_Hitservice = new Load_Login_WS(LoginActivity.this, login_NVP);
            Login_Hitservice.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;
        Context context_aact;


        Dialog dialog;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = new Dialog(LoginActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(LoginActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {

                    String Message = jObj.getString("Response");
                    String user_wallet_amount= jObj.getString("user_wallet_amount");
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY,user_wallet_amount);
                    System.out.println("Login response-->" + Message);

                    userID = jObj.getJSONArray("Response").getJSONObject(0).getString("UserID");
                    UserName = jObj.getJSONArray("Response").getJSONObject(0).getString("UserName");
                    Email = jObj.getJSONArray("Response").getJSONObject(0).getString("Email");
                    Mobile = jObj.getJSONArray("Response").getJSONObject(0).getString("Mobile");

                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_ID, userID);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME, UserName);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_EMAIL, Email);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_MOBILE, Mobile);
                    AppPreferences.putBooleanIntoStore(Preference_Constants.PREY_KEY_USER_HOWIT_FLAG, true);


                    Intent LoginIntent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(LoginIntent);
                    finish();

                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }
}