package com.smartspot.fragment;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smartspot.App;
import com.smartspot.R;
import com.smartspot.activity.FutureSlotActivity;
import com.smartspot.activity.MainActivity;
import com.smartspot.activity.SampleDialogCityActivity;
import com.smartspot.activity.SampleDialogLocationActivity;
import com.smartspot.activity.SlotsActivity;
import com.smartspot.adapter.ReservationHistoryAdapter;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.ListObject;
import com.smartspot.widget.SearchableSpinner;
import com.smartspot.widget.WS_CallService;
import com.yqritc.scalablevideoview.ScalableVideoView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by iyyapparajr on 11/9/2017.
 */

public class NewBookingFragment extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SearchableAdapter    circle_list_item_adapter;
    // TODO: Rename and change types of parameters
    ArrayList<ListObject> filteredData;
    private String mParam1;
    private String mParam2;
    AutoCompleteTextView search_netwrok;
    ReservationHistoryAdapter reservation_history_adapter;
    private ArrayList<ListObject>originalData = null;

    String selecteddaetime="";

    Calendar calendar,calendartemp;


    WS_CallService service_City;
    ArrayList<ListObject> CityArrayList=new ArrayList<>();
    ArrayList<ListObject> CityArrayListnew=new ArrayList<>();


    ArrayList<ListObject> LocationArrayList=new ArrayList<>();

    ArrayList<NameValuePair> City_NVP=new ArrayList<>();
    City_WS city_WebService;
    String entered_text;

    WS_CallService service_Location;
    ArrayList<NameValuePair> Location_NVP=new ArrayList<>();

    Location_WS Location_WebService;

    String SelectedLocationId,SelectedLocationStr;

    TextView spinner1,location_spinner;
    SearchableSpinner searchnewspinner;


    WS_CallService service_ReservationHistory;
    ArrayList<NameValuePair> ReservationHistory_NVP=new ArrayList<>();
    ReservationHistory_WS ReservationHistory_WebService;

    LocationService_WS location_service_ws;
    ArrayList<NameValuePair> location_namevaluepair=new ArrayList<>();
    WS_CallService service_locationHistory;

    RelativeLayout city_layout,placeLayout;
    ArrayList<ListObject> BookingAray=new ArrayList<>();

    TextView search_slot,city_text,placeTExt,dateTime_text,clickdatetime;
    String SelectedcityId,SelectedcityStr, datetimeStr="",     BookingId;

    WS_CallService service_BookSlotFromServer;
    ArrayList<NameValuePair> BookSlotFromServer_NVP = new ArrayList<>();
    WS_CallService service_SearchSlots;
    private OnFragmentInteractionListener mListener;
    BroadcastReceiver broadFragment;
    ListView dialogList;
    BroadcastReceiver city_broadcast;
    int val=0;
    boolean flag=false;
    ScalableVideoView mVideoView;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public NewBookingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
        if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty())
        {

            spinner1.setText("Select City");
            location_spinner.setText("Select Location");
            Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
            startActivity(DalogIntent);

        }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty())
        {
            spinner1.setText("Select City");
            Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
            startActivity(DalogIntent);
        }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION).isEmpty())
        {
            spinner1.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME));
            location_spinner.setText("Select Location");
        }
        else
        {
            spinner1.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME));
            location_spinner.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME));
            String Apply_Hourly_Charges_string=AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges);
            if (!Apply_Hourly_Charges_string.isEmpty())
            {
                int Apply_Hourly_Charges= Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges).toString());

            if (Apply_Hourly_Charges==1)
            {
                final Dialog dialog1 = new Dialog(getActivity());
                if (Build.VERSION.SDK_INT < 16) {
                    getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.pop_up_dialog);
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);

                TextView   text=(TextView)dialog1.findViewById(R.id.text);
                text.setText("The charges for this location are Aed  " +Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Hourly_Charges))+" per hour ");

                // text.setTextColor(Color.parseColor("#ffffff"));
                dialog1.show();

                TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
                okay_popup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();

                        //getActivity().finish();
                    }
                });
            }
            }
            else
            {
                Toast.makeText(getContext(),"Try again Later",Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
//        if (isVisible) {
//            if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty())
//            {
//
//                spinner1.setText("Select City");
//                location_spinner.setText("Select Location");
//                Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
//                startActivity(DalogIntent);
//
//            }
//            else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty())
//            {
//                spinner1.setText("Select City");
//                Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
//                startActivity(DalogIntent);
//            }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION).isEmpty())
//            {
//                spinner1.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME));
//                location_spinner.setText("Select Location");
//            }
//            else
//            {
//                spinner1.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME));
//
//                location_spinner.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME));
//                int Apply_Hourly_Charges= Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges).toString());
//                if (Apply_Hourly_Charges==1)
//                {
//                    final Dialog dialog1 = new Dialog(getActivity());
//                    if (Build.VERSION.SDK_INT < 16) {
//                        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                    }
//                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    dialog1.setContentView(R.layout.pop_up_dialog);
//                    dialog1.setCancelable(false);
//                    dialog1.setCanceledOnTouchOutside(false);
//
//                    TextView   text=(TextView)dialog1.findViewById(R.id.text);
//                    text.setText("The charges for this location are Aed  " +Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Hourly_Charges))+" per hour ");
//
//                    // text.setTextColor(Color.parseColor("#ffffff"));
//                    dialog1.show();
//
//                    TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
//                    okay_popup.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            dialog1.dismiss();
//
//                            //getActivity().finish();
//                        }
//                    });
//                }
//            }
//        }


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
//        if (isVisible && isStarted) {
//            isStarted = true;
//
//            if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty())
//            {
//
//                spinner1.setText("Select City");
//                location_spinner.setText("Select Location");
//                Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
//                startActivity(DalogIntent);
//
//            }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty())
//            {
//                spinner1.setText("Select City");
//                Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
//                startActivity(DalogIntent);
//            }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION).isEmpty())
//            {
//                spinner1.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME));
//                location_spinner.setText("Select Location");
//            }
//            else
//            {
//                spinner1.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME));
//                location_spinner.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME));
//                int Apply_Hourly_Charges= Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges).toString());
//                if (Apply_Hourly_Charges==1)
//                {
//                    final Dialog dialog1 = new Dialog(getActivity());
//                    if (Build.VERSION.SDK_INT < 16) {
//                        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                    }
//                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    dialog1.setContentView(R.layout.pop_up_dialog);
//                    dialog1.setCancelable(false);
//                    dialog1.setCanceledOnTouchOutside(false);
//
//                    TextView   text=(TextView)dialog1.findViewById(R.id.text);
//                    text.setText("The charges for this location are Aed  " +Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Hourly_Charges))+" per hour ");
//
//                    // text.setTextColor(Color.parseColor("#ffffff"));
//                    dialog1.show();
//
//                    TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
//                    okay_popup.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            dialog1.dismiss();
//
//                            //getActivity().finish();
//                        }
//                    });
//                }
//            }
//        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    public static NewBookingFragment newInstance(String param1, String param2) {
        NewBookingFragment fragment = new NewBookingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        Log.e("Fragment","Passing");
    }


    /**
     * Callback for when the menu is closed.search_slot
     */
    public static interface OnMenuClosedListenerNewBooking {

        public abstract void onMenuClosed();

    }
    RadioButton nowbutton, futurebutton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.city_state_layout_new, container, false);


        city_text= (TextView)view.findViewById(R.id.city_text);
        placeTExt= (TextView)view.findViewById(R.id.placeTExt);

        spinner1 = (TextView)view.findViewById(R.id.city_spinner);
        location_spinner= (TextView) view. findViewById(R.id.location_spinner);
        search_slot= (TextView)view. findViewById(R.id.search_slot);
        dateTime_text= (TextView)view. findViewById(R.id.datetime_text);
        clickdatetime= (TextView)view. findViewById(R.id.clickdatetime);

        placeLayout= (RelativeLayout) view. findViewById(R.id.placeLayout);
        city_layout= (RelativeLayout)view. findViewById(R.id.city_layout);

        nowbutton=(RadioButton)view.findViewById(R.id.button21);
        futurebutton=(RadioButton)view.findViewById(R.id.button22);
        AppPreferences.putStringIntoStore(Preference_Constants.CURRENT_FRAGMENT,"1");
        selecteddaetime="";
//        searchnewspinner=(SearchableSpinner)view.findViewById(R.id.searchnewspinner);
//        hitCityService();
//        searchnewspinner.setAdapter(new SpinnerCityAdapter(getActivity(),R.layout.dropdownview, CityArrayListnew));
//        spinner1.setAdapter(new CityAdapter(getActivity(),R.layout.dropdownview, CityArrayList));


        System.out.println(Preference_Constants.PREF_KEY_Service+"CITY AND LOCATION"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY));
        System.out.println(Preference_Constants.PREF_KEY_Service+"CITY AND LOCATION"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION));

//        if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty())
//        {
//
//            spinner1.setText("Select City");
//            location_spinner.setText("Select Location");
//            Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
//            startActivity(DalogIntent);
//
//        }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).isEmpty())
//        {
//            spinner1.setText("Select City");
//            Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
//            startActivity(DalogIntent);
//        }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION).isEmpty())
//        {
//            spinner1.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME));
//            location_spinner.setText("Select Location");
//        }
//        else
//        {
//            spinner1.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY_NAME));
//            location_spinner.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME));
//            int Apply_Hourly_Charges= Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges).toString());
//            if (Apply_Hourly_Charges==1)
//            {
//                final Dialog dialog1 = new Dialog(getActivity());
//                if (Build.VERSION.SDK_INT < 16) {
//                    getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                            WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                }
//                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog1.setContentView(R.layout.pop_up_dialog);
//                dialog1.setCancelable(false);
//                dialog1.setCanceledOnTouchOutside(false);
//
//                TextView   text=(TextView)dialog1.findViewById(R.id.text);
//                text.setText("The charges for this location are Aed  " +Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Hourly_Charges))+" per hour ");
//
//                // text.setTextColor(Color.parseColor("#ffffff"));
//                dialog1.show();
//
//                TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
//                okay_popup.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialog1.dismiss();
//
//                        //getActivity().finish();
//                    }
//                });
//            }
//        }

        city_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("Sample city","Click");
                if(isInternetOn()) {
                    Intent DalogIntent = new Intent(getActivity(), SampleDialogCityActivity.class);
                    startActivity(DalogIntent);
                }
            }
        });

        placeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String City= AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).toString();

                    if (City.contains("Select City") || City.isEmpty() || City == null) {
                        Toast.makeText(getActivity(), "Please select city", Toast.LENGTH_LONG).show();
                    } else {
                        if(isInternetOn()) {
                        Log.e("Sample location", "Click");
                        Intent DalogIntent = new Intent(getActivity(), SampleDialogLocationActivity.class);
                        startActivity(DalogIntent);
                    }
                }
            }
        });

        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                search_slot,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();

        search_slot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String City= AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY).toString();
                String Location= AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION).toString();

                String user_wallet_amount= AppPreferences.getStringFromStore(Preference_Constants.user_wallet_amount).toString();
                String user_free_parking= AppPreferences.getStringFromStore(Preference_Constants.user_free_parking).toString();
                System.out.println(Preference_Constants.PREF_KEY_Service+"City and location-->"+City+"Lovationis -->"+Location);

                if(isInternetOn()){
                if (City.contains("Select City") || Location.contains("Select Location") ||City.isEmpty()||Location.isEmpty()||City==null||Location==null) {
                    if (City.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Please select city", Toast.LENGTH_LONG).show();
                     }
                    else if(Location.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Please select location", Toast.LENGTH_LONG).show();
                    }else if (City.equalsIgnoreCase("") && Location.equalsIgnoreCase("")){
                        Toast.makeText(getActivity(), "Please select city and location", Toast.LENGTH_LONG).show();
                    }

                } else

                {
                    if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOK_TYPE).equalsIgnoreCase("now")) {
                        String Apply_Hourly_Charges_string=AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges);
                        if (!Apply_Hourly_Charges_string.isEmpty()) {
                            int Apply_Hourly_Charges= Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges));
                            Intent slotsIntent = new Intent(getActivity(), SlotsActivity.class);
                            slotsIntent.putExtra("CityId", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY));
                            slotsIntent.putExtra("LocationId", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION));
                            slotsIntent.putExtra("date",datetimeStr );
                            slotsIntent.putExtra("Apply_Hourly_Charges",Apply_Hourly_Charges);
                            slotsIntent.putExtra("user_wallet_amount",user_wallet_amount);
                            slotsIntent.putExtra("user_free_parking",user_free_parking);

                            startActivity(slotsIntent);
                        }
                        else
                        {
                            Toast.makeText(getContext(),"Try again Later",Toast.LENGTH_SHORT).show();
                        }
                    }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOK_TYPE).equalsIgnoreCase("future")) {
                        if(dateTime_text.getText().toString().equalsIgnoreCase("")){
                            Popup("Please choose a Date and Time");
                        }else{
                           // FutureSlot();
                            int Apply_Hourly_Charges= Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges));
                            Intent slotsIntent = new Intent(getActivity(), FutureSlotActivity.class);
                            slotsIntent.putExtra("CityId", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY));
                            slotsIntent.putExtra("LocationId", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION));
                            slotsIntent.putExtra("date",datetimeStr );;
                            slotsIntent.putExtra("Apply_Hourly_Charges",Apply_Hourly_Charges);
                            slotsIntent.putExtra("user_wallet_amount",user_wallet_amount);
                            slotsIntent.putExtra("user_free_parking",user_free_parking);
                            startActivity(slotsIntent);
                        }
                    }
                }

                    System.out.println(Preference_Constants.PREF_KEY_Service + "seleted city and location-->" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY) + "Seleted city id-->" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION));

                }
            }
        });

        if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOK_TYPE).equalsIgnoreCase("")){
            SegmentedGroup segmented2 = (SegmentedGroup) view.findViewById(R.id.segmented2);
            segmented2.setTintColor(Color.parseColor("#f15938"));
            nowbutton.setChecked(true);
            futurebutton.setChecked(false);
            dateTime_text.setVisibility(View.GONE);
            clickdatetime.setVisibility(View.GONE);
            AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");
        }
        else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOK_TYPE).equalsIgnoreCase("now")){
            SegmentedGroup segmented2 = (SegmentedGroup) view.findViewById(R.id.segmented2);
            segmented2.setTintColor(Color.parseColor("#f15938"));
            nowbutton.setChecked(true);
            futurebutton.setChecked(false);
            dateTime_text.setVisibility(View.GONE);
            clickdatetime.setVisibility(View.GONE);
            AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");

        }else if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOK_TYPE).equalsIgnoreCase("future")){
            SegmentedGroup segmented2 = (SegmentedGroup) view.findViewById(R.id.segmented2);
            segmented2.setTintColor(Color.parseColor("#f15938"));
            futurebutton.setChecked(true);
            nowbutton.setChecked(false);
            dateTime_text.setVisibility(View.VISIBLE);
            clickdatetime.setVisibility(View.VISIBLE);
            AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"future");

        }

        nowbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateTime_text.setVisibility(View.GONE);
                clickdatetime.setVisibility(View.GONE);
                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");
            }
        });

        futurebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dateTime_text.setVisibility(View.VISIBLE);
                clickdatetime.setVisibility(View.VISIBLE);
                AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"future");

            }
        });

        clickdatetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showDateTimePicker();
                try {
                    if(spinner1.getText().toString().equalsIgnoreCase("Select City") || location_spinner.getText().toString().equalsIgnoreCase("Select Location")){
                      if(spinner1.getText().toString().equalsIgnoreCase("Select City"))
                        Toast.makeText(getActivity().getApplicationContext(),"Please select City",Toast.LENGTH_LONG).show();

                        if(location_spinner.getText().toString().equalsIgnoreCase("Select Location"))
                            Toast.makeText(getActivity().getApplicationContext(),"Please select Location",Toast.LENGTH_LONG).show();
                    }else{
                    //DatetimePicker();
                        if(isInternetOn()) {
                            getLocation();
                        }
                    }
                }
                catch (Exception e)
                {
                 Log.e("Exception","->"+e.toString());
                }
            }
        });



        mVideoView = (ScalableVideoView)view. findViewById(R.id.surfaceViewFrame);
        // findViewById(R.id.btn_next).setOnClickListener(this);

        try {
            mVideoView.setRawData(R.raw.smart);
            mVideoView.setVolume(0, 0);
            mVideoView.setLooping(true);
            mVideoView.prepare(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mVideoView.start();
                }
            });
        } catch (IOException ioe) {
            //ignore
        }


        return view;

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    long time;
    public void DatetimePicker() throws ParseException {
        final View dialogView = View.inflate(getActivity(), R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
        TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      //  sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
       // String currentDateandTime = sdf.format(new Date());
        String currentDateandTime=AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_CURRENT_GMT);
        Date date = sdf.parse(currentDateandTime);




        if(selecteddaetime.equalsIgnoreCase("")) {

                calendar=Calendar.getInstance();
            calendar.setTime(date);



            Calendar calendarmin = new GregorianCalendar(datePicker.getYear(),
                    datePicker.getMonth(),
                    datePicker.getDayOfMonth(),
                    timePicker.getCurrentHour(),
                    timePicker.getCurrentMinute());

            try {
                Log.e("test", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_AFTERHOUR));
                Log.e("test", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_LOCATION_TIMEZONE));
                String plusorminus = new String();
                String tempval = new String();
                if (AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_LOCATION_TIMEZONE).contains("+")) {
                    tempval = AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_LOCATION_TIMEZONE).replace("+", "");
                    plusorminus = "+";
                } else {
                    tempval = AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_LOCATION_TIMEZONE).replace("-", "");
                    plusorminus = "-";

                }
                float addtime = Float.parseFloat(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_AFTERHOUR));
                float timezone = Float.parseFloat(tempval);
                float overalltime = 0;
                if (plusorminus.equalsIgnoreCase("+"))

                {
                    overalltime = addtime + timezone;
                } else {
                    overalltime = addtime - timezone;
                }
                String split[] = String.valueOf(overalltime).split("\\.");
                float hours = 0;
                float min = 0;
                if (split.length > 1) {
                    hours = Float.parseFloat(split[0]);
                    if (split[1].length() == 1) {
                        min = Float.parseFloat(split[1] + "0");
                    } else {
                        min = Float.parseFloat(split[1]);
                    }
                } else {
                    hours = Float.parseFloat(split[0]);

                }
                calendar.add(Calendar.HOUR, (int) hours);
                calendar.add(Calendar.MINUTE, (int) min);
                calendartemp=calendar;

            } catch (Exception e) {
                Log.e("error", e.toString());
                e.printStackTrace();
            }
        }
        else
        {
            try {
                calendar=Calendar.getInstance();

                calendar.setTime(sdf.parse(selecteddaetime));
            }
            catch (Exception e)
            {

            }
        }
       // datePicker.setMinDate(calendarmin.getTimeInMillis());


       // datePicker.setMinDate(calendar.getTimeInMillis()- 1 * 1000);
        //c.getTimeInMillis() - 1 * OFFSET
       // cal.sett

        //timePicker.setCurrentHour();
        datePicker.updateDate(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));

        timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));


        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);

                Calendar calendar_inside = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());

                time = calendar_inside.getTimeInMillis();
                //dateTime_text.setText("Date and Time : "+calendar.get(Calendar.DATE)+","+calendar.get(Calendar.MONTH)+calendar.get(Calendar.YEAR)+" "+calendar.getTime());

                if(calendar_inside.getTimeInMillis()>=(calendartemp.getTimeInMillis()-60000)) {
                    datetimeStr=""+calendar_inside.YEAR+"-"+calendar_inside.MONTH+"-"+calendar_inside.DATE+" "+calendar_inside.HOUR+":"+calendar_inside.MINUTE;
                    //dateTime_text.setText("Date and Time : " + " " + calendar.getTime());
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH/mm/ss");
                   // System.out.println(cal.getTime());
// Output "Wed Sep 26 14:23:28 EST 2012"


                    String formatted = format1.format(calendar_inside.getTime());

                   // dateTime_text.setText("Date and Time : "+" "+""+calendar_inside.YEAR+"-"+calendar_inside.MONTH+"-"+calendar_inside.DATE+" "+calendar_inside.HOUR+":"+calendar_inside.MINUTE);
                    datetimeStr=formatted;
                //    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat updatetime =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                     selecteddaetime=updatetime.format(calendar_inside.getTime());
                    SimpleDateFormat format2 = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
                    String dateFormat = format2.format(calendar_inside.getTime());
                    dateTime_text.setText(dateFormat);
                    alertDialog.dismiss();

                    Log.e("SelectDateTime","->"+datetimeStr);
                }
                else
                {
                    //Toast.makeText(getActivity(),"Future bookings can be made starting from 5 hours later to eternity",Toast.LENGTH_LONG).show();
                    Toast.makeText(getActivity(),"Invalid Date and Time",Toast.LENGTH_LONG).show();
                }
               // Log.e("dateim","->"+time+", "+calendar.getTime()+","+calendar.get(Calendar.DATE)+","+calendar.get(Calendar.MONTH)+calendar.get(Calendar.YEAR));
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    public String parseDateToddMMyyyy(String time) {
        String outputPattern= "yyyy-MM-dd HH:mm";
        String inputPattern = "dd-MMM-yyyy h:mm ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    private void FutureSlot() {

        byte[] data;
        try {

            String CityId =AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY);
            String LocationId=AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION);
            String login_string = "Function:FutureBooking|city_id:"+ CityId+"|location_id:"+LocationId+"|user_id:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)+"|booking_date:"+datetimeStr+"|fcm_key:"+ AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
            System.out.println(Preference_Constants.PREF_KEY_Service + "FutureSlotparam  -->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_BookSlotFromServer = new WS_CallService(getActivity());
            BookSlotFromServer_NVP.add(new BasicNameValuePair("WS", base64_register));

            FutureSlot_WS  FutureSlot_WS_Slot_WebService = new FutureSlot_WS(getActivity(), BookSlotFromServer_NVP);
            FutureSlot_WS_Slot_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    public class FutureSlot_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public FutureSlot_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            System.out.println("POST EXECUTE");
            dialog.dismiss();
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                Log.e("If Monye","FutureSlotResponse"+jsonResponse);

                String status = jObj.getString("Status");

                if(status.equalsIgnoreCase("Success"))
                {
                    String messageResponse="";
                    if(jObj.has("Message")) {
                        messageResponse = jObj.getString("Message");

                        Log.e("messageResponse", "" + messageResponse);

                        SuccessPopup(messageResponse);
                    }
                    //Toast.makeText(getActivity().getApplicationContext(),status,Toast.LENGTH_LONG).show();
                }else
                {
                    if(jObj.has("Message")) {
                        if(jObj.getString("Message").equalsIgnoreCase("No slots available.")) {
                            Popup("Sorry! No Spots available! Booked-full!" );
                        }else if(jObj.getString("Message").equalsIgnoreCase("Insufficient amount in wallet.")) {
                            WalletPopup("" + jObj.getString("Message"));
                        }else{
                            Popup("" + jObj.getString("Message"));
                        }

                    }
                    //   Toast.makeText(getApplicationContext(),status,Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                Popup("Error on Server. Please try again later");
                System.out.println(e.toString() + "zcx");


            }

        }

    }


    private void getLocation() {

        location_namevaluepair=new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY);
            AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION);

            String login_string= "Function:SelectLocation|city_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY) + "|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println("Fucntionals login request-->" + login_string);

            Log.e("Location","Id-->"+login_string);


            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_locationHistory = new WS_CallService(getActivity());
            location_namevaluepair.add(new BasicNameValuePair("WS", base64_register));
            location_service_ws = new LocationService_WS(getActivity(), location_namevaluepair);
            location_service_ws.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class LocationService_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;
        Dialog dialog;

        public LocationService_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(SampleDialogLocationActivity.this);
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");

            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_locationHistory = new WS_CallService(context_aact);
                jsonResponseString = service_locationHistory.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
            Log.e("vignesh", "Location LIST-->" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service+"Values-->"+jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success"))
                {

                    String futureBookingAmt=jObj.getString("$FutureBookingAfterHrs");
                    String currentGMT=jObj.getString("currentGMT");

                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_AFTERHOUR,futureBookingAmt);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_CURRENT_GMT,currentGMT);



                    Log.e("currentGmt","->"+currentGMT);
                    Log.e("FutureAfterHrs","->"+futureBookingAmt);

                    JSONArray ReservJson=jObj.getJSONArray("Location");
//                  JSONArray jObj1 = new JSONArray(jsonResponse);


                    for(int i=0;i<ReservJson.length();i++)
                    {

                        String city_id=jObj.getJSONArray("Location").getJSONObject(i).getString("location_id");
                        String city_name=jObj.getJSONArray("Location").getJSONObject(i).getString("location_name");
                        String location_timezone=jObj.getJSONArray("Location").getJSONObject(i).getString("location_timezone");
                        if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION).equalsIgnoreCase(""+city_id)) {
                            AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_LOCATION_TIMEZONE, location_timezone);

                        }
                        Log.e("Responselocation","->"+city_id+" , "+ city_name+", "+location_timezone);
                        System.out.println("Response city list ==>"+city_id);

                      /*  CityArrayListObj.setCity_id(city_id);
                        CityArrayListObj.setCityName(city_name);
                        CityArrayListObj.setLocation_timezone(location_timezone);

                        filteredData.add(CityArrayListObj);*/
                    }

                      DatetimePicker();


                    System.out.println("success");
                }else {


//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }



//                hitLocationService();

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {
                dialog.dismiss();
                System.out.println(e.toString() + "zcx");
            }

        }

    }

    public void WalletPopup(String message){
        final Dialog dialog1 = new Dialog(getActivity());
        if (Build.VERSION.SDK_INT < 16) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView   text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(message);
        // text.setTextColor(Color.parseColor("#ffffff"));
        dialog1.show();

        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                Intent in = new Intent("WalletOpen");
                getActivity().getApplicationContext().sendBroadcast(in);
                //getActivity().finish();
            }
        });

    }


    public void Popup(String message){
        final Dialog dialog1 = new Dialog(getActivity());
        if (Build.VERSION.SDK_INT < 16) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView   text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(message);
        // text.setTextColor(Color.parseColor("#ffffff"));
        dialog1.show();

        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();

                //getActivity().finish();
            }
        });

    }

    public void SuccessPopup(String message){
        final Dialog dialog1 = new Dialog(getActivity());
        if (Build.VERSION.SDK_INT < 16) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView   text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(message);
        // text.setTextColor(Color.parseColor("#ffffff"));
        dialog1.show();

        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                Intent in = new Intent("BookSuccess");
                getActivity().getApplicationContext().sendBroadcast(in);
                //getActivity().finish();
            }
        });

    }


    public class City_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;
        Dialog dialog;

        public City_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(getActivity());
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();



            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();


//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_City = new WS_CallService(context_aact);
                jsonResponseString = service_City.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            CityArrayList.clear();
            System.out.println("POST EXECUTE");
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service+"City Values-->"+jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success"))
                {

                    JSONArray cityAr=jObj.getJSONArray("city");

                    for(int i=0;i<cityAr.length();i++)
                    {

                        ListObject CityArrayListObj=new ListObject();


                        String city_id=jObj.getJSONArray("city").getJSONObject(i).getString("city_id");
                        String city_name=jObj.getJSONArray("city").getJSONObject(i).getString("city_name");


                        CityArrayListObj.setCity_id(city_id);
                        CityArrayListObj.setCityName(city_name);
                        CityArrayListnew.add(CityArrayListObj);

                        CityArrayList.add(CityArrayListObj);
                    }
                    ListObject CityArrayListObj1=new ListObject();
                    CityArrayListObj1.setCity_id("-1");
                    CityArrayListObj1.setCityName("Select City");
                    CityArrayList.add(0,CityArrayListObj1);


//                    spinner1.setAdapter(new CityAdapter(getActivity(),R.layout.dropdownview, CityArrayList));

                    System.out.println("success");
                }else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }



//                hitLocationService();

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

    public class Location_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;

        Dialog dialog;

        Context context_aact;


        public Location_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(getActivity());
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();



            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_City = new WS_CallService(context_aact);
                jsonResponseString = service_City.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            LocationArrayList.clear();
            System.out.println("POST EXECUTE");
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service+"Location Values-->"+jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success"))
                {

                    JSONArray LocationAr=jObj.getJSONArray("Location");

                    for(int i=0;i<LocationAr.length();i++)
                    {

                        ListObject LocationArraylistObj=new ListObject();

                        String location_id=jObj.getJSONArray("Location").getJSONObject(i).getString("location_id");
                        String location_name=jObj.getJSONArray("Location").getJSONObject(i).getString("location_name");

                        LocationArraylistObj.setCity_id(location_id);
                        LocationArraylistObj.setCityName(location_name);
                        LocationArrayList.add(LocationArraylistObj);

                    }

                    ListObject LocationArrayListObj1=new ListObject();
                    LocationArrayListObj1.setCity_id("-1");
                    LocationArrayListObj1.setCityName("Select Location");
                    LocationArrayList.add(0,LocationArrayListObj1);

//                    location_spinner.setAdapter(new LocationAdapter(getActivity(),R.layout.dropdownview, LocationArrayList));


                    System.out.println("success");
                }else {

                    ListObject LocationArrayListObj1=new ListObject();
                    LocationArrayListObj1.setCity_id("-1");
                    LocationArrayListObj1.setCityName("No Location Available");
                    LocationArrayList.add(0,LocationArrayListObj1);

//                    location_spinner.setAdapter(new LocationAdapter(getActivity(),R.layout.dropdownview, LocationArrayList));



                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    private void hitReservationHistory() {


        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Select City|StateID:1";
            System.out.println("Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_ReservationHistory = new WS_CallService(getActivity());
            ReservationHistory_NVP.add(new BasicNameValuePair("WS", base64_register));
            ReservationHistory_WebService = new ReservationHistory_WS(getActivity(), ReservationHistory_NVP);
            ReservationHistory_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


    public class ReservationHistory_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;
        public ReservationHistory_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(getActivity());
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();



            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();


//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_ReservationHistory = new WS_CallService(context_aact);
                jsonResponseString = service_ReservationHistory.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service+"   Values-->"+jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success"))
                {

                    JSONArray ReservJson=jObj.getJSONArray("city");


                    for(int i=0;i<ReservJson.length();i++)
                    {
                        ListObject CityArrayListObj=new ListObject();



                        String city_id=jObj.getJSONArray("city").getJSONObject(i).getString("city_id");
                        String city_name=jObj.getJSONArray("city").getJSONObject(i).getString("city_name");


                        CityArrayListObj.setCity_id(city_id);
                        CityArrayListObj.setCityName(city_name);
                        CityArrayListnew.add(CityArrayListObj);

                        CityArrayList.add(CityArrayListObj);
                    }


                    System.out.println(Preference_Constants.PREF_KEY_Service+"Size of an adapter ==>"+CityArrayList.size());

                    circle_list_item_adapter = new SearchableAdapter(context_aact, CityArrayList);
                    dialogList.setAdapter(circle_list_item_adapter);


                    System.out.println("success");
                }else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }



//                hitLocationService();

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    public class SearchableAdapter extends BaseAdapter implements Filterable {



        private LayoutInflater mInflater;
        ItemFilter mFilter = new ItemFilter();

        Context actContext;
        public SearchableAdapter(Context context, ArrayList<ListObject> data) {
//            this.filteredData = data ;
            this.actContext=context;
            originalData = data ;
            mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return filteredData.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // A ViewHolder keeps references to children views to avoid unnecessary calls
            // to findViewById() on each row.
            Holder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_item, null);


//                convertView = inflater.inflate(layoutId, null);
                holder = new Holder();






                holder.Group_firstName=(TextView)convertView.findViewById(R.id.textview);


                System.out.println("Values in adapter if section");


                convertView.setTag(holder);
            } else {

                holder = (Holder) convertView.getTag();
            }

            if (convertView == null) {

                System.out.println("ADADPTER DESC--->"+ filteredData.get(position).getCityName());

            } else {

                holder = (Holder) convertView.getTag();
            }



//            holder.  Group_firstName.setText(plan_list_adap.get(position).getGroupOwnerFirstName()+" "+plan_list_adap.get(position).getGroupOwnerLastName());
//            holder.Group_firstName.setText(filteredData.get(position).getGroupOwnerFirstName());
            holder.Group_firstName.setText(filteredData.get(position).getCityName());







            return convertView;
        }
        class ViewHolder {
            TextView text;
        }

        public Filter getFilter() {
            return mFilter;
        }

        private class Holder {



            TextView Group_firstName,Group_lastName,ownerEmail;
        }

        private class ItemFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final List<ListObject> list = originalData;

                int count = list.size();
                final ArrayList<ListObject> nlist = new ArrayList<>(count);

                ListObject filterableString ;

                for (int i = 0; i < count; i++) {
                    filterableString = list.get(i);
                    if (filterableString.getCityName().toLowerCase().contains(filterString)) {
                        nlist.add(filterableString);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (ArrayList<ListObject>) results.values;
                circle_list_item_adapter. notifyDataSetChanged();
            }

        }
    }
    Calendar date;
    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        date = Calendar.getInstance();
        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                      //  Log.v(TAG, "The choosen one " + date.getTime());
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getActivity().getApplicationContext(), " No Internet connection", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }
}
