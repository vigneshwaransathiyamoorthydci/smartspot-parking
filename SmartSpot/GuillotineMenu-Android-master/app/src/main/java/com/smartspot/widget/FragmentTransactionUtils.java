package com.smartspot.widget;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by iyyapparajr on 9/9/2017.
 */

public class FragmentTransactionUtils {
    public static final String DEFAULT = "default";
    public static final String TYPE1 = "type1";
    public static final String TYPE2 = "type2";
    public static final String TYPE3 = "type3";
    public static final String TYPE4 = "type4";
    public static final String TYPE5 = "type5";
    private static FragmentTransactionUtils INSTANCE;
    private Context mContext;

    private int mEnterAnimation;
    private int mExitAnimation;
    private int mPopEnterAnimation;
    private int mPopExitAnimation;


    private FragmentTransactionUtils() {

    }


    public static FragmentTransactionUtils getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FragmentTransactionUtils();
        }
        return INSTANCE;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void setTransaction(FragmentTransaction transaction) {
        setTransaction(transaction, DEFAULT);
    }

    public void setTransaction(FragmentTransaction transaction, String type) {

        //default transaction
        mEnterAnimation = 0;
        mExitAnimation = 0;
//        mPopEnterAnimation = R.anim.fade_in;
//        mPopExitAnimation = R.anim.fade_out;

//        switch (type) {
//            case TYPE1:
//                mEnterAnimation = R.anim.slide_out_right;
//                mExitAnimation = R.anim.slide_out_left_fade;
//                break;
//            case TYPE2:
//                mEnterAnimation = R.anim.slide_out_right;
//                mExitAnimation = R.anim.slide_out_left_fade;
//                break;
//
//            case TYPE3:
//                mEnterAnimation = R.anim.scale_inlastic;
//                mExitAnimation = R.anim.slide_out_right_fade;
//                break;
//            case TYPE4:
//                mEnterAnimation = R.anim.slide_in_from_right_fade;
//                mExitAnimation = R.anim.fade_out_delay;
//                break;
//
//
//        }


        transaction.setCustomAnimations(mEnterAnimation, mExitAnimation, mPopEnterAnimation, mPopExitAnimation);


    }


}
