package com.smartspot.font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by iyyapparajr on 8/31/2017.
 */

public class ProximaNovaBold {

    private static ProximaNovaBold instance;
    private static Typeface typeface;

    public static ProximaNovaBold getInstance(Context context) {
        synchronized (ProximaNovaBold.class) {
            if (instance == null) {
                instance = new ProximaNovaBold();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "PROXIMANOVA_EXTRABOLD.OTF");
            }
            return instance;
        }
    }
    public static ProximaNovaBold getInstance(Context context , int i) {
        synchronized (ProximaNovaBold.class) {
            if (instance == null) {
                instance = new ProximaNovaBold();
                if(i==0)
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "PROXIMANOVA_EXTRABOLD.OTF");
                else
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "PROXIMANOVA_EXTRABOLD.OTF");

            }
            return instance;
        }
    }






    public Typeface getTypeFace() {
        return typeface;
    }
}
