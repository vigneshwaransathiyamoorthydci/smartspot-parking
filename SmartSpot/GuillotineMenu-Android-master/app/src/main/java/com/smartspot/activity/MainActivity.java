package com.smartspot.activity;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smartspot.App;
import com.smartspot.R;
import com.smartspot.fragment.NewBookingFragment;
import com.smartspot.fragment.ProfileFragment;
import com.smartspot.fragment.PromoCodeFragment;
import com.smartspot.fragment.ReservationHistoryFragment;
import com.smartspot.fragment.WaitingHistoryFragment;
import com.smartspot.fragment.WalletFragment;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.Permission;
import com.smartspot.widget.WS_CallService;
import com.yalantis.guillotine.animation.GuillotineAnimation;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Dmytro Denysenko on 5/4/15.
 */
public class MainActivity extends AppCompatActivity implements Animation.AnimationListener {
    private static final long RIPPLE_DURATION = 250;
    Fragment fragment;
    ObjectAnimator objectanimator1;
    Animation animMoveToTop;

    View guillotineMenu;
    public static boolean fromcreategroup;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.logout)
    ImageView logout;
    @BindView(R.id.edit)
    ImageView edit;
    int fragmentPosition;

    ImageView history_image, helpimage;
    TextView user_email, user_name, amount_mainmenu;
    BroadcastReceiver cityBroadcast;
    BroadcastReceiver IndexPassing;
    AlertDialog.Builder builder;
    @BindView(R.id.root)
    FrameLayout root;
    @BindView(R.id.content_hamburger)
    View contentHamburger;
    WS_CallService service_ReservationHistory;
    ArrayList<NameValuePair> ReservationHistory_NVP = new ArrayList<>();
    ReservationHistory_WS ReservationHistory_WebService;
    LinearLayout howit,feed_group, profile_group, booking_history, Waiting_History, wallet,  animatio, bounce,promocode;
    TextView actiobarTitle;
    BroadcastReceiver accountReceiver, profieReceiver, BookReceiver, amtReceiver,walletReceiver,currentWalletReceiver,UpdateWallet;
//, bookReceiver
private boolean mHasSaveInstanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity);
        ButterKnife.bind(this);

        Permission.verifyStoragePermissions(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }


        guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);


        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();


        actiobarTitle = (TextView) findViewById(R.id.actiobarTitle);
        howit= (LinearLayout) findViewById(R.id.howit);
        profile_group = (LinearLayout) findViewById(R.id.profile_group);
        feed_group = (LinearLayout) findViewById(R.id.new_booking);
        booking_history = (LinearLayout) findViewById(R.id.booking_history);
        Waiting_History = (LinearLayout) findViewById(R.id.waiting_history);
        wallet = (LinearLayout) findViewById(R.id.wallet);
        animatio = (LinearLayout) findViewById(R.id.animatio);
        bounce = (LinearLayout) findViewById(R.id.bounce);
        promocode= (LinearLayout) findViewById(R.id.applydis);
        final FrameLayout frame = (FrameLayout) findViewById(R.id.fragmtnframe);


        user_name = (TextView) findViewById(R.id.user_name);
        user_email = (TextView) findViewById(R.id.user_email);
        user_name.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME));
        user_email.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL));

        history_image = (ImageView) findViewById(R.id.history_image);
        amount_mainmenu = (TextView) findViewById(R.id.amount_mainmenu);
        helpimage= (ImageView) findViewById(R.id.help);

        Intent intent = getIntent();
        if (intent!=null)
        {
            String open = intent.getStringExtra("open");
            Log.d("SATURDAY","open"+open);
            if (open !=null)
            {

                history_image.setVisibility(View.VISIBLE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                helpimage.setVisibility(View.GONE);
                Fragment fragment = null;
                actiobarTitle.setText("WALLET");
                fragmentPosition=7;
                fragment = new WalletFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment,"WalletFragment")
                            .addToBackStack(null)
                            .commit();
                }

            }
            else if (intent.getBooleanExtra("fromnotification",false))
            {
                Fragment fragment = null;
                actiobarTitle.setText("BOOKING HISTORY");
                helpimage.setVisibility(View.VISIBLE);
                history_image.setVisibility(View.GONE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                fragmentPosition=2;
                fragment = new ReservationHistoryFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment,"ReservationHistoryFragment")
                            .addToBackStack(null)
                            .commit();

                }
            }
            else
            {
                helpimage.setVisibility(View.VISIBLE);
                actiobarTitle.setText("NEW BOOKING");
                Fragment fragment = null;
                fragment = new NewBookingFragment();
                fragmentPosition=1;
                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out);
                    ft.replace(R.id.fragmtnframe, fragment,"NewBookingFragment").commit();

                }
            }
        }

        amount_mainmenu.setText("Aed " + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));
        Log.e("Amount", "Value" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));


        SharedPreferences sharedpreferences=getSharedPreferences("HOWITWORKS",MODE_PRIVATE);
    //    sharedpreferences.getBoolean("how_it_flag",false);


        if(AppPreferences.getStringFromStore(Preference_Constants.PREY_KEY_INTENT_TYPE).equalsIgnoreCase("Login")){

            //If you want to open howitworks after login , please uncomment below
         /*if(sharedpreferences.getInt("how_it_flag",-1)==1){

             startActivity(new Intent(MainActivity.this,HowItWorks.class));

            int ssvalue=sharedpreferences.getInt("how_it_flag",2);
            ssvalue++;
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putInt("how_it_flag",ssvalue);
            editor.commit();
          }*/
        }
        else if(AppPreferences.getStringFromStore(Preference_Constants.PREY_KEY_INTENT_TYPE).equalsIgnoreCase("Signup")){

            if(AppPreferences.getBooleanFromStore(Preference_Constants.PREY_KEY_USER_HOWIT_FLAG))
            startActivity(new Intent(MainActivity.this,HowItWorks.class));

        }



        AppPreferences.putBooleanIntoStore(Preference_Constants.PREY_KEY_USER_HOWIT_FLAG, false);
        //AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");

        history_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent walletIntent = new Intent(MainActivity.this, WalletHistoryActovity.class);
                startActivity(walletIntent);
            }
        });

        helpimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent walletIntent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(walletIntent);
            }
        });


        cityBroadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                System.out.println(Preference_Constants.PREF_KEY_Service + "broad cast intent value is -->" + intent.getStringExtra("Data"));
            //    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");
                actiobarTitle.setText("NEW BOOKING");
                history_image.setVisibility(View.GONE);
                Fragment fragment = null;
                fragment = new NewBookingFragment();
                fragmentPosition=1;
//				fragment = new GroupFragmentNew();
                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out);
                    ft.replace(R.id.toolbar, fragment,"NewBookingFragment").commit();

                } else {
                    // error in creating fragment
                    // Log.e("MainActivity", "Error in creating fragment");
                }

            }
        };
        IntentFilter intent1 = new IntentFilter("CreateGroupFollower");
        registerReceiver(cityBroadcast, intent1);




        promocode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                helpimage.setVisibility(View.GONE);
//                actiobarTitle.setText("PROMO CODE");
//                logout.setVisibility(View.GONE);
//                edit.setVisibility(View.GONE);
//                history_image.setVisibility(View.GONE);
//                Fragment fragment = null;
//                fragmentPosition=7;
//                fragment = new PromoCodeFragment();
//                if (fragment != null) {
//                    getSupportFragmentManager().beginTransaction()
//                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
//                            // .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//                            .replace(R.id.fragmtnframe, fragment,"PromoCodeFragment")
//                            //   .addToBackStack(null)
//                            .commit();
//
//                }


                helpimage.setVisibility(View.GONE);
                actiobarTitle.setText("PROMO CODE");
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                history_image.setVisibility(View.GONE);
                Fragment fragment = null;
                fragment = new PromoCodeFragment();
                fragmentPosition=7;
//				fragment = new GroupFragmentNew();
                if (fragment != null) {


                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            // .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                            .replace(R.id.fragmtnframe, fragment,"PromoCodeFragment")
                               .addToBackStack(null)
                            .commit();

                } else {
                    // error in creating fragment
                    // Log.e("MainActivity", "Error in creating fragment");
                }

                DrawerClose();


            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit.setVisibility(View.GONE);
                Intent in = new Intent("Profile");
                in.putExtra("edit","yes");
                getApplicationContext().sendBroadcast(in);
            }
        });


        profile_group.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
//                getSupportActionBar().setTitle("PROFILE");

//                frame.removeAllViewsInLayout();

          //      AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");


                helpimage.setVisibility(View.GONE);
                actiobarTitle.setText("PROFILE");
                logout.setVisibility(View.VISIBLE);
                edit.setVisibility(View.VISIBLE);
                history_image.setVisibility(View.GONE);
                Fragment fragment = null;
                fragmentPosition=5;
                fragment = new ProfileFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                           // .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                            .replace(R.id.fragmtnframe, fragment,"ProfileFragment")
                            .addToBackStack(null)
                            .commit();

                }

                DrawerClose();

             //   startActivity(new Intent(MainActivity.this, SignUpLoginActivity.class));

            }
        });


//        TranslateAnimation slide = new TranslateAnimation(0, 0, 100,0 );
//        slide.setDuration(100);
//        slide.setFillAfter(true);


//        Animation an = new TranslateAnimation(0,0,100,0);//0,0 is the current       coordinates
//        an.setFillAfter(true);// to keep the state after animation is finished
//        profile_group.startAnimation(an);// to start animation obviously

//        TranslateAnimation animate = new TranslateAnimation(0,0,100,0);
//        animate.setDuration(100);
//        animate.setFillAfter(true);
//        animatio.startAnimation(animate);
//        animatio.setVisibility(View.GONE);
//        animatio.startAnimation(slide);
        animMoveToTop = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animationtop);
        animMoveToTop.setAnimationListener(this);
        animatio.startAnimation(animMoveToTop);

//        animate= (LinearLayout) findViewById(R.id.animate);
//        objectanimator1 = ObjectAnimator.ofFloat(animatio,"x",150);
//        objectanimator1.setDuration(8000);
//        objectanimator1.start();


        feed_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getSupportActionBar().setTitle("NEW BOOKING");
//                frame.removeAllViewsInLayout();
            //    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");

                Fragment fragment = null;
                actiobarTitle.setText("NEW BOOKING");
                history_image.setVisibility(View.GONE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                helpimage.setVisibility(View.VISIBLE);
                fragment = new NewBookingFragment();
                fragmentPosition=1;
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()

                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment,"NewBookingFragment")
                            .addToBackStack(null)
                            .commit();
                }

                DrawerClose();

            }
        });

        booking_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getSupportActionBar().setTitle("RESERVATION HISTORY");
          //      AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");

                Fragment fragment = null;
                actiobarTitle.setText("BOOKING HISTORY");
                helpimage.setVisibility(View.VISIBLE);
                history_image.setVisibility(View.GONE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                fragment = new ReservationHistoryFragment();
                fragmentPosition=2;
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment,"ReservationHistoryFragment")
                            .addToBackStack(null)
                            .commit();

                }

                DrawerClose();

            }
        });

        Waiting_History.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getSupportActionBar().setTitle("WAITING HISTORY");
         //       AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");

                Fragment fragment = null;
                actiobarTitle.setText("MY WAITLISTS");
                helpimage.setVisibility(View.VISIBLE);
                history_image.setVisibility(View.GONE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                fragmentPosition=3;
                fragment = new WaitingHistoryFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment,"WaitingHistoryFragment")
                            .addToBackStack(null)
                            .commit();
                }

                DrawerClose();

            }
        });
        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getSupportActionBar().setTitle("WALLET");
         //       AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");

                history_image.setVisibility(View.VISIBLE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                helpimage.setVisibility(View.GONE);
                Fragment fragment = null;
                actiobarTitle.setText("WALLET");
                fragmentPosition=6;
                fragment = new WalletFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment,"WalletFragment")
                            .addToBackStack(null)
                            .commit();
                }

                DrawerClose();


            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked

                                LogoutService();

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
//						      dialog.dismiss();

                                break;
                        }
                    }
                };
                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Do you want to logout?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();


            }
        });

        howit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,HowItWorks.class));
                DrawerClose();
            }
        });

    }

    public void clickProfile(View view){
   //     AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOK_TYPE,"now");
        helpimage.setVisibility(View.GONE);
        actiobarTitle.setText("PROFILE");
        logout.setVisibility(View.VISIBLE);
        edit.setVisibility(View.VISIBLE);
        history_image.setVisibility(View.GONE);
        Fragment fragment = null;
        fragment = new ProfileFragment();
        fragmentPosition=5;
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                    // .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.fragmtnframe, fragment,"ProfileFragment")
                    //   .addToBackStack(null)
                    .commit();

        }

        DrawerClose();
    }


    private void LogoutService() {


        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Logout|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println("Fucntionals logout request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_ReservationHistory = new WS_CallService(MainActivity.this);
            ReservationHistory_NVP.add(new BasicNameValuePair("WS", base64_register));
            ReservationHistory_WebService = new ReservationHistory_WS(MainActivity.this, ReservationHistory_NVP);
            ReservationHistory_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void deleteCache(MainActivity mainActivity) {

        try {
            File dir = mainActivity.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void DrawerClose() {
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mHasSaveInstanceState = true;
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d("THRUSDAY","wallet");
        amount_mainmenu.setText("Aed " + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));
        amtReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //Bundle bundle = intent.getExtras();
                Log.d("THRUSDAY","onReceive");
                amount_mainmenu.setText("Aed " + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));

            }
        };
        try {
            IntentFilter amtIntent = new IntentFilter("AmountReceive");
            registerReceiver(amtReceiver, amtIntent);
        }catch(Exception e){}


        walletReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //Bundle bundle = intent.getExtras();
                history_image.setVisibility(View.VISIBLE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                helpimage.setVisibility(View.GONE);
                Fragment fragment = null;
                actiobarTitle.setText("      WALLET");

                fragment = new WalletFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment,"WalletFragment")
                            .addToBackStack(null)
                            .commitAllowingStateLoss();

                }

                DrawerClose();
            }
        };
        try {
            IntentFilter walletIntent = new IntentFilter("WalletOpen");
            registerReceiver(walletReceiver, walletIntent);
        }catch(Exception e){}


        currentWalletReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //Bundle bundle = intent.getExtras();
                history_image.setVisibility(View.VISIBLE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                helpimage.setVisibility(View.GONE);
                actiobarTitle.setText("WALLET");

                Fragment fragment = null;
                fragment = new WalletFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment,"WalletFragment")
                            .addToBackStack(null)
                            .commitAllowingStateLoss();

                }

                DrawerClose();

            }
        };
        try {
            IntentFilter curtWalletIntent = new IntentFilter("CurrentWalletReceiver");
            registerReceiver(currentWalletReceiver, curtWalletIntent);
        }catch (Exception e){}


        mHasSaveInstanceState = false;

        BookReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("WEDNESDAY","BookReceiver");
                //Bundle bundle = intent.getExtras();
                Fragment fragment = null;
                actiobarTitle.setText("BOOKING HISTORY");
                helpimage.setVisibility(View.VISIBLE);
                history_image.setVisibility(View.GONE);
                logout.setVisibility(View.GONE);
                edit.setVisibility(View.GONE);
                fragment = new ReservationHistoryFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment)
                            .addToBackStack(null)
                            .commitAllowingStateLoss();

                }

                DrawerClose();


            }
        };
        try {
            IntentFilter bookIntent = new IntentFilter("BookSuccess");
            registerReceiver(BookReceiver, bookIntent);
        }catch (Exception e){}


        accountReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();
                if (bundle.getString("edit").equalsIgnoreCase("yes")) {
                    edit.setVisibility(View.VISIBLE);
                }

                user_name.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME));

            }
        };
        try {
            IntentFilter intent2 = new IntentFilter("ProfileOn");
            registerReceiver(accountReceiver, intent2);
        }catch (Exception e){}


        profieReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();
               /* if (bundle.getString("edit").equalsIgnoreCase("yes")) {
                    edit.setVisibility(View.VISIBLE);
                }*/
                helpimage.setVisibility(View.GONE);
                actiobarTitle.setText("PROFILE");
                logout.setVisibility(View.VISIBLE);
                edit.setVisibility(View.VISIBLE);
                history_image.setVisibility(View.GONE);
                Fragment fragment = null;
                fragment = new ProfileFragment();
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
                            .replace(R.id.fragmtnframe, fragment)
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                }

                DrawerClose();

            }
        };
        try {
            IntentFilter profileIntent = new IntentFilter("OpenProfile");
            registerReceiver(profieReceiver, profileIntent);
        }catch (Exception e){}


        bounce.clearAnimation();
        Animation animFadein;
        animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bounce);
        bounce.startAnimation(animFadein);

        animMoveToTop = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animationtop);
        animMoveToTop.setAnimationListener(this);
        animatio.startAnimation(animMoveToTop);

        amount_mainmenu.setText("Aed " + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));

        Log.e("Amount", "ResumeValue" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));
//        TranslateAnimation slide = new TranslateAnimation(0, 0, 100,0 );
//        slide.setDuration(100);
//        slide.setFillAfter(true);
//        animatio.startAnimation(slide);
        if (fromcreategroup) {
            fromcreategroup = false;

            Log.e("Resume", "Calling");
            System.out.println("broad Valuse return");
//            wallet_history.setVisibility(View.GONE);

            new Handler().post(new Runnable() {
                public void run() {
                    Fragment fragment = null;
                    actiobarTitle.setText("NEW BOOKING");
                    fragment = new NewBookingFragment();
                    if (fragment != null) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        //ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                        ft.replace(R.id.fragmtnframe, fragment).commit();
                    }
                }

            });
        }
    }


    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(cityBroadcast);
            // if (walletReceiver != null)
            unregisterReceiver(walletReceiver);

            //  if (profieReceiver != null)
            unregisterReceiver(profieReceiver);

            //    if (currentWalletReceiver != null)
            unregisterReceiver(currentWalletReceiver);

            //   if (accountReceiver != null)
            unregisterReceiver(accountReceiver);

            //     if (BookReceiver != null)
            unregisterReceiver(BookReceiver);

            //     if (amtReceiver != null)
            unregisterReceiver(amtReceiver);
        }catch (Exception e){}
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(cityBroadcast);
            // if (walletReceiver != null)
            unregisterReceiver(walletReceiver);

            //  if (profieReceiver != null)
            unregisterReceiver(profieReceiver);

                if (currentWalletReceiver != null)
            unregisterReceiver(currentWalletReceiver);

            //   if (accountReceiver != null)
            unregisterReceiver(accountReceiver);

            //     if (BookReceiver != null)
            unregisterReceiver(BookReceiver);

            //     if (amtReceiver != null)
            unregisterReceiver(amtReceiver);
        }catch (Exception e){}
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    public class ReservationHistory_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Dialog dialog;
        Context context_aact;
        String booking_barrier_name, booking_barrier_id;

        public ReservationHistory_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(MainActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();


//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_ReservationHistory = new WS_CallService(context_aact);
                jsonResponseString = service_ReservationHistory.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
            Log.e("Logout", "Response" + jsonResponse);
//            Toast.makeText(getApplicationContext(), jsonResponse, Toast.LENGTH_LONG).show();
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {
                    AppPreferences.clearAll(MainActivity.this);
                    //deleteCache(MainActivity.this);
//                  Log.e("DEvice","Logoiut");
                    Intent in = new Intent(MainActivity.this, SignUpLoginActivity.class);
                    startActivity(in);
                    finish();
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }


//                hitLocationService();

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");


            }

        }

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (fragmentPosition!=1)
        {
            actiobarTitle.setText("NEW BOOKING");
        }
        Log.d("PARKING_BACK","OnbackPressed"+fragmentPosition);







    }
}
