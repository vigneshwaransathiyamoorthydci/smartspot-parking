package com.smartspot.sharedpreferences;

/**
 * Created by iyyapparajr on 9/9/2017.
 */



/**
 * Created by manojg on 06-Jul-17.
 */
public class Preference_Constants {


//    public final static String PREF_KEY_FCMTOKEN = "FCMTOKEN";
    public final static String PREF_KEY_FCMTOKEN_NEW = "FCMTOKEN_new";
    public final static String PREF_KEY_USER_ID = "user_id";
    public final static String PREF_KEY_USER_FIRST_NAME = "user_firstname";
    public final static String PREF_KEY_USER_LAST_NAME = "user_lastname";
    public final static String PREF_KEY_USER_EMAIL = "user_email";

    public final static String PREF_KEY_APIKEY = "srijanVADS2S8JJ126FFKN4LL9VZ64C0NYW934";
    public final static String PREF_KEY_Service = "service";

    public final static String PREF_KEY_SELECTED_CITY = "Select City id";

    public final static String PREF_KEY_SELECTED_CITY_NAME = "Select City";


    public final static String PREF_KEY_SELECTED_LOCATION = "Select Location id";
    public final static String PREF_KEY_SELECTED_LOCATION_NAME = "Select Location";
    public final static String PREF_KEY_SELECTED_LOCATION_Apply_Hourly_Charges = "Select Apply Hourly charges";
    public final static String PREF_KEY_SELECTED_LOCATION_Hourly_Charges = "Select Location Hourly charges";

    public final static String PREF_KEY_USER_MOBILE = "user_mobile";

    public final static String PREY_KEY_INTENT_TYPE = "intent_type";
    public final static String PREY_KEY_USER_HOWIT_FLAG = "how_it_works";
    public final static String PREF_KEY_USER_STATUS = "user_status";
    public final static String PREF_KEY_USER_LAST_LOGIN = "user_last_login";
    public final static String PREF_KEY_USER_LAST_LOGOUT = "user_last_logout";
    public final static String PREF_KEY_USER_DEVICE_ID = "user_device_id";

    public final static String PREF_KEY_USER_LOCATION_TIMEZONE= "location_timezone";
    public final static String PREF_KEY_USER_FUTUREBOOKING_AFTERHOUR = "future_after_hrs";
    public final static String PREF_KEY_USER_FUTUREBOOKING_CURRENT_GMT = "current_gmt";
    public final static String PREF_KEY_USER_FUTUREBOOKING_BOOKING_PRICE = "future_bookprice";

    public final static String PREF_KEY_USER_WALLET_MONEY = "user_wallet_money";
//    public final static String PREF_KEY_USER_DEVICE_FCM_KEY = "fcm_key";
    public final static String PREF_KEY_USER_PROFILE_IMAGE = "user_profile_image";

    public final static String PREF_KEY_BOOKING_PRICE = "user_booking_price";
    public final static String PREF_KEY_BOOK_TYPE = "user_booktype";
    public final static String user_wallet_amount = "user_wallet_amount";
    public final static String user_free_parking = "user_free_parking";
    public final static String CURRENT_FRAGMENT = "CURRENT_FRAGMENT";
    public final static String CREDIT_CARD_STATUS = "CREDIT_CARD_STATUS";


}


