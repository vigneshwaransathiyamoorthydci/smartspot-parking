package com.smartspot.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.checkout.CardValidator;
import com.checkout.CheckoutKit;
import com.checkout.exceptions.CardException;
import com.checkout.exceptions.CheckoutException;
import com.checkout.httpconnector.Response;
import com.checkout.models.Card;
import com.checkout.models.CardToken;
import com.checkout.models.CardTokenResponse;
import com.smartspot.App;
import com.smartspot.R;
import com.smartspot.fragment.WalletFragment;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.BookingPojo;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


/**
 * Created by iyyapparajr on 8/31/2017.
 */

public class SlotsActivity extends Activity {

    TextView amount, slots_total_count, time_duration, amountString, okay_slot, slot_waitingNumber,slot_text;
    LinearLayout sots_available, slots_notavailable, slots_notavailableTextView;
    ImageView back;
    String CityId, LocationId;
    WS_CallService service_SearchSlots;
    ArrayList<NameValuePair> SearchSlots_NVP = new ArrayList<>();
    Search_Slot_WS search_Slot_WebService;

    ProgressDialog dialog;

    String availableslots_count="0", walletMoneybool, BookingId;
    String CloudBookingIdfin;

    ArrayList<BookingPojo> nonBookedArrList = new ArrayList<>();
    ArrayList<BookingPojo> BookedArrList = new ArrayList<>();

    String wsBarrierName, wsSlotNo;

    WS_CallService serviceafter_BlockSlots;
    ArrayList<NameValuePair> BlockSlotsafter_NVP = new ArrayList<>();
    Block_SlotAfter_WS block_Slotafter_WebService;


    WS_CallService service_ReserveSlots;
    ArrayList<NameValuePair> reserelots_NVP = new ArrayList<>();
    Reserve_Slot_WS reserve_Slot_WebService;


    WS_CallService service_Walletchecking;
    ArrayList<NameValuePair> walletChecking_NVP = new ArrayList<>();
    WalletChecking_WS walletChecking_Slot_WebService;


    WS_CallService service_BookSlotFromServer;
    ArrayList<NameValuePair> BookSlotFromServer_NVP = new ArrayList<>();
    BookSlotFromServer_WS BookSlotFromServer_Slot_WebService;
    SearchSlotFromServer_WS SearchSlotFromServer_Slot_WebService;

    WS_CallService service_IFMoneyInWallet;
    ArrayList<NameValuePair> IFMoneyInWallet_NVP = new ArrayList<>();
    IFMoneyWallet_WS IFMoneyInWallet_WebService;


    TextView block_spot, reserve_slot;

    private String publicKey = "pk_7b0fb574-55cb-4629-8e2c-e1cb3839763e";
    Resources res;
    WS_CallService service_BlockSlots;
    ArrayList<NameValuePair> BlockSlots_NVP = new ArrayList<>();
    Block_Slot_Begore_WS block_Slot_WebService;
    LinearLayout waiting_laypout;
    TextView slots_page_titile;
    String slotCount, SlotAmount, SlotTime, datetimeStr;

    String activity_back, refundStatus;

    BroadcastReceiver braodcastNewBoking;
//, bookReceiver
    String fcmToken;
    SharedPreferences shared;

    WS_CallService service_Refund;
    ArrayList<NameValuePair> Refund_NVP = new ArrayList<>();
    Refund_WebService refund_webService;
    String user_wallet_amount;
    int Apply_Hourly_Charges;
    int user_free_parking;
    int credit_card;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.slot_main);

        Log.e("Slots ", "Activity");


        shared = getSharedPreferences("FCM_Shared", MODE_PRIVATE);
        fcmToken = shared.getString(Preference_Constants.PREF_KEY_FCMTOKEN_NEW, "");


        back = (ImageView) findViewById(R.id.back);
        amount = (TextView) findViewById(R.id.slots_amount);
        slot_text = (TextView) findViewById(R.id.slot_text);

        slots_total_count = (TextView) findViewById(R.id.slots_total_count);
        time_duration = (TextView) findViewById(R.id.time_duration);

        waiting_laypout = (LinearLayout) findViewById(R.id.waiting_laypout);
        refundStatus = "NONEED";
        okay_slot = (TextView) findViewById(R.id.okay_slot);
        amountString = (TextView) findViewById(R.id.amount);
//      amount.setTextColor(Color.parseColor("#51d1d6"));
        activity_back = "NormalBack";

        slot_waitingNumber = (TextView) findViewById(R.id.slot_waitingNumber);
        CityId = getIntent().getStringExtra("CityId");
        LocationId = getIntent().getStringExtra("LocationId");
        datetimeStr = getIntent().getStringExtra("date");
        Apply_Hourly_Charges=getIntent().getIntExtra("Apply_Hourly_Charges",0);
        user_wallet_amount=getIntent().getStringExtra("user_wallet_amount");
        //user_free_parking= Integer.parseInt((getIntent().getStringExtra("user_free_parking")));
        Log.d("Vignesh","datas"+Apply_Hourly_Charges+user_wallet_amount+user_free_parking);
//        hitSearchService();


        braodcastNewBoking = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

            }
        };
        IntentFilter intentIndexPassi = new IntentFilter("BackFinish");
        registerReceiver(braodcastNewBoking, intentIndexPassi);

        amountString.setText("Aed");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("SATURDAY","refund"+refundStatus);
                if (refundStatus.equalsIgnoreCase("NONEED")) {
                    finish();
                } else {
                    if (user_free_parking==0)

                    {
                        hitRefundWalletService();
                    }
                    else if (Apply_Hourly_Charges==1)
                    {
                        hitRefundWalletService();
                    }


                    finish();

                }

            }
        });
        block_spot = (TextView) findViewById(R.id.block_spot);
        walletMoneybool = "NONE";

//      block_spot = (TextView) findViewById(R.id.block_spot);
        reserve_slot = (TextView) findViewById(R.id.reserve_slot);
        sots_available = (LinearLayout) findViewById(R.id.sots_available);
        slots_notavailable = (LinearLayout) findViewById(R.id.slots_notavailable);
        slots_page_titile = (TextView) findViewById(R.id.slots_page_titile);

        slots_notavailableTextView = (LinearLayout) findViewById(R.id.slots_notavailableTextView);

        block_spot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                sots_available.setVisibility(View.GONE);
//                slots_notavailable.setVisibility(View.VISIBLE);
//                hitBlockBeforeSlotService();
//                animateButtonAndRevert(block_spot,
//                        ContextCompat.getColor(SlotsActivity.this, R.color.black),
//                BitmapFactory.decodeResource(res,R.drawable.logo));
                activity_back = "Blocked";
                Log.d("saturday","walletMoneybool"+walletMoneybool);
                if (walletMoneybool.equalsIgnoreCase("YES")) {
                    if (isInternetOn())
                        HiCloudBookService();
                }
                else if (walletMoneybool.equalsIgnoreCase("CRE"))
                    {
                        if (isInternetOn())
                            hitBlockBeforeSlotService();
                    }

                 else {
                    Intent addmoneyIntent = new Intent(SlotsActivity.this, WalletActivity.class);
                    startActivity(addmoneyIntent);
                }


            }
        });


        reserve_slot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                slots_notavailable.setVisibility(View.GONE);
//                sots_available.setVisibility(View.VISIBLE);
//                reserve_slot.startAnimation();

                final Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.smartspot_logo);
                if(isInternetOn())
                hitReservationService();
            }
        });

        ObjectAnimator reserveDown = ObjectAnimator.ofPropertyValuesHolder(
                reserve_slot,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        reserveDown.setDuration(310);

        reserveDown.setRepeatCount(ObjectAnimator.INFINITE);
        reserveDown.setRepeatMode(ObjectAnimator.REVERSE);

        reserveDown.start();


        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                okay_slot,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();


        okay_slot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog1 = new Dialog(SlotsActivity.this);
                if (Build.VERSION.SDK_INT < 16) {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.pop_up_dialog);
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);
                TextView text = (TextView) dialog1.findViewById(R.id.text);
                //text.setText("You are waitlist number " + slot_waitingNumber.getText().toString() + "  We are trying to get u a spot ASAP.");
                text.setText("Your spot will be reserved smartly ASAP!");
                text.setTextColor(Color.parseColor("#000000"));
                dialog1.show();

                TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                okay_popup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();

                        finish();


                    }
                });


            }
        });


//        hitWalletcheking();
        //LoginData();
        //SearchSlot();
        if(isInternetOn())
        {
            if (Apply_Hourly_Charges==1)
            {
                hitWalletcheking();
            }
            else if (Apply_Hourly_Charges==0)
            {
                hitWalletcheking();

            }


        }



    }


    private void hitBlockBeforeSlotService() {

        BlockSlots_NVP= new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
//            String login_string = "Function:Block a slot|amount:" + SlotAmount + "|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID) + "|location_id:" + LocationId + "|trans_detail:123|payment_status:1";
            String login_string = "Function:BookSlotBeforPay|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID) + "|amount:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE)+"|location_id:" + LocationId + "|city_id:" + CityId + "|barrier_name:" + wsBarrierName + "|barrier_id:|slot_id:" + wsSlotNo+"|dummyParam:1"+"|credit_card:"+credit_card;

            System.out.println(Preference_Constants.PREF_KEY_Service + " service  -->" + login_string);

            Log.e("Webservice", "BeforePay" + login_string);
            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_BlockSlots = new WS_CallService(SlotsActivity.this);
            BlockSlots_NVP.add(new BasicNameValuePair("WS", base64_register));
            block_Slot_WebService = new Block_Slot_Begore_WS(SlotsActivity.this, BlockSlots_NVP);
            block_Slot_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void hitWalletcheking() {

        walletChecking_NVP=new ArrayList<>();
        byte[] data;
        try {
//            String login_string = "Function:Block a slot|amount:" + SlotAmount + "|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID) + "|location_id:" + LocationId + "|trans_detail:123|payment_status:1";
            String login_string = "Function:updateSlot|locationid:" + LocationId + "|cityid:" + CityId + "|available:" + availableslots_count + "|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID) + "|fcm_key:" + AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW) + "|device_type:2";

            Log.e("FCM", "Token" + login_string);

            System.out.println(Preference_Constants.PREF_KEY_Service + "UpdateSlot service  -->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_Walletchecking = new WS_CallService(SlotsActivity.this);
            walletChecking_NVP.add(new BasicNameValuePair("WS", base64_register));
            walletChecking_Slot_WebService = new WalletChecking_WS(SlotsActivity.this, walletChecking_NVP);
            walletChecking_Slot_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void hitRefundWalletService() {

        Refund_NVP=new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:updateWallet|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID) + "|type:"+4+"|amount:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE)+"|hrly_charge:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION);
//            String login_string = "Function:BookSlotBeforPay|user_id:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)+"|amount:"+SlotAmount+"|location_id:"+LocationId+"|city_id:"+CityId;
            Log.e("Service", "Function:updateWallet" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_Refund = new WS_CallService(SlotsActivity.this);
            Refund_NVP.add(new BasicNameValuePair("WS", base64_register));
            refund_webService = new Refund_WebService(SlotsActivity.this, Refund_NVP);
            refund_webService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void hitIfMoneyInWalletService() {

        IFMoneyInWallet_NVP=new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:updateWallet|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)
                    + "|type:"+2+"|amount:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE)+"|hrly_charge:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION);

//           String login_string = "Function:BookSlotBeforPay|user_id:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)+"|amount:"+SlotAmount+"|location_id:"+LocationId+"|city_id:"+CityId;

            Log.e("Service", "Function:updateWallet" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_IFMoneyInWallet = new WS_CallService(SlotsActivity.this);
            IFMoneyInWallet_NVP.add(new BasicNameValuePair("WS", base64_register));
            IFMoneyInWallet_WebService = new IFMoneyWallet_WS(SlotsActivity.this, IFMoneyInWallet_NVP);
            IFMoneyInWallet_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void hitReservationService() {

        reserelots_NVP= new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Reserve slot|location_id:" + LocationId + "|city_id:" + CityId + "|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println(Preference_Constants.PREF_KEY_Service + " service  -->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_ReserveSlots = new WS_CallService(SlotsActivity.this);
            reserelots_NVP.add(new BasicNameValuePair("WS", base64_register));
            reserve_Slot_WebService = new Reserve_Slot_WS(SlotsActivity.this, reserelots_NVP);
            reserve_Slot_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void hitBookingServierFinalWS() {

        ArrayList<NameValuePair> SearchSlotFromServer_NVP = new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Block a slot|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID) + "|booking_id:" + CloudBookingIdfin + "|trans_detail:wallet|payment_status:1";
            System.out.println(Preference_Constants.PREF_KEY_Service + " service  -->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_BookSlotFromServer = new WS_CallService(SlotsActivity.this);
            SearchSlotFromServer_NVP.add(new BasicNameValuePair("WS", base64_register));
            BookSlotFromServer_Slot_WebService = new BookSlotFromServer_WS(SlotsActivity.this, SearchSlotFromServer_NVP);
            BookSlotFromServer_Slot_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void SearchSlot() {
        BookSlotFromServer_NVP= new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Search Slot|city_id:" + CityId + "|location_id:" + LocationId;
            System.out.println(Preference_Constants.PREF_KEY_Service + " SearchSlotparam  -->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_BookSlotFromServer = new WS_CallService(SlotsActivity.this);
            BookSlotFromServer_NVP.add(new BasicNameValuePair("WS", base64_register));
            SearchSlotFromServer_Slot_WebService = new SearchSlotFromServer_WS(SlotsActivity.this, BookSlotFromServer_NVP);
            SearchSlotFromServer_Slot_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class SearchSlotFromServer_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public SearchSlotFromServer_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            System.out.println("POST EXECUTE");
            dialog.dismiss();
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                Log.e("If Monye", "SearchSlotResponse" + jsonResponse);

                String status = jObj.getString("Status");

                if (status.equalsIgnoreCase("Success")) {
                    if (jObj.has("SlotCount"))
                        availableslots_count = jObj.getString("SlotCount");

                    if (jObj.has("SlotTime"))
                        SlotTime = jObj.getString("SlotTime");

                    if (jObj.has("SlotAmount"))
                        SlotAmount = jObj.getString("SlotAmount");

                    Log.e("Availble Slots ", "Count" + availableslots_count);

                    if (availableslots_count.equalsIgnoreCase("0") || availableslots_count.equalsIgnoreCase("")) {
//                            slots_page_titile.setText("RESERVE YOUR SPOT");
//                            slots_notavailable.setVisibility(View.VISIBLE);

                        refundStatus = "NONEED";
                        hitWalletcheking();

                    } else {
                        //  hitBlockBeforeSlotService();
                        hitWalletcheking();
                        refundStatus = "NEED";
//                            sots_available.setVisibility(View.VISIBLE);
//                            slots_page_titile.setText("BOOK MY SPOT");
//                            slots_total_count.setText(availableslots_count);
                    }

                    Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                } else {
                    refundStatus = "NONEED";
                    hitWalletcheking();

                    //   Toast.makeText(getApplicationContext(),status,Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");

                final Dialog dialog1 = new Dialog(SlotsActivity.this);
                if (Build.VERSION.SDK_INT < 16) {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.pop_up_dialog);
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);

                TextView text = (TextView) dialog1.findViewById(R.id.text);
                text.setText("Error on Server. Please try again later");
                // text.setTextColor(Color.parseColor("#ffffff"));
                dialog1.show();

                TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                okay_popup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();
                        finish();
                    }
                });

            }

        }

    }


    public void LoginData() {
        ArrayList<NameValuePair> loadvalue = new ArrayList<NameValuePair>();
        byte[] data;

        try {
//            Log.e("username",""+email.getText().toString());
//            Log.e("password", ""+password.getText().toString());
            loadvalue.add(new BasicNameValuePair("api_key", "srijanVADS2S8JJ126FFKN4LL9VZ64C0NYW934"));

            LoadTask loadTask = new LoadTask(getApplicationContext(), loadvalue);
            loadTask.execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void HiCloudBookService() {
        ArrayList<NameValuePair> loadvalue = new ArrayList<NameValuePair>();
        byte[] data;

        try {

//            Log.e("username",""+email.getText().toString());
//            Log.e("password", ""+password.getText().toString());
            loadvalue.add(new BasicNameValuePair("api_key", "srijanVADS2S8JJ126FFKN4LL9VZ64C0NYW934"));
            loadvalue.add(new BasicNameValuePair("no_of_slots", "1"));
            loadvalue.add(new BasicNameValuePair("user_id", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)));

            CloundCodeGEtting loadTask = new CloundCodeGEtting(getApplicationContext(), loadvalue);
            loadTask.execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private class LoadTask extends AsyncTask<String, Void, String> {
        String response;
        ProgressDialog dia;
        Dialog dialog;
        String jsonResponse;
        Context context_aact;
        ArrayList<NameValuePair> loadValue = new ArrayList<NameValuePair>();

        public LoadTask(Context context_ws, ArrayList<NameValuePair> loadValue) {
            // TODO Auto-generated constructor stub
            this.loadValue = loadValue;
            this.context_aact = context_ws;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//          isInternetOn();

            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
//            dialog.setTitle("Title...");
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();


        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                WS_CallService service2 = new WS_CallService(context_aact);
                jsonResponse = service2.getJSONObjestString(loadValue,
                        "http://52.24.121.21:9000/parking/api/city/" + CityId + "/location/" + LocationId + "/slots");
                Log.e("City id -->", "Location id" + CityId + "and " + LocationId);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponse;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.e("jsonResponse", "Bookslot" + result);
//            Toast.makeText(getApplicationContext(),"Response : "+result,Toast.LENGTH_LONG).show();

            if (result != null) {
                try {
                    JSONObject object = new JSONObject(result);

                    String statusString = "";
                    if (object.has("ResponseCode"))
                        statusString = object.getString("ResponseCode");

                    Log.e("Cloud checking ", "Count" + result);
                    dialog.dismiss();
                    if (statusString.equalsIgnoreCase("200")) {

                        availableslots_count = object.getJSONObject("Data").getString("AvailableSlots");
                        Log.e("Availble Slots ", "Count" + availableslots_count);
                        if (availableslots_count.equalsIgnoreCase("0") || availableslots_count.equalsIgnoreCase("")) {
//                            slots_page_titile.setText("RESERVE YOUR SPOT");
//                            slots_notavailable.setVisibility(View.VISIBLE);

                            refundStatus = "NONEED";
                            hitWalletcheking();

                        } else {

                            //  hitBlockBeforeSlotService();

                            hitWalletcheking();
                            refundStatus = "NEED";
//                            sots_available.setVisibility(View.VISIBLE);
//                            slots_page_titile.setText("BOOK MY SPOT");
//                            slots_total_count.setText(availableslots_count);
                        }


                    } else {
//                        slots_notavailable.setVisibility(View.VISIBLE);

                        //Toast.makeText(getApplicationContext(),"Error on server",Toast.LENGTH_LONG).show();


                        final Dialog dialog1 = new Dialog(SlotsActivity.this);
                        if (Build.VERSION.SDK_INT < 16) {
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        }
                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog1.setContentView(R.layout.pop_up_dialog);
                        dialog1.setCancelable(false);
                        dialog1.setCanceledOnTouchOutside(false);

                        TextView text = (TextView) dialog1.findViewById(R.id.text);
                        text.setText("Error on Server. Please try again later");
                        // text.setTextColor(Color.parseColor("#ffffff"));
                        dialog1.show();

                        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                        okay_popup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog1.dismiss();
                                finish();
                            }
                        });


                    }

                } catch (Exception e) {
                    Log.e("exception", e.toString());

                }
            } else if (result == null) {
            }
            //   Toast.makeText(getActivity().getApplicationContext(),"email"+result,Toast.LENGTH_LONG).show();

        }//close onPostExecute
    }


    private class CloundCodeGEtting extends AsyncTask<String, Void, String> {
        String response;
        ProgressDialog dia;
        Dialog dialog;
        String jsonResponse;
        Context context_aact;
        ArrayList<NameValuePair> loadValue = new ArrayList<NameValuePair>();

        public CloundCodeGEtting(Context context_ws, ArrayList<NameValuePair> loadValue) {
            // TODO Auto-generated constructor stub
            this.loadValue = loadValue;
            this.context_aact = context_ws;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
//          dialog.setTitle("Title...");
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                WS_CallService service2 = new WS_CallService(context_aact);
                jsonResponse = service2.getJSONObjestString(loadValue,
                        "http://52.24.121.21:9000/parking/api/city/" + CityId + "/location/" + LocationId + "/slots/book");
                Log.e("City id --> ", "Location id" + CityId + "and in Book service " + LocationId);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponse;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.e("jsonResponse", "Book Cloud" + result);

//            Toast.makeText(getApplicationContext(),"Response : "+result,Toast.LENGTH_LONG).show();
            try {
                dialog.dismiss();
            }catch (Exception e){}

            if (result != null) {
                try {
                    JSONObject object = new JSONObject(result);

                    String statusString = "";
                    if (object.has("ResponseCode"))
                        statusString = object.getString("ResponseCode");

                    if (statusString.equalsIgnoreCase("200")) {
                        wsBarrierName = object.getJSONObject("Data").getJSONArray("BookedSlots").getJSONObject(0).getString("barrier_name");
                        wsSlotNo = object.getJSONObject("Data").getJSONArray("BookedSlots").getJSONObject(0).getString("slot_no");
                        if (walletMoneybool.equalsIgnoreCase("CRE"))
                        {
                            final Dialog SuccessDialogBox = new Dialog(SlotsActivity.this);
                            if (Build.VERSION.SDK_INT < 16) {
                                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                            }
                            SuccessDialogBox.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            SuccessDialogBox.setContentView(R.layout.pop_up_dialog);
                            SuccessDialogBox.setCancelable(false);
                            SuccessDialogBox.setCanceledOnTouchOutside(false);

                            TextView text = (TextView) SuccessDialogBox.findViewById(R.id.text);
//                  text.setText("Congratulations ! Your spot is ready. Kindly proceed to  "+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME)+" at "+wsSlotNo+". Your spot is waiting for you."/*" to spot number "+wsSlotNo+" and tap down barrier icon to lower barrier"*/);
                            text.setText("Congratulations! Your spot number is " + wsBarrierName + ". Go to My Bookings when ready to park.");
                            text.setTextColor(Color.parseColor("#000000"));


                            TextView okay_popup = (TextView) SuccessDialogBox.findViewById(R.id.okay_popup);
                            okay_popup.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    SuccessDialogBox.dismiss();
                                    finish();
                                    Intent in = new Intent("BookSuccess");
                                    getApplicationContext().sendBroadcast(in);
                                    // Intent in = new Intent("BookingSuccess");
                                    //  getApplicationContext().sendBroadcast(in);
                                }
                            });

                            try {
                                SuccessDialogBox.show();
                            } catch (Exception e) {
                            }

                        }
                        else {
                            hitBlockBeforeSlotService();
                            }


                        AppPreferences.putStringIntoStore("BAckPressed", "FromSlots");


//                                AppPreferences.putStringIntoStore("BAckPressed","3");
//                                Intent broadcastIntent = new Intent("IndexPassingToFragment");
//                                broadcastIntent.putExtra("Data","3");
//                                sendBroadcast(broadcastIntent);
//                                finish();


                    } else {
                        refundStatus = "NONEED";
                        if (user_free_parking==0)

                        {
                            hitRefundWalletService();
                        }
                        else if (Apply_Hourly_Charges==1)
                        {
                            hitRefundWalletService();
                        }
                        //  Toast.makeText(SlotsActivity.this,"Response code is not success",Toast.LENGTH_LONG).show();
                        final Dialog dialog1 = new Dialog(SlotsActivity.this);
                        if (Build.VERSION.SDK_INT < 16) {
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        }
                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog1.setContentView(R.layout.pop_up_dialog);
                        dialog1.setCancelable(false);
                        dialog1.setCanceledOnTouchOutside(false);

                        TextView text = (TextView) dialog1.findViewById(R.id.text);
                        text.setText("Please try again later.");
                        text.setTextColor(Color.parseColor("#000000"));
                        dialog1.show();

                        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                        okay_popup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog1.dismiss();
                                finish();
                            }
                        });
                    }

                    Log.e("Cloud Code  ", "Getiing " + result);
                    dialog.dismiss();

                } catch (Exception e) {
                    Log.e("exception", e.toString());

                }
            } else if (result == null) {
                refundStatus = "NONEED";
                if (user_free_parking==0 )

                {
                    hitRefundWalletService();
                }
                else if (Apply_Hourly_Charges==1)
                {
                    hitRefundWalletService();
                }
                final Dialog dialog1 = new Dialog(SlotsActivity.this);
                if (Build.VERSION.SDK_INT < 16) {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FULLSCREEN);

                }
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.pop_up_dialog);
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);

                TextView text = (TextView) dialog1.findViewById(R.id.text);
                text.setText("Please try again later.");
                text.setTextColor(Color.parseColor("#000000"));
                dialog1.show();
                TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                okay_popup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();
                        finish();
                    }
                });

                Toast.makeText(SlotsActivity.this, "Response " + result, Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
            // Toast.makeText(getActivity().getApplicationContext(),"email"+result,Toast.LENGTH_LONG).show();

        }
    }

    public class BookSlotFromServer_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public BookSlotFromServer_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            System.out.println("POST EXECUTE");
            dialog.dismiss();
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                Log.e("If Monye", "Response" + jsonResponse);

                String status = jObj.getString("Status");

                if (status.equalsIgnoreCase("Success")) {


                    final Dialog dialog1 = new Dialog(SlotsActivity.this);
                    if (Build.VERSION.SDK_INT < 16) {
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    }
                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog1.setContentView(R.layout.pop_up_dialog);
                    dialog1.setCancelable(false);
                    dialog1.setCanceledOnTouchOutside(false);

                    TextView text = (TextView) dialog1.findViewById(R.id.text);
//                  text.setText("Congratulations ! Your spot is ready. Kindly proceed to  "+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME)+" at "+wsSlotNo+". Your spot is waiting for you."/*" to spot number "+wsSlotNo+" and tap down barrier icon to lower barrier"*/);
                    text.setText("Congratulations! Your spot number is " + wsBarrierName + ". Go to My Bookings when ready to park.");
                    text.setTextColor(Color.parseColor("#000000"));
                    dialog1.show();

                    TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                    okay_popup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog1.dismiss();

//                                Intent broadcastIntent = new Intent("IndexPassing");
////                broadcastIntent.setAction("CreateGroupFollower");
//                                broadcastIntent.putExtra("Data","3");
//                                sendBroadcast(broadcastIntent);
                            finish();
                        }
                    });


                    Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),"Please try again later.",Toast.LENGTH_LONG).show();
                System.out.println(e.toString() + "zcx");
            }

        }

    }


    public class IFMoneyWallet_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public IFMoneyWallet_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            System.out.println("POST EXECUTE");
            dialog.dismiss();
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                Log.e("If Monye", "UpdateWallet" + jsonResponse);

                String status = jObj.getString("Status");

                if (status.equalsIgnoreCase("Success")) {
//                        HiCloudBookService();
                } else {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    public class Search_Slot_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public Search_Slot_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(SlotsActivity.this);
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();
            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();


//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();Book
            System.out.println("POST EXECUTE");
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service + "Slots Values-->" + jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {


                    sots_available.setVisibility(View.VISIBLE);
                    slots_notavailable.setVisibility(View.GONE);


                    slotCount = jObj.getString("SlotCount");
                    SlotAmount = jObj.getString("SlotAmount");
                    SlotTime = jObj.getString("SlotTime");


                    slots_total_count.setText(slotCount);
                    amount.setText(SlotAmount);
                    time_duration.setText(SlotTime);


                    System.out.println("success");
                } else {
                    System.out.println("failed");

                    String Message = jObj.getString("Message");
//                    String Message = jObj.getString("Message");
                    sots_available.setVisibility(View.GONE);
                    slots_notavailable.setVisibility(View.VISIBLE);
                }


//                hitLocationService();

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    public class WalletChecking_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public WalletChecking_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();


//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//          CityArrayList.clear();

            try {
                dialog.dismiss();
            } catch (Exception e) {

            }

            Log.e("jsonResponse", "UpdateSlot" + jsonResponse);

            if (jsonResponse!=null) {

                try {
                    JSONObject jObj = new JSONObject(jsonResponse);
                    System.out.println(Preference_Constants.PREF_KEY_Service + "Slots Values-->" + jsonResponse);
                    Log.e("after availabale", "Wallet Checking " + jsonResponse);

                    String status = jObj.getString("Status");
                    if (status.toString().equalsIgnoreCase("Success")) {

                        String walletAmount = jObj.getString("user_wallet_amount");
                        int WalletAmt= Integer.parseInt(walletAmount);
                        String bkPrice = jObj.getString("BookingPrice");
                        user_free_parking= Integer.parseInt(jObj.getString("user_free_parking"));
                        AppPreferences.putStringIntoStore(Preference_Constants.user_free_parking, String.valueOf(user_free_parking));
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY, walletAmount);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_BOOKING_PRICE, bkPrice);
                        Intent in = new Intent("amtReceiver");
                        getApplicationContext().sendBroadcast(in);
                        Log.e("BookPrice","->"+bkPrice);

                        int bookprice = 0;
                        if (AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE).equalsIgnoreCase("")) {

                        } else {
                            bookprice = Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE));

                        }
                        int price=Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_Hourly_Charges));
                        int creditCardsyncStatus= Integer.parseInt(jObj.getString("CreditCard"));
                        AppPreferences.putStringIntoStore(Preference_Constants.CREDIT_CARD_STATUS, String.valueOf(creditCardsyncStatus));
                        Log.d("Monday","data"+walletAmount+price);


                        String spot_available_count = jObj.getString("spot_available_count");
                        int spotacail= Integer.parseInt(spot_available_count);
                        if (spotacail!=0) {
                            if (Apply_Hourly_Charges == 1) {

                                if (WalletAmt<0 || Integer.parseInt(walletAmount) < price) {
                                    if (creditCardsyncStatus == 0) {
                                        walletMoneybool = "NO";
                                        refundStatus = "NONEED";
                                        final Dialog dialog1 = new Dialog(SlotsActivity.this);
                                        if (Build.VERSION.SDK_INT < 16) {
                                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

                                        }
                                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog1.setContentView(R.layout.pop_up_dialog);
                                        dialog1.setCancelable(false);
                                        dialog1.setCanceledOnTouchOutside(false);

                                        TextView text = (TextView) dialog1.findViewById(R.id.text);
                                        text.setText("There is no balance.. add balance and proceed with booking.");


                                        text.setTextColor(Color.parseColor("#000000"));
                                        dialog1.show();
                                        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                                        okay_popup.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                finish();
                                                dialog1.dismiss();
                                                Intent in = new Intent("CurrentWalletReceiver");
                                                getApplicationContext().sendBroadcast(in);

                                            }
                                        });
                                    } else {
                                        walletMoneybool = "YES";
                                        credit_card = 1;

                                        if (spot_available_count.equalsIgnoreCase("0")) {
                                            refundStatus = "NONEED";
                                            slots_page_titile.setText("RESERVE YOUR SPOT");
                                            slots_notavailable.setVisibility(View.VISIBLE);
                                        } else {
                                            refundStatus = "NONEED";
                                            slot_text.setText("Your Parking spot will be reserved for a period of 1 hour for Aed " + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE) + " awaiting your arrival. Once you arrive, you are free to park for as long as you wish as per the location's guidelines and charges.");
                                            //hitIfMoneyInWalletService();
                                            sots_available.setVisibility(View.VISIBLE);
                                            slots_page_titile.setText("BOOK MY SPOT");
                                            slots_total_count.setText(spot_available_count);
                                            block_spot.setText("BOOK");

                                        }
                                    }


                                } else {
                                    walletMoneybool = "YES";
                                    credit_card = 0;
                                    if (spot_available_count.equalsIgnoreCase("0")) {
                                        refundStatus = "NONEED";
                                        slots_page_titile.setText("RESERVE YOUR SPOT");
                                        slots_notavailable.setVisibility(View.VISIBLE);
                                    } else {

                                        refundStatus = "NEED";
                                        slot_text.setText("Your Parking spot will be reserved for a period of 1 hour for Aed " + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE) + " awaiting your arrival. Once you arrive, you are free to park for as long as you wish as per the location's guidelines and charges.");
                                        hitIfMoneyInWalletService();
                                        sots_available.setVisibility(View.VISIBLE);
                                        slots_page_titile.setText("BOOK MY SPOT");
                                        slots_total_count.setText(spot_available_count);
                                        block_spot.setText("BOOK");

                                    }

                                }
                            } else if (Apply_Hourly_Charges == 0) {

                                if (user_free_parking >0) {
                                    //hitWalletcheking();

                                    walletMoneybool = "YES";
                                    refundStatus = "NEED";
                                    slot_text.setText("Your Parking spot will be reserved for free , your avail free parking" + " " + user_free_parking);
                                    hitIfMoneyInWalletService();
                                    sots_available.setVisibility(View.VISIBLE);
                                    slots_page_titile.setText("BOOK MY SPOT");
                                    slots_total_count.setText(spot_available_count);
                                    block_spot.setText("BOOK");
                                    credit_card = 0;
                                } else {
                                    if (WalletAmt<0 || Integer.parseInt(walletAmount) < price) {
                                        if (creditCardsyncStatus == 0) {
                                            walletMoneybool = "NO";
                                            refundStatus = "NONEED";
                                            final Dialog dialog1 = new Dialog(SlotsActivity.this);
                                            if (Build.VERSION.SDK_INT < 16) {
                                                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                                        WindowManager.LayoutParams.FLAG_FULLSCREEN);

                                            }
                                            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog1.setContentView(R.layout.pop_up_dialog);
                                            dialog1.setCancelable(false);
                                            dialog1.setCanceledOnTouchOutside(false);

                                            TextView text = (TextView) dialog1.findViewById(R.id.text);
                                            text.setText("There is no balance... add balance and proceed with booking.");


                                            text.setTextColor(Color.parseColor("#000000"));
                                            dialog1.show();
                                            TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                                            okay_popup.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    finish();
                                                    dialog1.dismiss();
                                                    Intent in = new Intent("CurrentWalletReceiver");
                                                    getApplicationContext().sendBroadcast(in);

                                                }
                                            });


                                        } else {
                                            credit_card = 1;
                                            walletMoneybool = "YES";
                                            refundStatus = "NONEED";
                                            slot_text.setText("Your Parking spot will be reserved for a period of 1 hour for Aed " + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE) + " awaiting your arrival. Once you arrive, you are free to park for as long as you wish as per the location's guidelines and charges.");
                                            //hitIfMoneyInWalletService();
                                            sots_available.setVisibility(View.VISIBLE);
                                            slots_page_titile.setText("BOOK MY SPOT");
                                            slots_total_count.setText(spot_available_count);
                                            block_spot.setText("BOOK");
                                        }

                                    } else {
                                        credit_card = 0;
                                        walletMoneybool = "YES";
                                        refundStatus = "NEED";
                                        slot_text.setText("Your Parking spot will be reserved for a period of 1 hour for Aed " + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_BOOKING_PRICE) + " awaiting your arrival. Once you arrive, you are free to park for as long as you wish as per the location's guidelines and charges.");
                                        hitIfMoneyInWalletService();
                                        sots_available.setVisibility(View.VISIBLE);
                                        slots_page_titile.setText("BOOK MY SPOT");
                                        slots_total_count.setText(spot_available_count);
                                        block_spot.setText("BOOK");
                                    }


                                }


                            }
                        }
                        else
                        {
                            refundStatus = "NONEED";
                            slots_page_titile.setText("RESERVE YOUR SPOT");
                            slots_notavailable.setVisibility(View.VISIBLE);
                        }


                        System.out.println("success");
                    } else {
                        System.out.println("failed");
                        refundStatus = "NONEED";
                        String Message = jObj.getString("Message");
                        //Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_LONG).show();
                        sots_available.setVisibility(View.GONE);
                        //slots_notavailable.setVisibility(View.VISIBLE);

                        if(jObj.getString("Message").equalsIgnoreCase("SmartSpot is coming soon to this location. Please check in soon. Thank you")) {
                            PopupMessage("SmartSpot is coming soon to this location. Please check in soon. Thank you");
                        }else{
                            PopupMessage("Something went wrong. Please try again");
                        }

                        //  Toast.makeText(SlotsActivity.this, "Response " + jsonResponse, Toast.LENGTH_LONG).show();
                       // dialog.dismiss();
                    }
                    //                hitLocationService();

                } catch (Exception e) {
                    PopupMessage("Please try again later.");
                    System.out.println(e.toString() + "zcx");
                }
            } else {
                PopupMessage("Please check your Data/WiFi connection.");

              //  Toast.makeText(SlotsActivity.this, "Response " + jsonResponse, Toast.LENGTH_LONG).show();
              //  dialog.dismiss();
            }
        }

    }
    public void PopupMessage(final String msgText){
        final Dialog dialog1 = new Dialog(SlotsActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView text = (TextView) dialog1.findViewById(R.id.text);
        text.setText(""+msgText);
        text.setTextColor(Color.parseColor("#000000"));
        dialog1.show();
        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dialog1.dismiss();
                //finish();

                Intent addmoneyIntent = new Intent(SlotsActivity.this, MainActivity.class);
                addmoneyIntent.putExtra("open","wallet");
                startActivity(addmoneyIntent);
            }
        });

    }

    public class Block_Slot_Begore_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;
        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //ProgressDialog dialog;
        Dialog dialog;
        Context context_aact;

        public Block_Slot_Begore_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            try {
                dialog.dismiss();
            }catch (Exception e){}
            System.out.println("POST EXECUTE");
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service + "Slots Values-->" + jsonResponse);
                Log.e("Slots Before Pay", "Service" + jsonResponse);
                Log.d("Monday","Service"+jsonResponse);


                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {
//                    CloudBookingIdfin = jObj.getString("BookingId");

//                    Toast.makeText(SlotsActivity.this,"Slot Id is"+CloudBookingIdfin,Toast.LENGTH_LONG).show();

//                    hitBookingServierFinalWS();
                    if (walletMoneybool.equalsIgnoreCase("CRE"))
                    {
                        Log.d("Monday","CRE");
                        HiCloudBookService();
                    }
                    else {
                        final Dialog SuccessDialogBox = new Dialog(SlotsActivity.this);
                        if (Build.VERSION.SDK_INT < 16) {
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        }
                        SuccessDialogBox.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        SuccessDialogBox.setContentView(R.layout.pop_up_dialog);
                        SuccessDialogBox.setCancelable(false);
                        SuccessDialogBox.setCanceledOnTouchOutside(false);

                        TextView text = (TextView) SuccessDialogBox.findViewById(R.id.text);
//                  text.setText("Congratulations ! Your spot is ready. Kindly proceed to  "+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION_NAME)+" at "+wsSlotNo+". Your spot is waiting for you."/*" to spot number "+wsSlotNo+" and tap down barrier icon to lower barrier"*/);
                        text.setText("Congratulations! Your spot number is " + wsBarrierName + ". Go to My Bookings when ready to park.");
                        text.setTextColor(Color.parseColor("#000000"));


                        TextView okay_popup = (TextView) SuccessDialogBox.findViewById(R.id.okay_popup);
                        okay_popup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Log.d("WEDNESDAY","after");
                                SuccessDialogBox.dismiss();

                                Intent in = new Intent("BookSuccess");
                                getApplicationContext().sendBroadcast(in);
                                finish();
                                // Intent in = new Intent("BookingSuccess");
                                //  getApplicationContext().sendBroadcast(in);
                            }
                        });

                        try {
                            SuccessDialogBox.show();
                        } catch (Exception e) {
                        }
                    }
//                    OkayDialog("Congratulations! Your spot number is "+wsBarrierName+". Goto My Bookings when ready to park.");
//                    new ConnectionTask().execute("");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");
                   //Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_LONG).show();
                    PopupMessage(Message);
//                    OkayDialog(Message);
                }


//                hitLocationService();

//                try {
//
//                } catch (Exception e) {
//
//                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    class ConnectionTask extends AsyncTask<String, Void, String> {

        final EditText name = (EditText) findViewById(R.id.name);
        final EditText numberField = (EditText) findViewById(R.id.number);
        final EditText cvvField = (EditText) findViewById(R.id.cvv);
        final Spinner spinMonth = (Spinner) findViewById(R.id.spinnerMonth);
        final Spinner spinYear = (Spinner) findViewById(R.id.spinnerYear);
        final int errorColor = Color.rgb(204, 0, 51);

        private boolean validateCardFields(final String number, final String month, final String year, final String cvv) {
            boolean error = false;
            clearFieldsError();

            if (!CardValidator.validateCardNumber(number)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        numberField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            if (!CardValidator.validateExpiryDate(month, year)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinMonth.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                        spinYear.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            if (cvv.equals("")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cvvField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                error = true;
            }
            return !error;
        }

        private void clearFieldsError() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cvvField.getBackground().clearColorFilter();
                    spinMonth.getBackground().clearColorFilter();
                    spinYear.getBackground().clearColorFilter();
                    numberField.getBackground().clearColorFilter();
                }
            });
        }

        @Override
        protected String doInBackground(String... urls) {

            if (validateCardFields(numberField.getText().toString(), spinMonth.getSelectedItem().toString(), spinYear.getSelectedItem().toString(), cvvField.getText().toString())) {
                clearFieldsError();
                try {
                    Card card = new Card(numberField.getText().toString(), name.getText().toString(), spinMonth.getSelectedItem().toString(), spinYear.getSelectedItem().toString(), cvvField.getText().toString());
                    CheckoutKit ck = CheckoutKit.getInstance(publicKey);
                    final Response<CardTokenResponse> resp = ck.createCardToken(card);
                    if (resp.hasError) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                goToError();
                                dialog.show();
                            }
                        });
                    } else {
                        CardToken ct = resp.model.getCard();

                        resp.model.getJson();
//                        goToSuccess();
                        dialog.show();
                        return resp.model.getCardToken();
                    }
                } catch (final CardException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (e.getType().equals(CardException.CardExceptionType.INVALID_CVV)) {
                                cvvField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            } else if (e.getType().equals(CardException.CardExceptionType.INVALID_EXPIRY_DATE)) {
                                spinMonth.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                                spinYear.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            } else if (e.getType().equals(CardException.CardExceptionType.INVALID_NUMBER)) {
                                numberField.getBackground().setColorFilter(errorColor, PorterDuff.Mode.SRC_ATOP);
                            }
                        }
                    });
                } catch (CheckoutException | IOException e2) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            goToError();
                            dialog.show();
                        }
                    });
                }
            }
            return "";
        }

    }


    public class Block_SlotAfter_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public Block_SlotAfter_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this).load(R.drawable.loading).asGif().placeholder(R.drawable.loading).crossFade().into(image);
            dialog.show();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact, WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//          CityArrayList.clear();
            System.out.println("POST EXECUTE");
//          Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service + "Slots Values-->" + jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {
                    String Meesage = jObj.getString("Meesage");

//                    Toast.makeText(getApplicationContext(),Meesage,Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");
                    Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_LONG).show();
                }


                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    public class Reserve_Slot_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public Reserve_Slot_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            System.out.println("POST EXECUTE");
//            Log.e("jsonResponse", "Login" + jsonResponse);
            dialog.dismiss();
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service + "Slots Values-->" + jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {

                    slots_notavailable.setVisibility(View.GONE);
                    waiting_laypout.setVisibility(View.VISIBLE);
                    String Meesage = jObj.getString("WaitingNumber");

                    slot_waitingNumber.setText(Meesage);

//                    finish();
                } else {
                    System.out.println("failed");
//                    String Message = jObj.getString("Message");

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }


//                hitLocationService();

                try {
//                    dialog.dismiss();
//                    reserve_slot.stopAnimation();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    private class Refund_WebService extends AsyncTask<String, Void, String> {
        String response;

        Dialog dialog;
        String jsonResponse;
        Context context_aact;
        ArrayList<NameValuePair> loadValue = new ArrayList<NameValuePair>();

        public Refund_WebService(Context context_ws, ArrayList<NameValuePair> loadValue) {
            // TODO Auto-generated constructor stub
            this.loadValue = loadValue;
            this.context_aact = context_ws;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(SlotsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);

          dialog.setTitle("Title...");
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SlotsActivity.this)
                    .load(R.drawable.loading)
                   .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                WS_CallService service2 = new WS_CallService(context_aact);
                jsonResponse = service2.getJSONObjestString(loadValue,
                        WebserviceUrl.mainurl);
                Log.e("City id --> ", "Location id" + CityId + "and in Book service " + LocationId);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponse;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.e("Refund", "Amount Cloud" + result);
    try {
        dialog.dismiss();
     }catch (Exception e){}

//            Toast.makeText(getApplicationContext(),"Response : "+result,Toast.LENGTH_LONG).show();

            if (result != null) {
                try {
                    JSONObject object = new JSONObject(result);

                    String statusString = "";
                    if (object.has("ResponseCode"))
                        statusString = object.getString("ResponseCode");

                    if (statusString.equalsIgnoreCase("200")) {

                    }
                    Log.e("Cloud Code  ", "Getiing " + result);


                } catch (Exception e) {
                    Log.e("exception", e.toString()+",");
                 PopupMessage("Please try again later.");
                }
            } else if (result == null) {

                final Dialog dialog1 = new Dialog(SlotsActivity.this);
                if (Build.VERSION.SDK_INT < 16) {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FULLSCREEN);

                }
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.pop_up_dialog);
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);

                TextView text = (TextView) dialog1.findViewById(R.id.text);
                text.setText("Please try again later");
                text.setTextColor(Color.parseColor("#000000"));
                dialog1.show();
                TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                okay_popup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();
                        finish();
                    }
                });

//                OkayDialog("Reseponse Null");

                //Toast.makeText(SlotsActivity.this, "Response " + result, Toast.LENGTH_LONG).show();
                //dialog.dismiss();
            }
            // Toast.makeText(getActivity().getApplicationContext(),"email"+result,Toast.LENGTH_LONG).show();

        }
    }


    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        Log.d("SATURDAY","backpressed"+refundStatus);
        if (refundStatus.equalsIgnoreCase("NONEED")) {
            finish();
        } else {
            if (user_free_parking==0)

            {
                hitRefundWalletService();
            }
            else if (Apply_Hourly_Charges==1)
            {
                hitRefundWalletService();
            }
            finish();
//
        }
    }

    public void OkayDialog(String Message) {
        final Dialog dialog1 = new Dialog(SlotsActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView text = (TextView) dialog1.findViewById(R.id.text);
        text.setText(Message);
        text.setTextColor(Color.parseColor("#000000"));
        dialog1.show();
        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                finish();
            }
        });
    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }
    @Override
    protected void onPause() {
        super.onPause();

        try {
            unregisterReceiver(braodcastNewBoking);
        } catch (Exception e) {
        }
    }
}
