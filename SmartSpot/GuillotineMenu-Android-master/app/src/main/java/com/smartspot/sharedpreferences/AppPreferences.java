package com.smartspot.sharedpreferences;

/**
 * Created by iyyapparajr on 9/9/2017.
 */


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by manojg on 06-Jul-17.
 */
public class AppPreferences {


    private static Context STORE_CONTEXT;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor_One;



    private static Context STORE_CONTEXT_One;
    private static SharedPreferences preferences_One;
    private static SharedPreferences.Editor editor;


    public static void setStoreContext(Context context) {
        STORE_CONTEXT = context;
    }

    public static boolean putStringIntoStore(String key, String value) {
        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences("store",STORE_CONTEXT.MODE_PRIVATE );
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putString(key, value).commit();
    }



    public static String getStringFromStore(String key) {
        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences("store", STORE_CONTEXT.MODE_PRIVATE);
        return prefs.getString(key, "");

    }

    public static boolean getBooleanFromStore(String key) {

        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences("store", STORE_CONTEXT.MODE_PRIVATE);
        return prefs.getBoolean(key, false);

    }

    public static boolean putBooleanIntoStore(String key, boolean value) {
        SharedPreferences prefs = STORE_CONTEXT.getSharedPreferences("store", STORE_CONTEXT.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putBoolean(key, value).commit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        preferences = context.getSharedPreferences("store", Context.MODE_PRIVATE);
        return preferences;
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        editor = getSharedPreferences(context).edit();
        return editor;
    }

    public static void clearAll(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("store", STORE_CONTEXT.MODE_PRIVATE);
        prefs.edit().clear().commit();

    }



    //New Key



    public static void setSTORE_CONTEXT_One(Context context) {
        STORE_CONTEXT_One = context;
    }

    public static boolean putStringIntoStoreOne(String key, String value) {
        SharedPreferences prefs = STORE_CONTEXT_One.getSharedPreferences("storeOne",STORE_CONTEXT_One.MODE_PRIVATE );
        SharedPreferences.Editor editor = prefs.edit();
        return editor.putString(key, value).commit();
    }


    public static String getStringFromStoreOne(String key) {
        SharedPreferences prefs = STORE_CONTEXT_One.getSharedPreferences("storeOne", STORE_CONTEXT_One.MODE_PRIVATE);
        return prefs.getString(key, "");

    }

    public static boolean getBooleanFromStoreOne(String key) {

        SharedPreferences prefs = STORE_CONTEXT_One.getSharedPreferences("storeOne", STORE_CONTEXT_One.MODE_PRIVATE);
        return prefs.getBoolean(key, false);

    }


    private static SharedPreferences getSharedPreferencesOne(Context context) {
        preferences_One = context.getSharedPreferences("storeOne", Context.MODE_PRIVATE);
        return preferences_One;
    }

    private static SharedPreferences.Editor getEditorOne(Context context) {
        editor = getSharedPreferencesOne(context).edit();
        return editor;
    }

    public static void clearAllOne(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("storeOne", STORE_CONTEXT_One.MODE_PRIVATE);
        prefs.edit().clear().commit();

    }



}
