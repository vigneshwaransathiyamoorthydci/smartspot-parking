package com.smartspot.font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class Montserrat {

    private static Montserrat instance;
    private static Typeface typeface;

    public static Montserrat getInstance(Context context) {
        synchronized (Montserrat.class) {
            if (instance == null) {
                instance = new Montserrat();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Regular.otf");
            }
            return instance;
        }
    }
    public static Montserrat getInstance(Context context , int i) {
        synchronized (Montserrat.class) {
            if (instance == null) {
                instance = new Montserrat();
                if(i==0)
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Regular.otf");
                else
                    typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Montserrat-Regular.otf");

            }
            return instance;
        }
    }






    public Typeface getTypeFace() {
        return typeface;
    }
}
