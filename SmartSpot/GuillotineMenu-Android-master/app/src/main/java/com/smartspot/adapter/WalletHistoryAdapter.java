package com.smartspot.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartspot.R;
import com.smartspot.widget.BookingPojo;

import org.apache.http.NameValuePair;

import java.util.ArrayList;


/**
 * Created by iyyapparajr on 9/12/2017.
 */




public class WalletHistoryAdapter extends BaseAdapter {
    public View view;
    public int currPosition = 0;
    Context context;

    int layoutId;
    ProgressDialog pd;
    Holder holder;
    ArrayList<NameValuePair> skills_nv = new ArrayList<NameValuePair>();
    ArrayList<BookingPojo> plan_list_adap = new ArrayList<BookingPojo>();

    public WalletHistoryAdapter(Context context, int textViewResourceId,
                                ArrayList<BookingPojo> list) {

        System.out.println("Valuesin adapter if contructor");
        this.context = context;
        plan_list_adap = list;
        layoutId = textViewResourceId;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }


    @Override
    public int getCount() {
        return plan_list_adap.size();
    }

    @Override
    public BookingPojo getItem(int position) {
        return plan_list_adap.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        LinearLayout layout;
//        layout = (LinearLayout) View.inflate(context, layoutId, null);
//        holder = new Holder();
        System.out.println("Valuesin adapter convertview");
        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.wallet_history_item, parent, false);
            holder = new Holder();

            holder.amount=(TextView)convertView.findViewById(R.id.amount);
            holder.status=(TextView)convertView.findViewById(R.id.wallet_type);
            holder.date_ans=(TextView)convertView.findViewById(R.id.date);
            convertView.setTag(holder);
//            holder.amount.setText(plan_list_adap.get(position).getWalletMoney());
//            holder.status.setText(plan_list_adap.get(position).getwalletStatus());
//            holder.date_ans.setText(plan_list_adap.get(position).getWalletDAte());

            System.out.println("Values in adapter if section"+plan_list_adap.get(position).getWalletDAte());
            Log.e("Values ","Wallet"+plan_list_adap.get(position).getWalletMoney()+""+plan_list_adap.get(position).getwalletStatus()+""+plan_list_adap.get(position).getWalletDAte());


        } else {
            convertView = (LinearLayout) convertView;
            holder = (Holder) convertView.getTag();
        }

        holder.amount.setText("Aed "+plan_list_adap.get(position).getWalletMoney());
        holder.status.setText(plan_list_adap.get(position).getWallwallet_type());
        holder.date_ans.setText(plan_list_adap.get(position).getWalletDAte());
        Log.e("Wallet","Status"+plan_list_adap.get(position).getwalletStatus());
        return convertView;
    }

    private class Holder {

        TextView   amount,status,date_ans;

    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return plan_list_adap.indexOf(getItem(position));
    }

}