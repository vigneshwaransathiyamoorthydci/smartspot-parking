package com.smartspot.adapter;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smartspot.R;
import com.smartspot.fragment.ReservationHistoryFragment;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.BookingPojo;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ReservationHistoryAdapter extends BaseAdapter {
    public View view;
    public int currPosition = 0;
    Context context;
    int Postionadap;
    Holder holder;
    WS_CallService service_ReservationHistory;
    ArrayList<NameValuePair> ReservationHistory_NVP = new ArrayList<>();
    BarrierStatusUpdate_SErvice ReservationHistory_WebService;
    String barrierdown_id;

    int layoutId;
    ProgressDialog pd;

    ArrayList<NameValuePair> skills_nv = new ArrayList<NameValuePair>();
    ArrayList<BookingPojo> plan_list_adap = new ArrayList<BookingPojo>();
    ReservationHistoryFragment fragment;

    public ReservationHistoryAdapter(ReservationHistoryFragment fragment, Context context, int textViewResourceId,
                                     ArrayList<BookingPojo> list) {

        System.out.println("Valuesin adapter if contructor");
        this.context = context;
        plan_list_adap = list;
        layoutId = textViewResourceId;
        this.fragment = fragment;


    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getCount() {
        return plan_list_adap.size();
    }

    @Override
    public BookingPojo getItem(int position) {
        return plan_list_adap.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        LinearLayout layout;
//        layout = (LinearLayout) View.inflate(context, layoutId, null);
//        holder = new Holder();

        System.out.println("Valuesin adapter convertview");
        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.reservaqtion_history_item, parent, false);
            holder = new Holder();


            holder.city_name = (TextView) convertView.findViewById(R.id.city_name);
            holder.amount_one = (TextView) convertView.findViewById(R.id.amount_one);
            holder.amount = (TextView) convertView.findViewById(R.id.amount);

            holder.locationName = (TextView) convertView.findViewById(R.id.locationName);
            holder.status = (TextView) convertView.findViewById(R.id.payment_status_ans);
            holder.date_ans = (TextView) convertView.findViewById(R.id.date_ans);
            holder.booked_till = (TextView) convertView.findViewById(R.id.booked_till);
            holder.barrier_name = (TextView) convertView.findViewById(R.id.barrier_name);
//            holder.slot_id = (TextView) convertView.findViewById(R.id.slot_id);
            holder.barrier_down = (TextView) convertView.findViewById(R.id.barrier_down);
            holder.status_txt = (TextView) convertView.findViewById(R.id.status);

            convertView.setTag(holder);           /* System.out.println("Values in adapter if section" + plan_list_adap.get(position).g());
            Log.e("Bookied", "slot Id" + plan_list_adap.get(position).getBookingSlotId());
            Log.e("Bookied", "Barrier Name" + plan_list_adap.get(position).getBookingBarrierName());
*/

        } else {
            convertView = (LinearLayout) convertView;
            holder = (Holder) convertView.getTag();

        }

//        holder.slot_id.setText(plan_list_adap.get(position).getBooking_slot_id());
        if (plan_list_adap.get(position).getBooking_barrier_name().equalsIgnoreCase("")) {
            holder.barrier_name.setVisibility(View.GONE);
            //  holder.barrier_down.setVisibility(View.GONE);
        } else {
            //    holder.barrier_down.setVisibility(View.VISIBLE);
            holder.barrier_name.setText("Booked Barrier Name : " + plan_list_adap.get(position).getBooking_barrier_name());
        }
        holder.amount.setText(plan_list_adap.get(position).getBooking_payment_amount());
        holder.locationName.setText(plan_list_adap.get(position).getBooking_location_name());
        holder.city_name.setText(plan_list_adap.get(position).getBooking_city_name());

//        holder.barrier_up=(TextView)convertView.findViewById(R.id.barrier_up);
        holder.date_ans.setText("Booked From : " + plan_list_adap.get(position).getBooking_time());

        if (plan_list_adap.get(position).getBooking_payment_status().equalsIgnoreCase("1")) {
            holder.status.setText("Payment Status :  Paid");
        } else if (plan_list_adap.get(position).getBooking_payment_status().equalsIgnoreCase("0")) {
            holder.status.setText("Payment Status :  Failed");
        } else {
            holder.status.setText("Payment Status :  Pending");
        }

        if (plan_list_adap.get(position).getType().equalsIgnoreCase("now")) {
            holder.booked_till.setText("Booked Till : " + plan_list_adap.get(position).getBookedTill());
            holder.booked_till.setVisibility(View.VISIBLE);

            if (plan_list_adap.get(position).getBooking_barrier_status().equalsIgnoreCase("0")) {
                holder.barrier_down.setText("Barrier Down");
                holder.barrier_down.setVisibility(View.VISIBLE);
            } else {
                holder.barrier_down.setVisibility(View.GONE);
            }

        } else if (plan_list_adap.get(position).getType().equalsIgnoreCase("future")) {
//            holder.booked_till.setVisibility(View.GONE);

            holder.booked_till.setText("Booked Till : " + plan_list_adap.get(position).getBookedTill());
            holder.booked_till.setVisibility(View.VISIBLE);

            if (plan_list_adap.get(position).getBooking_barrier_status().equalsIgnoreCase("1") || plan_list_adap.get(position).getBooking_barrier_name().equalsIgnoreCase("") || plan_list_adap.get(position).getBooking_barrier_name().isEmpty() || plan_list_adap.get(position).getBooking_barrier_name() == null) {
                holder.barrier_down.setVisibility(View.GONE);
            } else if (plan_list_adap.get(position).getBooking_barrier_status().equalsIgnoreCase("0") || !plan_list_adap.get(position).getBooking_barrier_name().equalsIgnoreCase("")) {
                holder.barrier_down.setText("Barrier Down");
                holder.barrier_down.setVisibility(View.VISIBLE);
            } /*else if (plan_list_adap.get(position).getBooking_barrier_status().equalsIgnoreCase("0")) {
                holder.barrier_down.setVisibility(View.GONE);
            }*/
        }


        if (plan_list_adap.get(position).getBooking_status().equalsIgnoreCase("")) {
            if(plan_list_adap.get(position).getBooking_barrier_status().equalsIgnoreCase("1")){
                holder.status_txt.setVisibility(View.VISIBLE);
                holder.status_txt.setText("Status : Availed");
            }else{
                holder.status_txt.setVisibility(View.GONE);
            }

        } else if (plan_list_adap.get(position).getBooking_status().equalsIgnoreCase("Expired")) {
            holder.barrier_down.setVisibility(View.GONE);
            holder.status_txt.setVisibility(View.VISIBLE);
            holder.status_txt.setText("Status : Expired");
        } else {
            holder.status_txt.setVisibility(View.GONE);
        }


        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                holder.barrier_down,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();

        holder.barrier_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("position", "->" + position);
                Postionadap = position;
                if (plan_list_adap.get(position).getBooking_barrier_status().equalsIgnoreCase("0")) {


                    barrierdown_id = plan_list_adap.get(position).getBooking_slot_id();

                    Log.e("barrierdown_id", "->" + barrierdown_id);


                    final Dialog dialog1 = new Dialog(context);
                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog1.setContentView(R.layout.pop_up_dialog);
                    dialog1.setCancelable(false);
                    dialog1.setCanceledOnTouchOutside(false);

                    TextView noPopup = (TextView) dialog1.findViewById(R.id.noPopup);
                    noPopup.setVisibility(View.VISIBLE);

                    TextView text = (TextView) dialog1.findViewById(R.id.text);
                    text.setText("Are you ready to park and sure to lower the barrier now ?");

                    // text.setTextColor(Color.parseColor("#ffffff"));
                    dialog1.show();

                    TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                    okay_popup.setText("YES");
                    okay_popup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog1.dismiss();
                            BariierDown(barrierdown_id, position);


                        }
                    });

                    noPopup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog1.dismiss();
                        }
                    });


                } else {
                    barrierdown_id = plan_list_adap.get(position).getBooking_slot_id();

//                    BariierUp(barrierdown_id);
                }


            }
        });


//        holder.barrier_up.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                BariierUp();
//            }
//        });
//
        return convertView;
    }


    public void BariierDown(String barrierdown_id, int post) {
        ArrayList<NameValuePair> loadvalue = new ArrayList<NameValuePair>();
        byte[] data;

        try {

//            Log.e("username",""+email.getText().toString());
            Log.e("barrierdown_id", "" + barrierdown_id);
            //loadvalue.add(new BasicNameValuePair("api_key", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_APIKEY)));
            loadvalue.add(new BasicNameValuePair("api_key", "srijanVADS2S8JJ126FFKN4LL9VZ64C0NYW934"));
            loadvalue.add(new BasicNameValuePair("user_id", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)));
            LoadTask loadTask = new LoadTask(context, loadvalue, barrierdown_id, post);
            loadTask.execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void BariierUp(String baririd) {
        ArrayList<NameValuePair> loadvalue = new ArrayList<NameValuePair>();
        byte[] data;

        try {

//            Log.e("username",""+email.getText().toString());
//            Log.e("password", ""+password.getText().toString());
            loadvalue.add(new BasicNameValuePair("api_key", "srijanVADS2S8JJ126FFKN4LL9VZ64C0NYW934"));
            loadvalue.add(new BasicNameValuePair("user_id", AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)));
            LoadTaskBarrierUp loadTask = new LoadTaskBarrierUp(context, loadvalue, baririd);
            loadTask.execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private class LoadTask extends AsyncTask<String, Void, String> {
        String response;
        Dialog dialog;
        String jsonResponse;
        Context context_aact;
        String barws;
        int Postitionws, positionClick;
        ArrayList<NameValuePair> loadValue = new ArrayList<NameValuePair>();

        public LoadTask(Context context_ws, ArrayList<NameValuePair> loadValue, String barr, int positionClick) {
            // TODO Auto-generated constructor stub
            this.loadValue = loadValue;
            this.context_aact = context_ws;
            barws = barr;
            this.positionClick = positionClick;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            isInternetOn();

            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(context)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                WS_CallService service2 = new WS_CallService(context_aact);
                jsonResponse = service2.getJSONObjestString(loadValue,
                        "http://52.24.121.21:9000/parking/api/barrier/" + barws + "/state/down");
                Log.e("Bariier Id-->", barws);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponse;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.e("jsonResponse", "BarrierDown" + result);

//            Toast.makeText(getApplicationContext(),"Response : "+result,Toast.LENGTH_LONG).show();
            dialog.dismiss();
            if (result != null) {
                try {
                    JSONObject object = new JSONObject(result);

                    String statusString = "";
                    if (object.has("ResponseCode"))
                        statusString = object.getString("ResponseCode");

                    if (statusString.equalsIgnoreCase("200")) {

                        hitBarrierUpdate(positionClick);


//                      time_duration.setText(dataJson.getJSONObject(0).getString("id"));
//                        slots_total_count.setText(slots_count);

                    } else {
//                        slots_notavailable.setVisibility(View.VISIBLE);

                        final Dialog dialog1 = new Dialog(context);

                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog1.setContentView(R.layout.pop_up_dialog);
                        dialog1.setCancelable(false);
                        dialog1.setCanceledOnTouchOutside(false);

                        TextView text = (TextView) dialog1.findViewById(R.id.text);
                        text.setText("Slot already expired.");
                        // text.setTextColor(Color.parseColor("#ffffff"));
                        dialog1.show();

                        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                        okay_popup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog1.dismiss();
                                Log.e("SlotExpire", "- " + positionClick);
                               // plan_list_adap.get(positionClick).setBooking_barrier_status("1");
                              //  plan_list_adap.get(positionClick).setBooking_status("Expired");
                               // notifyDataSetChanged();

                            }
                        });

                    }

                } catch (Exception e) {
                    Log.e("ex", e.toString());

                }
            } else if (result == null) {

                Toast.makeText(context, "Server Busy... Please try again later", Toast.LENGTH_LONG).show();
            }
            //   Toast.makeText(getActivity().getApplicationContext(),"email"+result,Toast.LENGTH_LONG).show();

        }//close onPostExecute
    }// close validateUserTask


    private class LoadTaskBarrierUp extends AsyncTask<String, Void, String> {
        String response;
        ProgressDialog dia;
        String jsonResponse;
        String barrier_wsid;
        Context context_aact;
        ArrayList<NameValuePair> loadValue = new ArrayList<NameValuePair>();

        public LoadTaskBarrierUp(Context context_ws, ArrayList<NameValuePair> loadValue, String barrierId) {
            // TODO Auto-generated constructor stub
            this.loadValue = loadValue;
            this.context_aact = context_ws;
            barrier_wsid = barrierId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            isInternetOn();

            dia = new ProgressDialog(context);
            dia.setMessage("Loading...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                WS_CallService service2 = new WS_CallService(context_aact);
                jsonResponse = service2.getJSONObjestString(loadValue,
                        "http://52.24.121.21:9000/parking/api/barrier/" + barrier_wsid + "/state/up");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponse;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.e("jsonResponse", "Login" + result);
            try {
                dia.dismiss();
            } catch (Exception e) {

            }

//            Toast.makeText(getApplicationContext(),"Response : "+result,Toast.LENGTH_LONG).show();

            if (result != null) {
                try {
                    JSONObject object = new JSONObject(result);

                    String statusString = "";
                    if (object.has("ResponseCode"))
                        statusString = object.getString("ResponseCode");

                    if (statusString.equalsIgnoreCase("200")) {


                        // holder.barrier_down.setText("Barrier Up");
//                        hitBarrierUpdate();


//                      time_duration.setText(dataJson.getJSONObject(0).getString("id"));
//                        slots_total_count.setText(slots_count);


                    } else {
//                        slots_notavailable.setVisibility(View.VISIBLE);
                        final Dialog dialog1 = new Dialog(context);

                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog1.setContentView(R.layout.pop_up_dialog);
                        dialog1.setCancelable(false);
                        dialog1.setCanceledOnTouchOutside(false);

                        TextView text = (TextView) dialog1.findViewById(R.id.text);
                        text.setText("Error on server please try again later");
                        //  text.setTextColor(Color.parseColor("#ffffff"));
                        dialog1.show();

                        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
                        okay_popup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog1.dismiss();


                            }
                        });


                    }

                } catch (Exception e) {
                    Log.e("ex", e.toString());

                }
            } else if (result == null) {

                Toast.makeText(context, "email" + result, Toast.LENGTH_LONG).show();
            }


        }
    }

    private void hitBarrierUpdate(int posit) {


        byte[] data;
        try {
            Log.e("updatePosition", "->" + posit);
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:updateBarrier|user_id:" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID) + "|booking_id:" + plan_list_adap.get(posit).getBooking_id() + "|slot_id:" + plan_list_adap.get(posit).getBooking_slot_id();
            System.out.println("Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_ReservationHistory = new WS_CallService(context);
            ReservationHistory_NVP.add(new BasicNameValuePair("WS", base64_register));
            ReservationHistory_WebService = new BarrierStatusUpdate_SErvice(context, ReservationHistory_NVP, posit);
            ReservationHistory_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public class BarrierStatusUpdate_SErvice extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Dialog dialog;
        Context context_aact;
        String booking_barrier_name, booking_barrier_id;
        int valuePp;

        public BarrierStatusUpdate_SErvice(Context context_ws, ArrayList<NameValuePair> loginws, int valuePp) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;
            this.valuePp = valuePp;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(context)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();


//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_ReservationHistory = new WS_CallService(context_aact);
                jsonResponseString = service_ReservationHistory.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            dialog.dismiss();
            System.out.println("POST EXECUTE");
            Log.e("Reservation", "Response" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {
                    Toast.makeText(context, "Updated successfully.", Toast.LENGTH_LONG).show();
//                     plan_list_adap.get(Postionadap).setBooking_barrier_status("1");
                    //         notifyDataSetChanged();

                    Log.e("notifyPosition", "- " + valuePp);
                    plan_list_adap.get(valuePp).setBooking_barrier_status("1");
                    notifyDataSetChanged();

                    //   ((ReservationHistoryFragment)fragment).hitReservationHistory();

                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");
                    Toast.makeText(context, Message, Toast.LENGTH_LONG).show();
                }


//                hitLocationService();

                try {

                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");

                Toast.makeText(context, "ERROR on server. Please try again later " + e, Toast.LENGTH_LONG).show();
            }

        }

    }


    private class Holder {

        TextView city_name, amount_one, amount, locationName, status, date_ans, booked_till, status_txt;
        TextView barrier_down;
        TextView slot_id;
        TextView barrier_name;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return plan_list_adap.indexOf(getItem(position));
    }

}