package com.smartspot.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smartspot.R;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class WalletProcessingActivity extends Activity{

    RelativeLayout payment_process_layout,payment_first_layout;
    String Token,Amount, card_number="", card_name="", card_expiry_month="", card_expiry_year="", card_cvv="", save_card="",card_id="", getEmailagain="";
    Payment_WS IFMoneyInWallet_WebService;
    WS_CallService service_IFMoneyInWallet;
    ArrayList<NameValuePair> IFMoneyInWallet_NVP = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.credit_card_lay);

        Token = getIntent().getStringExtra("PaymentToken");
        Amount= getIntent().getStringExtra("PaymentAmount");

        getEmailagain= getIntent().getStringExtra("Email");
        card_number= getIntent().getStringExtra("card_number");
        card_id= getIntent().getStringExtra("CardId");
        card_name= getIntent().getStringExtra("card_name");
        card_expiry_month= getIntent().getStringExtra("card_expiry_month");
        card_expiry_year= getIntent().getStringExtra("card_expiry_year");
        card_cvv= getIntent().getStringExtra("card_cvv");
        save_card= getIntent().getStringExtra("save_card");


        Log.e("vignesh Email","->"+getEmailagain);
        Log.e("card_number","->"+card_number);
        Log.e("CardId","->"+card_id);
        Log.e("card_name","->"+card_name);
        Log.e("card_expiry_month","->"+card_expiry_month);
        Log.e("card_expiry_year","->"+card_expiry_year);
        Log.e("card_cvv","->"+card_cvv);
        Log.e("save_card","->"+save_card);


        payment_process_layout = (RelativeLayout) findViewById(R.id.payment_process_layout);
        payment_first_layout = (RelativeLayout) findViewById(R.id.payment_first_layout);

        RotateAnimation rotateAnimation = new RotateAnimation(0, 360f,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);

                rotateAnimation.setInterpolator(new LinearInterpolator());
                rotateAnimation.setDuration(900);
                rotateAnimation.setRepeatCount(Animation.INFINITE);

                findViewById(R.id.rotate_image).startAnimation(rotateAnimation);


        payment_process_layout.setVisibility(View.VISIBLE);
        payment_first_layout.setVisibility(View.GONE);
        if(isInternetOn())
            hitPaymentService();

    }
        private void hitPaymentService() {

            IFMoneyInWallet_NVP=new ArrayList<>();
            byte[] data;
            try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
                String login_string = "Function:payment|token:"+Token+"|card_cvv:"+card_cvv+"|card_id:"+card_id+"|email:"+ getEmailagain+"|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)+"|amount:"+Amount+"|card_number:"+card_number+"|card_name:"+card_name+"|card_expiry_month:"+card_expiry_month+"|card_expiry_year:"+card_expiry_year+"|save_card:"+save_card;
                //String login_string = "Function:payment|token:"+Token+"|card_id:"+card_id+"|email:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL)+"|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)+"|amount:"+Amount+"|card_number:"+card_number+"|card_name:"+card_name+"|card_expiry_month:"+card_expiry_month+"|card_expiry_year:"+card_expiry_year+"|card_cvv:"+card_cvv+"|save_card:"+save_card;
                System.out.println(Preference_Constants.PREF_KEY_Service + " service  -->" + login_string);
                Log.e("Service","Context"+login_string);

                data = login_string.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

                service_IFMoneyInWallet = new WS_CallService(WalletProcessingActivity.this);
                IFMoneyInWallet_NVP.add(new BasicNameValuePair("WS", base64_register));
                IFMoneyInWallet_WebService = new Payment_WS(WalletProcessingActivity.this, IFMoneyInWallet_NVP);
                IFMoneyInWallet_WebService.execute();

            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }



    public class Payment_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

//        Dialog dialog;

        public Payment_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            dialog = new Dialog(PaymentActivity.this);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setCancelable(false);
//            dialog.setCanceledOnTouchOutside(false);
//            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);
//
//            Glide.with(PaymentActivity.this)
//                    .load(R.drawable.loading)
//                    .asGif()
//                    .placeholder(R.drawable.loading)
//                    .crossFade()
//                    .into(image);
//            dialog.show();
//            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_IFMoneyInWallet = new WS_CallService(context_aact);
                jsonResponseString = service_IFMoneyInWallet.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            System.out.println("POST EXECUTE");

            Log.e("Payment", "response" + jsonResponse);
            if(jsonResponse!=null) {
                try {
                    JSONObject jObj = new JSONObject(jsonResponse);

                    Log.e("If Monye", "Response" + jsonResponse);


                    if (jObj.getString("Status").equalsIgnoreCase("Payment Successfull")) {
                        String walletAmount = jObj.getString("walletAmount");
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY, walletAmount);
                        Toast.makeText(getApplicationContext(), "Recharge Successful !", Toast.LENGTH_SHORT).show();

                        if (jObj.has("Message")) {
                            if (jObj.getString("Message").equalsIgnoreCase("Save card failed. Email already exists for this customer.")) {
                                OkayDialog("Recharge Successful !. Unable to save card, Email already exists!", "");
                            } else {
                                OkayDialog("Congratulations ! Your money added to wallet.You can proceed booking now.", "");
                            }
                        } else {
                            OkayDialog("Congratulations ! Your money added to wallet.You can proceed booking now.", "");
                        }


                    } else {
                        if (jObj.has("Message")) {
                            String msg = jObj.getString("Message");
                            OkayDialog("" + msg, "1");
                        } else {
                            OkayDialog("Payment Failed", "1");
                        }


                    }


                } catch (Exception e) {
                    OkayDialog("Please try again later.", "1");
                    System.out.println(e.toString() + "zcx");
                }
            }else{
                PopupMessage("Please check your Data/WiFi connection.");
            }
        }

    }

    public void PopupMessage(final String msgText){
        final Dialog dialog1 = new Dialog(WalletProcessingActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView text = (TextView) dialog1.findViewById(R.id.text);
        text.setText(""+msgText);
        text.setTextColor(Color.parseColor("#000000"));
        dialog1.show();
        TextView okay_popup = (TextView) dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                finish();
            }
        });

    }




    public void OkayDialog(String Message,final String type)
    {
        final Dialog dialog1 = new Dialog(WalletProcessingActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(Message);
        text.setTextColor(Color.parseColor("#000000"));
        dialog1.show();
        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();

                if(type.equalsIgnoreCase("1")){
                    finish();
                }else {
                    Intent newInten = new Intent(WalletProcessingActivity.this, MainActivity.class);
                    newInten.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(newInten);
                    finish();
                }
            }
        });
    }
    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }


}
