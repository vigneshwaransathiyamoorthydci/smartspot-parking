package com.smartspot.fragment;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smartspot.R;
import com.smartspot.activity.LoginOtp;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by iyyapparajr on 3/19/2018.
 */

public class PromoCodeFragment extends Fragment {

    EditText couponcodeEdit;

    TextView applycoupon;

    WS_CallService service_WS;
    ArrayList<NameValuePair> coupon_namevalue=new ArrayList<>();
    ApplyDiscount_WS coupon_WS;
    TextView noteView ;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.promocode, container, false);


        couponcodeEdit=(EditText)view.findViewById(R.id.code);
        applycoupon=(TextView)view.findViewById(R.id.applydiscount);
        noteView = (TextView)view.findViewById(R.id.promocodePhone);
        noteView.setText("If you don't have a cash code already, Please call +971556938366 to get one.");
        Linkify.addLinks(noteView, Linkify.ALL);
        AppPreferences.putStringIntoStore(Preference_Constants.CURRENT_FRAGMENT,"6");
        applycoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetOn()){
                    if(Validation()){
                        ApplyCoupon();
                    }
                }

            }
        });


        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                applycoupon,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();
        return  view;
    }

    public boolean Validation(){
        boolean check=true;
        int v=0;
        String error="";

        if(couponcodeEdit.getText().length()==0 || couponcodeEdit.getText().toString().equalsIgnoreCase("") || couponcodeEdit.getText().toString().isEmpty() ||couponcodeEdit.getText().toString().equalsIgnoreCase("0")){
            error="Enter a valid code";
            check=false;
            errorPopup(error,"");
            return check;
        }

        return check;
    }


    private void ApplyCoupon() {

        coupon_namevalue=new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String coupon_String = "Function:Applycoupon|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)+"|coupon_code:"+couponcodeEdit.getText().toString().trim();
            System.out.println("Fucntionals Applycoupon request-->" + coupon_String);

            data = coupon_String.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_WS = new WS_CallService(getActivity());
            coupon_namevalue.add(new BasicNameValuePair("WS", base64_register));
            coupon_WS = new ApplyDiscount_WS(getActivity(), coupon_namevalue);
            coupon_WS.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }




    public class ApplyDiscount_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;

        Dialog dialog;
        Context context_aact;

        String user_wallet_money,user_device_type,user_fcm_key,user_id,Noofavailablefreebooking;
        String user_nameService,user_emailService,user_mobile_numberService
                ,user_statusService,user_last_loginService,user_last_logoutService,user_profile_imageService,user_device_idService,user_car_number;
        public ApplyDiscount_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(getActivity());
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();


            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_WS = new WS_CallService(context_aact);
                jsonResponseString = service_WS.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            try {
                dialog.dismiss();
            } catch (Exception e) {

            }

            if(jsonResponse!=null){

                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);

                    if(jsonObject.has("Status")){
                        if(jsonObject.getString("Status").equalsIgnoreCase("Success")){
                            if(jsonObject.has("Message"))
                                errorPopup(jsonObject.getString("Message"),"Success");

                        }else{
                            if(jsonObject.has("Message"))
                                errorPopup(jsonObject.getString("Message"),"");

                        }
                    }


                }catch (Exception e){
                    Log.e("ex","->"+e.toString());
                }
            }else {
                errorPopup("Please try again later.","");
            }


        }

    }



    public void errorPopup(String message,final String type){

        final Dialog dialog1 = new Dialog(getActivity());
        if (Build.VERSION.SDK_INT < 16) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);

        TextView messagetxt=(TextView)dialog1.findViewById(R.id.text);
        messagetxt.setText(""+message);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        dialog1.show();


        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                if(type.equalsIgnoreCase("Success")) {
                    Intent in = new Intent("CurrentWalletReceiver");
                    getActivity().getApplicationContext().sendBroadcast(in);
                }

            }
        });

    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getActivity().getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }


}
