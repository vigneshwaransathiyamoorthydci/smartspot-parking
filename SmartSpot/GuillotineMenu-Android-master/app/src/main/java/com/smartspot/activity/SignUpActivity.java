package com.smartspot.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.smartspot.R;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


/**
 * Created by iyyapparajr on 8/30/2017.
 */

public class SignUpActivity extends Activity {
    EditText emailEditTExt, nameEditTExt, passwordEdit, mobile_no;
    TextView signupText, termsText, howitworks;
    boolean atleastOneAlpha;
    ImageView back;
    WS_CallService service_SignUp;
    ArrayList<NameValuePair> SignUp_NVP = new ArrayList<>();
    Load_SignUp_WS signUp_Hitservice;

    TextView mobilenumber_error, password_error, email_error, username_error;
    Spinner city_spinner;
    String checkStr;
    String fcmToken;
SharedPreferences shared;


    EditText retype_pas_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.sign_up_layout);

//      EditText mobilenumber=(EditText)findViewById(R.id.fName1);




        emailEditTExt = (EditText) findViewById(R.id.emailEditTExt);
        nameEditTExt = (EditText) findViewById(R.id.nameEditTExt);
        passwordEdit = (EditText) findViewById(R.id.passwordEdit);

        retype_pas_text= (EditText) findViewById(R.id.retype_pas_text);

        mobile_no = (EditText) findViewById(R.id.mobile_no);
        signupText = (TextView) findViewById(R.id.signupText);
        howitworks = (TextView) findViewById(R.id.howitworks);

        city_spinner= (Spinner) findViewById(R.id.city_spinner1);

        mobilenumber_error = (TextView) findViewById(R.id.mobilenumber_error);
        password_error = (TextView) findViewById(R.id.password_error);
        email_error = (TextView) findViewById(R.id.email_error);
        username_error = (TextView) findViewById(R.id.username_error);

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        termsText = (TextView) findViewById(R.id.termsText);

       // termsText.setTextColor(Color.parseColor("#ffffff"));

        Drawable drawable = emailEditTExt.getBackground(); // get current EditText drawable
        drawable.setColorFilter(Color.parseColor("#838383"), PorterDuff.Mode.SRC_ATOP);


        Drawable drawable1 = nameEditTExt.getBackground(); // get current EditText drawable
        drawable1.setColorFilter(Color.parseColor("#838383"), PorterDuff.Mode.SRC_ATOP);
        Drawable drawable2 = passwordEdit.getBackground(); // get current EditText drawable
        drawable2.setColorFilter(Color.parseColor("#838383"), PorterDuff.Mode.SRC_ATOP);
        Drawable drawable3 = mobile_no.getBackground(); // get current EditText drawable
        drawable3.setColorFilter(Color.parseColor("#838383"), PorterDuff.Mode.SRC_ATOP);

        AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
        Log.e("Stored","Value SignUp"+AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW));
//        AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_FCMTOKEN);
//        Log.e("Stored","Value SignupOld"+AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN));



        if (Build.VERSION.SDK_INT > 16) {
            nameEditTExt.setBackground(drawable1); // set the new drawable to EditText
            emailEditTExt.setBackground(drawable);
            passwordEdit.setBackground(drawable2);
            mobile_no.setBackground(drawable3);

        } else {
            nameEditTExt.setBackgroundDrawable(drawable1); // use setBackgroundDrawable because setBackground required API 16
            emailEditTExt.setBackgroundDrawable(drawable);
            passwordEdit.setBackgroundDrawable(drawable2);
            mobile_no.setBackgroundDrawable(drawable3);

        }


        ArrayList<String> countryList=new ArrayList<>();
        countryList.add("971-2");
        countryList.add("971-6");
        countryList.add("971-3");
        countryList.add("971-58");
        countryList.add("971-6");
        countryList.add("971-9");
        countryList.add("971-4");
        countryList.add("971-6");
        countryList.add("971-70");
        countryList.add("971-48");
        countryList.add("971-77");
        countryList.add("971-6");
        countryList.add("971-88");




        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.country_ids, R.layout.spinner_item_country);
        adapter2.setDropDownViewResource(R.layout.spinner_item_country);
        city_spinner.setAdapter(adapter2);



//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item_new,countryList);
//        city_spinner.setAdapter(adapter);

        mobile_no.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)))) {
                    Log.i("service Response", "Enter pressed");

                    if (mobile_no.getText().toString().length() < 9) {


//                        passwordEdit.setFocusable(false);
//                        nameEditTExt.setFocusable(false);
//                        emailEditTExt.setFocusable(false);
//                        mobile_no.setError("Please enter valid mobile number");
                        mobilenumber_error.setVisibility(View.VISIBLE);
//                        email_error.setText(R.string.email_validation);
                    } else {
                        System.out.println(Preference_Constants.PREF_KEY_Service + "-->submit email-->");

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                        passwordEdit.setFocusable(true);
//                        nameEditTExt.setFocusable(false);
//                        emailEditTExt.setFocusable(false);
//                        passwordEdit.setFocusable(true);
                    }

//                    join_email_layout.setVisibility(View.GONE);
//                    join_password_layout.setVisibility(View.VISIBLE);
//                    join_password_layout.startAnimation(inFromRightAnimation());

                }

                return false;
            }
        });


//        mobile_no.setText("+971");
//        Selection.setSelection(mobile_no.getText(), mobile_no.getText().length());


        mobile_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mobilenumber_error.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

//                if (!s.toString().startsWith("+971")) {
//                    mobile_no.setText("+971");
//                    Selection.setSelection(mobile_no.getText(), mobile_no.getText().length());
//
//                }

            }
        });


        passwordEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
//                    Log.i(TAG,"Enter pressed");

//                    hasNums(passwordEditText.getText().toString().trim());
                    passwordEdit.getText().toString().replaceAll("( +)", " ").trim();
                    if (passwordEdit.getText().toString().replaceAll("( +)", " ").trim().length() < 9) {

//                        passwordEdit.setError("Enter a Password with at least 8 characters and that has least 1 letter and 1 number");
                        password_error.setVisibility(View.VISIBLE);
                        password_error.setText("Enter a Password with at least 8 characters and that has least 1 letter and 1 number");
                    } else {

                        hasNums(passwordEdit.getText().toString().trim());
                        System.out.println("values is number -->" + hasNums(passwordEdit.getText().toString().trim()));

                        atleastOneAlpha = passwordEdit.getText().toString().trim().matches(".*[a-zA-Z]+.*");

                        System.out.println("values is alphabet is -->" + atleastOneAlpha);


                        if (hasNums(passwordEdit.getText().toString().trim()).equalsIgnoreCase("NONE")) {

//                            passwordEdit.setError("password should have one number");
                            password_error.setVisibility(View.VISIBLE);
                            password_error.setText("password should have one number");
                        } else if (atleastOneAlpha == false) {

//                            passwordEdit.setError("password should have one alphabet");
                            password_error.setVisibility(View.VISIBLE);
                            password_error.setText("password should have one alphabet");
                        } else if (hasNums(passwordEdit.getText().toString().trim()).equalsIgnoreCase("NONE") && atleastOneAlpha == false)

                        {

//                            passwordEdit.setError("password should have one alphabet and one number");

                            password_error.setVisibility(View.VISIBLE);
                            password_error.setText("password should have one alphabet and one number");
                        }

                        else {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            nameEditTExt.setFocusable(true);
                        }
                    }
                }
                return false;
            }
        });


        passwordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                emailValidation(emailEditTExt.getText().toString().trim());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                password_error.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        nameEditTExt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                username_error.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        nameEditTExt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.i(TAG,"Enter pressed");

                    System.out.println(Preference_Constants.PREF_KEY_Service + "first name" + nameEditTExt.getText().toString().replaceAll("( +)", " ").trim());

                    System.out.println(Preference_Constants.PREF_KEY_Service + "first name text length-->" + nameEditTExt.getText().toString().trim().length());
                    if (nameEditTExt.getText().toString().replaceAll("( +)", " ").trim().length() < 6 || nameEditTExt.getText().toString().isEmpty()) {

                        nameEditTExt.setFocusable(true);
//                        nameEditTExt.setError("Please enter a Username at least 6 character long");
                        username_error.setVisibility(View.VISIBLE);
                        username_error.setText("Please enter a Username at least 6 character long");
                    }

//                    else   if(lastNameEditTExt.getText().toString().length()<4)
//                    {
//                        first_last_name_error.setVisibility(View.VISIBLE);
//                        lastNameEditTExt.setFocusable(true);
//                        first_last_name_error.setText(R.string.last_name_validation);
//                    }
//
                    else {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        emailEditTExt.setFocusable(true);

                    }
                }
                return false;
            }
        });

        emailEditTExt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i("service Response", "Enter pressed");


                    emailValidation(emailEditTExt.getText().toString().trim());

                    if (checkStr.equalsIgnoreCase("Failed")) {
//                        emailEditTExt.setError("Please enter valid email address");
//                        email_error.setText(R.string.email_validation);
                        email_error.setVisibility(View.VISIBLE);
                        email_error.setText(R.string.email_validation);
                    } else {
                        System.out.println(Preference_Constants.PREF_KEY_Service + "submit email-->");

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);


                    }

//                    join_email_layout.setVisibility(View.GONE);
//                    join_password_layout.setVisibility(View.VISIBLE);
//                    join_password_layout.startAnimation(inFromRightAnimation());

                }
                return false;
            }
        });


        emailEditTExt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                email_error.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        signupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (mobile_no.getText().toString().isEmpty() || passwordEdit.getText().toString().trim().isEmpty() || emailEditTExt.getText().toString().trim().isEmpty() || nameEditTExt.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter the required fields for a successful signup.", Toast.LENGTH_LONG).show();
                }else if(hasNums(passwordEdit.getText().toString().trim()).equalsIgnoreCase("NONE")) {

                    Log.d(Preference_Constants.PREF_KEY_Service,"Value in one");

                    Toast.makeText(getApplicationContext(), "Password should have one number", Toast.LENGTH_LONG).show();
                }
                else if (hasNums(passwordEdit.getText().toString().trim()).equalsIgnoreCase("NONE") && atleastOneAlpha == false)
                {
                    Log.d(Preference_Constants.PREF_KEY_Service,"Value in three");

                    Toast.makeText(getApplicationContext(), "Password should have one alphabet and one number", Toast.LENGTH_LONG).show();
                }
                else if(!passwordEdit.getText().toString().equalsIgnoreCase(retype_pas_text.getText().toString()))
                {
                    Log.d(Preference_Constants.PREF_KEY_Service,"Value in four");
                    Toast.makeText(getApplicationContext(), "Password and Re-Enter password did not match", Toast.LENGTH_LONG).show();
                }else if(mobile_no.getText().toString().length()>9)
                {
                    Toast.makeText(getApplicationContext(), "Please enter valid number", Toast.LENGTH_LONG).show();
                }

                else {
                    hitService();
                }

            }
        });

        howitworks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this,HowItWorks.class));

            }
        });
    }

    private void hitService() {

        byte[] data;
        try {
//          String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";

            String mobi = mobile_no.getText().toString().substring(1, mobile_no.getText().toString().length());

            String login_string = "Function:Registration|Mobile Number:" + mobile_no.getText().toString() + "|Password:" + passwordEdit.getText().toString().trim() + "|Name:" + nameEditTExt.getText().toString() + "|Email address:" + emailEditTExt.getText().toString().trim() + "|device id:|SignupType:Manual|device_type:2|fcm_key:"+ AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
            System.out.println(Preference_Constants.PREF_KEY_Service + "Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_SignUp = new WS_CallService(getApplicationContext());
            SignUp_NVP.add(new BasicNameValuePair("WS", base64_register));
            signUp_Hitservice = new Load_SignUp_WS(SignUpActivity.this, SignUp_NVP);
            signUp_Hitservice.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public boolean emailValidation(String emailString) {
        boolean check = true;
        String error = "";


        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
//        String email = email_ediTText.getText().toString().trim();
        String email = emailString;
        if (emailString.length() == 0) {
            error = "Enter a Valid Email";
            check = false;
            checkStr = "Failed";
//            validatePopup(error);
            return check;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            error = "Invalid Email";
            checkStr = "Failed";
            check = false;
//            validatePopup(error);

            return check;
        } else {
//            check = true;
            checkStr = "Success";
        }

        return check;
    }


    public String hasNums(String str) {
        char[] nums = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char[] toChar = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            toChar[i] = str.charAt(i);
            for (int j = 0; j < nums.length; j++) {
                if (toChar[i] == nums[j]) {
                    return str;
                }
            }
        }
        return "NONE";
    }


    public class Load_SignUp_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;

        Context context_aact;

        Dialog dialog;

        public Load_SignUp_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(SignUpActivity.this);
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();



            dialog = new Dialog(SignUpActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SignUpActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SignUp = new WS_CallService(context_aact);
                jsonResponseString = service_SignUp.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {

                    String Message = jObj.getString("response");


                    String user_status = jObj.getJSONArray("response").getJSONObject(0).getString("user_status");
                    String user_last_login = jObj.getJSONArray("response").getJSONObject(0).getString("user_last_login");
                    String user_last_logout = jObj.getJSONArray("response").getJSONObject(0).getString("user_last_logout");
                    String user_name = jObj.getJSONArray("response").getJSONObject(0).getString("user_name");
                    String user_mobile_number = jObj.getJSONArray("response").getJSONObject(0).getString("user_mobile_number");
                    String user_email = jObj.getJSONArray("response").getJSONObject(0).getString("user_email");
                    String user_device_id = jObj.getJSONArray("response").getJSONObject(0).getString("user_device_id");
                    String user_id = jObj.getJSONArray("response").getJSONObject(0).getString("user_id");

              String      user_wallet_money= jObj.getJSONArray("response").getJSONObject(0).getString("user_wallet_money");
                    String              user_fcm_key= jObj.getJSONArray("response").getJSONObject(0).getString("user_fcm_key");
                    String user_profile_image= jObj.getJSONArray("response").getJSONObject(0).getString("user_profile_image");


                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_ID, user_id);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME, user_name);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_EMAIL, user_email);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_MOBILE, user_mobile_number);


                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_STATUS, user_status);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_LAST_LOGIN, user_last_login);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_LAST_LOGOUT, user_last_logout);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_DEVICE_ID, user_device_id);

                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY, user_wallet_money);
                    AppPreferences.putStringIntoStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW ,user_fcm_key);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_PROFILE_IMAGE, user_profile_image);





                    final Dialog dialog1 = new Dialog(SignUpActivity.this);
                    if (Build.VERSION.SDK_INT < 16) {
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);

                    }
                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog1.setContentView(R.layout.pop_up_dialog);

                    dialog1.setCancelable(false);
                    dialog1.setCanceledOnTouchOutside(false);

                    dialog1.show();


                    TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
                    okay_popup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog1.dismiss();

                            Intent LoginIntent = new Intent(SignUpActivity.this, MainActivity.class);
                            startActivity(LoginIntent);
                            finish();
                        }
                    });

                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

                    if (Message.contains("ocialIDEmailSignupTypeSocialType")) {

//                        join_ph_no_layout.startAnimation(outToRightAnimation());
//                        join_ph_no_layout.setVisibility(View.GONE);
//                        join_email_layout.setVisibility(View.VISIBLE);
//
//                        join_email_layout.startAnimation(inFromRightAnimation());
//                        email_error.setVisibility(View.VISIBLE);
//                        email_error.setText(Message);
                        Toast.makeText(getApplicationContext(), Message + "please try again ", Toast.LENGTH_LONG).show();


                    } else if (Message.contains("User Name already exist")) {

//                        join_ph_no_layout.startAnimation(outToRightAnimation());
//                        join_ph_no_layout.setVisibility(View.GONE);
//                        join_userName_layout.setVisibility(View.VISIBLE);
//                        join_userName_layout.startAnimation(inFromRightAnimation());
//                        first_last_name_error.setVisibility(View.VISIBLE);
//                        first_last_name_error.setText(Message);

                        nameEditTExt.setFocusable(true);
                        username_error.setVisibility(View.VISIBLE);
                        username_error.setText(Message);


                    } else if (Message.contains("Email already exists")) {

//                        join_ph_no_layout.startAnimation(outToRightAnimation());
//                        join_ph_no_layout.setVisibility(View.GONE);
//                        join_email_layout.setVisibility(View.VISIBLE);
//                        join_email_layout.startAnimation(inFromRightAnimation());
//                        email_error.setVisibility(View.VISIBLE);
//                        email_error.setText(Message);

                        emailEditTExt.setFocusable(true);
                        email_error.setVisibility(View.VISIBLE);
                        email_error.setText(Message);

                    } else {
//                        join_ph_no_layout.startAnimation(outToRightAnimation());
//                        join_ph_no_layout.setVisibility(View.GONE);
//                        join_userName_layout.setVisibility(View.VISIBLE);
//                        join_userName_layout.startAnimation(inFromRightAnimation());
//                        first_last_name_error.setVisibility(View.VISIBLE);
//                        first_last_name_error.setText(Message);


                        Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_LONG).show();


                    }

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }
}
