package com.smartspot.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smartspot.R;
import com.smartspot.adapter.WalletHistoryAdapter;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.BookingPojo;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by iyyapparajr on 11/10/2017.
 */

public class WalletHistoryActovity extends Activity {


    WS_CallService service_ReservationHistory;
    WalletHistoryAdapter reservation_history_adapter;;
    ArrayList<NameValuePair> ReservationHistory_NVP=new ArrayList<>();
    ReservationHistory_WS ReservationHistory_WebService;
ImageView iocn_navigation;
    ArrayList<BookingPojo> WalletArray=new ArrayList<>();

TextView NoItems;
ListView walletHistoryList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_reservation_list);

        iocn_navigation=(ImageView)findViewById(R.id.iocn_navigation);
        walletHistoryList=(ListView)findViewById(R.id.reservation_list_view);
        NoItems=(TextView) findViewById(R.id.emptyTExt);

        if(isInternetOn())
        hitWalletHistory();


        NoItems.setText("No history found");
        iocn_navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    private void hitWalletHistory() {

        ReservationHistory_NVP=new ArrayList<>();
        byte[] data;
        try {
            String login_string = "Function:wallet|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            //String login_string = "Function:wallet|user_id:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println("Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_ReservationHistory = new WS_CallService(WalletHistoryActovity.this);
            ReservationHistory_NVP.add(new BasicNameValuePair("WS", base64_register));
            ReservationHistory_WebService = new ReservationHistory_WS(WalletHistoryActovity.this, ReservationHistory_NVP);
            ReservationHistory_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class ReservationHistory_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;
        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        Dialog dialog;
        Context context_aact;
        String wallet_type,wallet_money,wallet_date;

        public ReservationHistory_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub
            this.loginact = loginws;
            this.context_aact = context_ws;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = new Dialog(WalletHistoryActovity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(WalletHistoryActovity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//          isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_ReservationHistory = new WS_CallService(context_aact);
                jsonResponseString = service_ReservationHistory.getJSONObjestString(loginact, WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            WalletArray.clear();
            System.out.println("POST EXECUTE");
//            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                System.out.println(Preference_Constants.PREF_KEY_Service+"Values-->"+jsonResponse);
                Log.e("Status","Response"+jsonResponse);

String StatusStr=jObj.getString("Status");
                if(StatusStr.equalsIgnoreCase("Success"))
                {
                        JSONArray responseJson=jObj.getJSONArray("response");



                    for(int i=0;i<responseJson.length();i++)
                    {
                        BookingPojo walletDetails=new BookingPojo();
 wallet_type=                        responseJson.getJSONObject(i).getString("wallet_type");
                         wallet_money=                        responseJson.getJSONObject(i).getString("wallet_money");
                         wallet_date=                        responseJson.getJSONObject(i).getString("wallet_date");
//                        Log.e("Walletacti","Staus"+wallet_type);

                        walletDetails.setWalletDAte(wallet_date);
                        walletDetails.setwalletStatus(wallet_type);
                        walletDetails.setWalletMoney(wallet_money);

                        walletDetails.setWalletDAte(wallet_date);
                        walletDetails.setWallwallet_type(wallet_type);
                        walletDetails.setWallwallet_money(wallet_money);


                        WalletArray.add(walletDetails);

                    }




                    reservation_history_adapter = new WalletHistoryAdapter(context_aact, R.layout.reservaqtion_history_item, WalletArray);
                    walletHistoryList.setAdapter(reservation_history_adapter);

                    if(WalletArray.size()==0){
                        NoItems.setVisibility(View.VISIBLE);
                    }else{
                        NoItems.setVisibility(View.GONE);
                    }

                }





                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                if(WalletArray.size()==0){
                    NoItems.setVisibility(View.VISIBLE);
                }else{
                    NoItems.setVisibility(View.GONE);
                }
                System.out.println(e.toString() + "zcx");
                dialog.dismiss();
            }

        }

    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
