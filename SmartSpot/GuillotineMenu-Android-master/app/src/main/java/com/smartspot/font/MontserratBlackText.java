package com.smartspot.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by iyyapparajr on 12/12/2017.
 */

public class MontserratBlackText extends TextView {

    public MontserratBlackText(Context context) {
        super(context);
        setTypeface(MontserratBlack.getInstance(context).getTypeFace());
    }

    public MontserratBlackText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(MontserratBlack.getInstance(context).getTypeFace());
    }

    public MontserratBlackText(Context context, AttributeSet attrs,
                              int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(MontserratBlack.getInstance(context).getTypeFace());
    }

}