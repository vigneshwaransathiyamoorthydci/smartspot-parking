package com.smartspot.notification;

/**
 * Created by abimathi on 06-Sep-17.
 */

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.smartspot.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


public class Notificationcustom {

    private String TAG = Notificationcustom.class.getSimpleName();

    private Context mContext;

    public Notificationcustom(Context mContext) {
        this.mContext = mContext;
    }

    public void CustomNotification(Bitmap url, String title, String message, String time, Intent intent) {
        // Using RemoteViews to bind custom layouts into Notification
        final RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(),
                R.layout.customnotification);

        SharedPreferences prefs = mContext.getSharedPreferences("UserPref", Context.MODE_PRIVATE);

        // Open NotificationView.java Activity
        PendingIntent pIntent=null;
      /*  if(!AppPreferences.getBooleanFromStore(Preference_Constants.PREF_KEY_NOTIFICATION))
        {
            pIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        }
      */
            pIntent =PendingIntent.getActivity(mContext, 0, intent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
       // }


        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext)
                // Set Icon
                .setSmallIcon(R.drawable.smartspot_logo)
                        // Set Ticker Message
                .setTicker(title + " : " + message)
                        // Dismiss Notification
                .setAutoCancel(true)
                        // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                        // Set RemoteViews into Notification
                .setContent(remoteViews);

        if (prefs.getString("RingtoneUri", null) != null) {
            builder.setSound(Uri.parse(prefs.getString("RingtoneUri", null)));
            Log.e("Error", "Ring OK");
        } else {
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        }

        long t = System.currentTimeMillis();


        Notification notification = builder.build();

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.notification_title, title);
        remoteViews.setTextViewText(R.id.notification_message, message);
        //remoteViews.setTextViewText(R.id.notification_time, ToolsUtils.getTimeAsStringa());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            remoteViews.setTextColor(R.id.notification_title, Color.BLACK);
            remoteViews.setTextColor(R.id.notification_message, Color.GRAY);
            remoteViews.setTextColor(R.id.notification_time, Color.GRAY);
        }
        remoteViews.setImageViewResource(R.id.notification_image,R.drawable.appicon);

       /*        Log.e("erhzm",url+"hzm");
        if (url==null) {
            remoteViews.setImageViewResource(R.id.notification_image,R.drawable.item_bg);
        }
        else
            remoteViews.setImageViewBitmap(R.id.notification_image,url);
*/


        NotificationManager notificationmanager = (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(100, notification);
           /* ImageLoader imageLoader = NetworkController.getInstance().getImageLoader();

// If you are using normal ImageView
            imageLoader.get((ToolsUtils.cloud().url().generate(url)), new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // Log.e(TAG, "Image Load Error: " + error.getMessage());
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        // load image into imageview
                        remoteViews.setImageViewBitmap(R.id.notification_image,response.getBitmap());
                    }
                }
            });*/
        // }
        //else
       /* Picasso.with(mContext).load(*//*ToolsUtils.IMAGE_URL_100 + url*//*
        ToolsUtils.cloud().url().generate(url)).into(remoteViews, R.id.notification_image, AppConfig.NOTIFICATION_ID, notification);*/

        // Create Notification Manager


        PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wake");
        wakeLock.acquire();
    }
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Method checks if the app is in background or not
     *
     * @param context
     * @return
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
}
