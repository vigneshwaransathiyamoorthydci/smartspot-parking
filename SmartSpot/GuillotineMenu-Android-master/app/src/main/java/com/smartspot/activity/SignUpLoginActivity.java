package com.smartspot.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.smartspot.R;
import com.smartspot.font.ProximaNovaEdit;
import com.smartspot.font.RoximaNovaBold;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.WS_CallService;
import com.yqritc.scalablevideoview.ScalableVideoView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by iyyapparajr on 11/9/2017.
 */

public class SignUpLoginActivity extends Activity {

    private static int SPLASH_TIME_OUT = 3000;
    TextView app_name_text, forgot_password,textView;
    String checkStr;
    boolean atleastOneAlpha;
    String numStr;

    WS_CallService service_SignUp;
    ArrayList<NameValuePair> SignUp_NVP = new ArrayList<>();
    Load_SignUp_WS signUp_Hitservice;

    WS_CallService service_Login;
    ArrayList<NameValuePair> login_NVP = new ArrayList<>();
    Load_Login_WS Login_Hitservice;
    AlertDialog.Builder builder;
    LinearLayout login_submit_TextView;
    String error = "";
    TextView next_phoneNumber;
    private SurfaceView mSurfaceView;
    private MediaPlayer mMediaPlayer;
    SurfaceHolder mFirstSurface;
    private View overlayScreen;
    LinearLayout forgot_password_submit_TextView;
    EditText login_mobile_number, login_mobile_number_code;
    TextView email_error, loginButtonTextView, password_error, first_last_name_error, error_mobile_number, login_email_pass_error, forgot_email_error;
    ImageView join_email_back_button, join_userName_back_button, join_password_back_button, join_ph_no_back_button, login_back_button, forgot_back_imageview;
    private Uri mVideoUri;
    TextView loginSubmitButton;
    String userID, UserName, Email, Mobile;
    TextView login_password_error;
    //TextView loginButton, signupButton;
    ProximaNovaEdit email_ediTText, passwordEditText, login_email_ediTText, forgot_emailEditText, firstNameEdit, phone_number, login_password_ediTText;
    RelativeLayout loginscreenlayout, join_email_layout, join_password_layout, join_userName_layout, join_ph_no_layout, loginlayout, forgot_layout;
    Button loginButton, signupButton;
    ScalableVideoView mVideoView;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.sign_login_layout);

//      app_name_text=(TextView)findViewById(R.id.app_name_text);
//      Typeface font = Typeface.createFromAsset(getAssets(), "PROXIMANOVA_BOLD_WEBFONT.TTF");
//
//      app_name_text.setTypeface(font);
      // mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
    //    overlayScreen = findViewById(R.id.scrollOverLay);
       /* mSurfaceView = (ScalableVideoView) findViewById(R.id.video_view);
        try {
            //Uri uri=Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.smartspotsption3);

           // mSurfaceView.setDataSource(SignUpLoginActivity.this,uri);
            mSurfaceView.setRawData(R.raw.smartspotsption3);
            mSurfaceView.start();
        } catch (IOException ioe) {
            //handle error
        }*/

//        String firebaseId=   FirebaseInstanceId.getInstance().getToken();

//        Log.e("Firebase id ","Is-->"+firebaseId);
        String Token= AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
        Log.e("WWooWOCDDK",Token);


//        AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_FCMTOKEN);
//        Log.e("Stored","Value SignLoginOld"+AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN));
//


        email_error = (TextView) findViewById(R.id.email_error);
        password_error = (TextView) findViewById(R.id.password_error);
        first_last_name_error = (TextView) findViewById(R.id.first_last_name_error);
        login_email_pass_error = (TextView) findViewById(R.id.login_email_pass_error);
        forgot_email_error = (TextView) findViewById(R.id.forgot_email_error);

        error_mobile_number = (TextView) findViewById(R.id.error_mobile_number);
        next_phoneNumber = (TextView) findViewById(R.id.next_phoneNumber);
        loginButtonTextView = (TextView) findViewById(R.id.loginButtonTextView);

        loginSubmitButton = (TextView) findViewById(R.id.loginSubmitButton);

        app_name_text = (TextView) findViewById(R.id.app_name_text);

        login_mobile_number_code = (EditText) findViewById(R.id.login_mobile_number_code);
        login_mobile_number = (EditText) findViewById(R.id.login_mobile_number);


//        textView=(TextView)findViewById(R.id.textView);
//        String text = "<font color=#ffffff>SMART</font><font color=#51d1d6>SPOT</font>";
//        textView.setText(Html.fromHtml(text));



//      next_phoneNumber=(TextView)findViewById(R.id.next_phoneNumber);

        loginButton = (Button) findViewById(R.id.loginButtonTextView);
        signupButton = (Button) findViewById(R.id.joinButton);
        loginscreenlayout = (RelativeLayout) findViewById(R.id.loginscreenlayout);

        join_email_layout = (RelativeLayout) findViewById(R.id.join_email_layout);
        join_password_layout = (RelativeLayout) findViewById(R.id.join_password_layout);
        join_userName_layout = (RelativeLayout) findViewById(R.id.join_userName_layout);
        join_ph_no_layout = (RelativeLayout) findViewById(R.id.join_ph_no_layout);
        loginlayout = (RelativeLayout) findViewById(R.id.loginlayout);
        forgot_layout = (RelativeLayout) findViewById(R.id.forgot_layout);
        forgot_password_submit_TextView = (LinearLayout) findViewById(R.id.forgot_password_submit_TextView);

//      submit_forgot_password=(RelativeLayout)findViewById(R.id.submit_forgot_password);

        join_email_back_button = (ImageView) findViewById(R.id.join_email_back_button);
        join_userName_back_button = (ImageView) findViewById(R.id.join_userName_back_button);
        join_password_back_button = (ImageView) findViewById(R.id.join_password_back_button);
        join_ph_no_back_button = (ImageView) findViewById(R.id.join_ph_no_back_button);
        login_back_button = (ImageView) findViewById(R.id.login_back_button);
        forgot_back_imageview = (ImageView) findViewById(R.id.forgot_back_imageview);

        login_submit_TextView = (LinearLayout) findViewById(R.id.login_submit_Button_layout);

        email_ediTText = (ProximaNovaEdit) findViewById(R.id.join_emaileditText);
        email_ediTText.setImeOptions(EditorInfo.IME_ACTION_DONE);
//      lastNameEditTExt=(ProximaNovaEdit)findViewById(R.id.lastNameEditTExt);
//      lastNameEditTExt.setImeOptions(EditorInfo.IME_ACTION_DONE);

        login_password_error = (RoximaNovaBold) findViewById(R.id.login_password_error);

        login_password_ediTText = (ProximaNovaEdit) findViewById(R.id.login_password_ediTText);
        login_password_ediTText.setImeOptions(EditorInfo.IME_ACTION_DONE);

        passwordEditText = (ProximaNovaEdit) findViewById(R.id.passwordEditText);
        passwordEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);

        firstNameEdit = (ProximaNovaEdit) findViewById(R.id.firstNameEdit);
        firstNameEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);

        phone_number = (ProximaNovaEdit) findViewById(R.id.phone_number);
        phone_number.setImeOptions(EditorInfo.IME_ACTION_DONE);

//      lastNameEditTExt=(ProximaNovaEdit)findViewById(R.id.lastNameEditTExt);
        passwordEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);

        login_email_ediTText = (ProximaNovaEdit) findViewById(R.id.login_mobile_number);
        login_email_ediTText.setImeOptions(EditorInfo.IME_ACTION_DONE);

        forgot_emailEditText = (ProximaNovaEdit) findViewById(R.id.forgot_emailEditText);
        forgot_emailEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);

        forgot_password = (TextView) findViewById(R.id.forgot_password);


        /*Login Screen Sction Validation and Functionalities  STARTS*/


        login_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginlayout.startAnimation(outToRightAnimation());
                loginlayout.setVisibility(View.GONE);
//                join_password_back_button.setVisibility(View.GONE);
//                join_userName_layout.setVisibility(View.GONE);
//                join_ph_no_layout.setVisibility(View.GONE);
                email_ediTText.clearFocus();
                email_ediTText.setText("");
                loginscreenlayout.setVisibility(View.VISIBLE);
            }
        });
        phone_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                error_mobile_number.setText("");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        next_phoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent LoginIntent=new Intent(SignUpLoginActivity.this,MainActivity.class);
//                startActivity(LoginIntent);
//                finish();


                if (phone_number.getText().toString().trim().length() == 8) {
                    hitService();
                } else {
                    error_mobile_number.setVisibility(View.VISIBLE);
                    error_mobile_number.setText("Please enter valid mobile number");
//                    Toast.makeText(getApplicationContext(), "Please enter valid mobile number", Toast.LENGTH_LONG).show();
                }


            }
        });


        loginSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (login_email_ediTText.getText().toString().trim().isEmpty() || login_password_ediTText.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter email and password", Toast.LENGTH_LONG).show();
                } else {
                    hitServiceLogin();
                }
            }
        });

        /*Login Screen Sction Validation and Functionalities  ENDS*/


        join_email_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                join_email_layout.startAnimation(outToRightAnimation());
                join_email_layout.setVisibility(View.GONE);

                email_ediTText.clearFocus();
                email_ediTText.setText("");


//                join_password_back_button.setVisibility(View.GONE);
//                join_userName_layout.setVisibility(View.GONE);
//                join_ph_no_layout.setVisibility(View.GONE);
                loginscreenlayout.setVisibility(View.VISIBLE);
            }
        });

        forgot_password_submit_TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (forgot_emailEditText.getText().toString().trim().isEmpty()) {
                    forgot_email_error.setVisibility(View.VISIBLE);
                    forgot_email_error.setText("Please enter a valid email address");
                } else {
                    emailValidation(forgot_emailEditText.getText().toString().trim());
                    if (checkStr.equalsIgnoreCase("Failed")) {
                        forgot_email_error.setVisibility(View.VISIBLE);
                        forgot_email_error.setText("Please enter a valid email address");
                    } else {
                        Intent LoginIntent = new Intent(SignUpLoginActivity.this, MainActivity.class);
                        startActivity(LoginIntent);
                        finish();
                    }
                }
            }
        });


        forgot_back_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgot_layout.startAnimation(outToRightAnimation());
                forgot_layout.setVisibility(View.GONE);
                loginlayout.setVisibility(View.VISIBLE);
            }
        });

        join_password_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                join_password_layout.startAnimation(outToRightAnimation());
                passwordEditText.setText("");
                join_password_layout.setVisibility(View.GONE);
                join_email_layout.setVisibility(View.VISIBLE);
            }
        });


        join_userName_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                join_userName_layout.startAnimation(outToRightAnimation());
                join_userName_layout.setVisibility(View.GONE);
//                lastNameEditTExt.setText("");
                firstNameEdit.setText("");

                join_password_layout.setVisibility(View.VISIBLE);
            }
        });

        join_ph_no_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                join_ph_no_layout.startAnimation(outToRightAnimation());
                join_ph_no_layout.setVisibility(View.GONE);
                join_userName_layout.setVisibility(View.VISIBLE);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginscreenlayout.setVisibility(View.GONE);
                join_email_layout.setVisibility(View.VISIBLE);
                join_email_layout.startAnimation(inFromRightAnimation());


            }
        });


        join_password_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                loginscreenlayout.setVisibility(View.GONE);
//                join_email_layout.setVisibility(View.VISIBLE);
//                join_email_layout.startAnimation(inFromRightAnimation());


            }
        });
        login_password_ediTText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                login_email_pass_error.setText("");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


       /* ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                signupButton,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();

        ObjectAnimator scaleDown2 = ObjectAnimator.ofPropertyValuesHolder(
                loginButton,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown2.setDuration(310);

        scaleDown2.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown2.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown2.start();

*/

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//***************************************************************************for animation section***************************************************************************

//                loginscreenlayout.setVisibility(View.GONE);
//                join_email_layout.setVisibility(View.VISIBLE);
//                join_email_layout.startAnimation(inFromRightAnimation());

//***************************************************************************for animation section***************************************************************************



                //Intent i = new Intent(SignUpLoginActivity.this, RegisterOtp.class);
                Intent i = new Intent(SignUpLoginActivity.this, RegisterOtp.class);
                startActivity(i);
            finish();
              //  mVideoView.getKeyDispatcherState();


            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//***************************************************************************for animation section***************************************************************************
//                loginscreenlayout.setVisibility(View.GONE);
//                loginlayout.setVisibility(View.VISIBLE);
//                loginlayout.startAnimation(inFromRightAnimation());
//***************************************************************************for animation section***************************************************************************

                Intent i = new Intent(SignUpLoginActivity.this, LoginOtp.class);
//              Intent i = new Intent(SplashNewActivity.this, SignUpActivity.class);
                startActivity(i);
                finish();
            }
        });


        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                loginlayout.setVisibility(View.GONE);
                forgot_layout.setVisibility(View.VISIBLE);
                forgot_layout.startAnimation(inFromRightAnimation());
            }
        });

        forgot_emailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.i(TAG,"Enter pressed");


                    if (forgot_emailEditText.getText().toString().trim().isEmpty()) {
                        email_error.setVisibility(View.VISIBLE);
                        email_error.setText(R.string.email_validation);
                    } else {

                        emailValidation(forgot_emailEditText.getText().toString().trim());
                        if (checkStr.equalsIgnoreCase("Failed")) {
                            email_error.setVisibility(View.VISIBLE);
                        } else {
//                        email_error.setVisibility(View.GONE);
//                        join_email_layout.setVisibility(View.GONE);
//                        join_password_layout.setVisibility(View.VISIBLE);
//                        join_password_layout.startAnimation(inFromRightAnimation());

                        }
                    }

                }
                return false;
            }
        });


        phone_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.i(TAG,"Enter pressed");


//                    emailValidation();

                    if (phone_number.getText().toString().length() < 8) {
                        error_mobile_number.setVisibility(View.VISIBLE);
                    } else {

                        Intent LoginIntent = new Intent(SignUpLoginActivity.this, MainActivity.class);
                        startActivity(LoginIntent);
                        finish();
//                        email_error.setVisibility(View.GONE);
//                        join_email_layout.setVisibility(View.GONE);
//                        join_password_layout.setVisibility(View.VISIBLE);
//                        join_password_layout.startAnimation(inFromRightAnimation());

                    }

                }
                return false;
            }
        });
        login_email_ediTText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                login_email_pass_error.setText("");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        login_email_ediTText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.i(TAG,"Enter pressed");


                    emailValidation(login_email_ediTText.getText().toString().trim());

                    if (checkStr.equalsIgnoreCase("Failed")) {
                        email_error.setVisibility(View.VISIBLE);
                        email_error.setText(R.string.email_validation);

                    } else {
//                        email_error.setVisibility(View.GONE);
//                        join_email_layout.setVisibility(View.GONE);
//                        join_password_layout.setVisibility(View.VISIBLE);
//                        join_password_layout.startAnimation(inFromRightAnimation());

                    }

//                    join_email_layout.setVisibility(View.GONE);
//                    join_password_layout.setVisibility(View.VISIBLE);
//                    join_password_layout.startAnimation(inFromRightAnimation());

                }
                return false;
            }
        });


        email_ediTText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                email_error.setText("");
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });


        email_ediTText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.i(TAG,"Enter pressed");


                    emailValidation(email_ediTText.getText().toString().trim());

                    if (checkStr.equalsIgnoreCase("Failed")) {
                        email_error.setVisibility(View.VISIBLE);
                        email_error.setText(R.string.email_validation);
                    } else {
                        System.out.println(Preference_Constants.PREF_KEY_Service+"submit email-->");
                        email_error.setVisibility(View.GONE);
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        join_email_layout.setVisibility(View.GONE);
                        join_password_layout.setVisibility(View.VISIBLE);
                        join_password_layout.startAnimation(inFromRightAnimation());

                    }

//                    join_email_layout.setVisibility(View.GONE);
//                    join_password_layout.setVisibility(View.VISIBLE);
//                    join_password_layout.startAnimation(inFromRightAnimation());

                }
                return false;
            }
        });


        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                password_error.setText("");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.i(TAG,"Enter pressed");

//                    hasNums(passwordEditText.getText().toString().trim());
                    passwordEditText.getText().toString().replaceAll("( +)", " ").trim();
                    if (passwordEditText.getText().toString().replaceAll("( +)", " ").trim().length() < 8) {
                        password_error.setVisibility(View.VISIBLE);
                        password_error.setText(R.string.pass_validation);
                    } else {

                        hasNums(passwordEditText.getText().toString().trim());
                        System.out.println("values is number -->" + hasNums(passwordEditText.getText().toString().trim()));

                        atleastOneAlpha = passwordEditText.getText().toString().trim().matches(".*[a-zA-Z]+.*");

                        System.out.println("values is alphabet is -->" + atleastOneAlpha);


                        if (hasNums(passwordEditText.getText().toString().trim()).equalsIgnoreCase("NONE")) {
                            password_error.setVisibility(View.VISIBLE);
                            password_error.setText("password should have one number");
                        } else if (atleastOneAlpha == false) {
                            password_error.setVisibility(View.VISIBLE);
                            password_error.setText("password should have one alphabet");
                        } else if (hasNums(passwordEditText.getText().toString().trim()).equalsIgnoreCase("NONE") && atleastOneAlpha == false)

                        {
                            password_error.setVisibility(View.VISIBLE);
                            password_error.setText("password should have one alphabet and one number");
                        } else {
                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            join_password_layout.setVisibility(View.GONE);
                            join_userName_layout.setVisibility(View.VISIBLE);
                            join_userName_layout.startAnimation(inFromRightAnimation());
                        }
                    }


                }
                return false;
            }
        });


        firstNameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                first_last_name_error.setText("");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        firstNameEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.i(TAG,"Enter pressed");


                    System.out.println(Preference_Constants.PREF_KEY_Service + "first name" + firstNameEdit.getText().toString().replaceAll("( +)", " ").trim());

                    System.out.println(Preference_Constants.PREF_KEY_Service + "first name text length-->" + firstNameEdit.getText().toString().trim().length());
                    if (firstNameEdit.getText().toString().replaceAll("( +)", " ").trim().length() < 6 || firstNameEdit.getText().toString().isEmpty()) {
                        first_last_name_error.setVisibility(View.VISIBLE);
                        firstNameEdit.setFocusable(true);
                        first_last_name_error.setText(R.string.first_name_validation);
                    }

//                    else   if(lastNameEditTExt.getText().toString().length()<4)
//                    {
//                        first_last_name_error.setVisibility(View.VISIBLE);
//                        lastNameEditTExt.setFocusable(true);
//                        first_last_name_error.setText(R.string.last_name_validation);
//                    }
//
                    else {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        first_last_name_error.setVisibility(View.GONE);
                        join_userName_layout.setVisibility(View.GONE);
                        join_ph_no_layout.setVisibility(View.VISIBLE);
                        join_ph_no_layout.startAnimation(inFromRightAnimation());
                    }
                }
                return false;
            }
        });


//        lastNameEditTExt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
////                    Log.i(TAG,"Enter pressed");
//
//                    if(firstNameEdit.getText().toString().length()<4)
//                    {
//                        first_last_name_error.setVisibility(View.VISIBLE);
//                        first_last_name_error.setText(R.string.first_name_validation);
//                    }
//
//                    else   if(lastNameEditTExt.getText().toString().length()<4)
//                    {
//                        first_last_name_error.setVisibility(View.VISIBLE);
//                        first_last_name_error.setText(R.string.last_name_validation);
//                    } else
//                    {
//                        first_last_name_error.setVisibility(View.GONE);
//                        join_userName_layout.setVisibility(View.GONE);
//                        join_ph_no_layout.setVisibility(View.VISIBLE);
//                        join_ph_no_layout.startAnimation(inFromRightAnimation());
//                    }
//
//
//
//                }
//                return false;
//            }
//        });

    }

    private void hitServiceLogin() {


        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Login|Mobile Number:971" + login_mobile_number_code.getText().toString().trim() + login_mobile_number.getText().toString().trim() + "|Password:" + login_password_ediTText.getText().toString() + "|LoginType:Manual";
            System.out.println("Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_Login = new WS_CallService(getApplicationContext());
            login_NVP.add(new BasicNameValuePair("WS", base64_register));
            Login_Hitservice = new Load_Login_WS(SignUpLoginActivity.this, login_NVP);
            Login_Hitservice.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    private void hitService() {

        byte[] data;
        try {
//          String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Registration|Mobile Number:" + "971" + phone_number.getText().toString().trim() + "|Password:" + passwordEditText.getText().toString().trim() + "|Name:" + firstNameEdit.getText().toString() + "|Email address:" + email_ediTText.getText().toString().trim() + "|device id:|SignupType:Manual|fcm_key:"+ AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW);
            System.out.println(Preference_Constants.PREF_KEY_Service+"Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_SignUp = new WS_CallService(getApplicationContext());
            SignUp_NVP.add(new BasicNameValuePair("WS", base64_register));
            signUp_Hitservice = new Load_SignUp_WS(SignUpLoginActivity.this, SignUp_NVP);
            signUp_Hitservice.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String hasNums(String str) {
        char[] nums = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char[] toChar = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            toChar[i] = str.charAt(i);
            for (int j = 0; j < nums.length; j++) {
                if (toChar[i] == nums[j]) {
                    return str;
                }
            }
        }
        return "NONE";
    }

    private void LoadVideo(){


        mVideoView = (ScalableVideoView) findViewById(R.id.surfaceViewFrame);
        // findViewById(R.id.btn_next).setOnClickListener(this);

        try {
            mVideoView.setRawData(R.raw.smart);
            mVideoView.setVolume(0, 0);
            mVideoView.setLooping(true);
            mVideoView.prepareAsync(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mVideoView.start();
                }
            });
           // mVideoView.start();


        } catch (IOException ioe) {
            //ignore
        }

    }
    private void loadVideo() {


     /*   MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(mSurfaceView);

        //specify the location of media file
        Uri uri=Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.smartspotsption3);

        //Setting MediaController and URI, then starting the videoView
        mSurfaceView.setMediaController(mediaController);
        mSurfaceView.setVideoURI(uri);
        mSurfaceView.requestFocus();*/
       // mSurfaceView.start();
        //mSurfaceView.se


//        mVideoUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bg_home_video);
//        mMediaPlayer = MediaPlayer.create(SignUpLoginActivity.this, R.raw.bg_home_video);


//        mVideoUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video);
//        mMediaPlayer = MediaPlayer.create(SignUpLoginActivity.this, R.raw.video);


        mVideoUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.smart);
        mMediaPlayer = MediaPlayer.create(SignUpLoginActivity.this, R.raw.smart);
        SurfaceHolder holder = mSurfaceView.getHolder();
        overlayScreen.setAlpha(0.05f);
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @SuppressLint("NewApi")
            @Override
            public void surfaceCreated(final SurfaceHolder holder) {
                mFirstSurface = holder;
                if (mFirstSurface.isCreating()) {
                    mMediaPlayer = MediaPlayer.create(SignUpLoginActivity.this, mVideoUri, holder);
                    mMediaPlayer.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
                    mMediaPlayer.setLooping(true);
                    mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            mMediaPlayer.start();
                            mMediaPlayer.setDisplay(holder);
                            int videoWidth = mMediaPlayer.getVideoWidth();
                            int videoHeight = mMediaPlayer.getVideoHeight();
                            float videoProportion = (float) videoWidth / (float) videoHeight;
                            int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
                            int screenHeight = getWindowManager().getDefaultDisplay().getHeight();
                            float screenProportion = (float) screenWidth / (float) screenHeight;
                            android.view.ViewGroup.LayoutParams lp = mSurfaceView.getLayoutParams();

                            if (videoProportion > screenProportion) {
                                lp.width = screenWidth;
                                lp.height = (int) ((float) screenWidth / videoProportion);
                            } else {
                                lp.width = (int) (videoProportion * (float) screenHeight);
                                lp.height = screenHeight;
                            }
                         //   mSurfaceView.setLayoutParams(lp);



                        }
                    });


                    mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            //check permission for location


                        }
                    });
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
            }
        });
    }

    private Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }


    private Animation outToRightAnimation() {

        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }


    public void validatePopup(String content) {

        final Dialog dialog = new Dialog(SignUpLoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customdialogios);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView titletext = (TextView) dialog.findViewById(R.id.barottastext);
        TextView contenttext = (TextView) dialog.findViewById(R.id.contenttext);
        TextView negativebuttontext = (TextView) dialog.findViewById(R.id.negativebutton);
        TextView postivebuttontext = (TextView) dialog.findViewById(R.id.postivebutton);

        negativebuttontext.setVisibility(View.GONE);

        contenttext.setText(content);
        postivebuttontext.setText("Ok");
        //   negativebuttontext.setText(neagtive);

        dialog.show();

        postivebuttontext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }

        });

    }


    public boolean emailValidation(String emailString) {
        boolean check = true;
        String error = "";


        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
//        String email = email_ediTText.getText().toString().trim();
        String email = emailString;
        if (emailString.length() == 0) {
            error = "Enter a Valid Email";
            check = false;
            checkStr = "Failed";
//            validatePopup(error);
            return check;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            error = "Invalid Email";
            checkStr = "Failed";
            check = false;
//            validatePopup(error);

            return check;
        } else {
//            check = true;
            checkStr = "Success";
        }

        return check;
    }


    public class Load_SignUp_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;

        Dialog dialog;
        Context context_aact;


        public Load_SignUp_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(SignUpLoginActivity.this);
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();


            dialog = new Dialog(SignUpLoginActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SignUpLoginActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();



//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SignUp = new WS_CallService(context_aact);
                jsonResponseString = service_SignUp.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {

                    String Message = jObj.getString("response");


                    String    user_status=jObj.getJSONArray("response").getJSONObject(0).getString("user_status");
                    String         user_last_login=jObj.getJSONArray("response").getJSONObject(0).getString("user_last_login");
                    String  user_last_logout=jObj.getJSONArray("response").getJSONObject(0).getString("user_last_logout");
                    String           user_name=jObj.getJSONArray("response").getJSONObject(0).getString("user_name");
                    String   user_mobile_number=jObj.getJSONArray("response").getJSONObject(0).getString("user_mobile_number");
                    String            user_email=jObj.getJSONArray("response").getJSONObject(0).getString("user_email");
                    String    user_device_id=jObj.getJSONArray("response").getJSONObject(0).getString("user_device_id");
                    String         user_id=jObj.getJSONArray("response").getJSONObject(0).getString("user_id");



                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_ID, user_id);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME, user_name);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_EMAIL, user_email);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_MOBILE, user_mobile_number);



                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_STATUS, user_status);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_LAST_LOGIN, user_last_login);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_LAST_LOGOUT, user_last_logout);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_DEVICE_ID, user_device_id);




//                    String user_status=jObj.
//                    user_last_login
//                    user_last_logout
//                    user_name
//                    user_mobile_number
//                    user_email
//                    user_device_id
//                    user_id
//                    user_profile_imageuser_id
//


//                  Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();

                    Intent LoginIntent = new Intent(SignUpLoginActivity.this, MainActivity.class);
                    startActivity(LoginIntent);
                    finish();

                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

                    if(Message.contains("ocialIDEmailSignupTypeSocialType"))
                    {

                        join_ph_no_layout.startAnimation(outToRightAnimation());
                        join_ph_no_layout.setVisibility(View.GONE);
                        join_email_layout.setVisibility(View.VISIBLE);

                        join_email_layout.startAnimation(inFromRightAnimation());
                        email_error.setVisibility(View.VISIBLE);
                        email_error.setText(Message);

                    }
                    else if(Message.contains("User Name already exist"))
                    {

                        join_ph_no_layout.startAnimation(outToRightAnimation());
                        join_ph_no_layout.setVisibility(View.GONE);
                        join_userName_layout.setVisibility(View.VISIBLE);
                        join_userName_layout.startAnimation(inFromRightAnimation());
                        first_last_name_error.setVisibility(View.VISIBLE);
                        first_last_name_error.setText(Message);

                    }else if(Message.contains("Email already exists"))
                    {

                        join_ph_no_layout.startAnimation(outToRightAnimation());
                        join_ph_no_layout.setVisibility(View.GONE);
                        join_email_layout.setVisibility(View.VISIBLE);
                        join_email_layout.startAnimation(inFromRightAnimation());
                        email_error.setVisibility(View.VISIBLE);
                        email_error.setText(Message);

                    }else
                    {
                        join_ph_no_layout.startAnimation(outToRightAnimation());
                        join_ph_no_layout.setVisibility(View.GONE);
                        join_userName_layout.setVisibility(View.VISIBLE);
                        join_userName_layout.startAnimation(inFromRightAnimation());
                        first_last_name_error.setVisibility(View.VISIBLE);
                        first_last_name_error.setText(Message);

                    }

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    public class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;


        Dialog dialog;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(SignUpLoginActivity.this);
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();

            dialog = new Dialog(SignUpLoginActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(SignUpLoginActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();



//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("Status");
                if (status.toString().equalsIgnoreCase("Success")) {

                    String Message = jObj.getString("Response");
                    System.out.println("Login response-->" + Message);

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();


                    userID = jObj.getJSONArray("Response").getJSONObject(0).getString("UserID");
                    UserName = jObj.getJSONArray("Response").getJSONObject(0).getString("UserName");
                    Email = jObj.getJSONArray("Response").getJSONObject(0).getString("Email");
                    Mobile = jObj.getJSONArray("Response").getJSONObject(0).getString("Mobile");


                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_ID, userID);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME, UserName);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_EMAIL, Email);
                    AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_MOBILE, Mobile);


                    Intent LoginIntent = new Intent(SignUpLoginActivity.this, MainActivity.class);
                    startActivity(LoginIntent);
                    finish();

                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

                    login_email_pass_error.setVisibility(View.VISIBLE);
                    login_email_pass_error.setText(Message);

//                    Toast.makeText(getApplicationContext(),Message,Toast.LENGTH_LONG).show();
                }

                try {
                    dialog.dismiss();
                } catch (Exception e) {

                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        System.out.println(Preference_Constants.PREF_KEY_Service+"Back presed");


        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked


                        AppPreferences.clearAll(SignUpLoginActivity.this);

                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
//                        System.exit(1);

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
//						      dialog.dismiss();

                        break;
                }
            }
        };
     //   builder = new AlertDialog.Builder(SignUpLoginActivity.this);
 //       builder.setMessage("Do you want to Exit?").setPositiveButton("Yes", dialogClickListener)
  //              .setNegativeButton("No", dialogClickListener).show();


      //  ExitPopup();
        AppPreferences.clearAll(SignUpLoginActivity.this);

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    public void ExitPopup(){

        final Dialog dialog1 = new Dialog(SignUpLoginActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.exit_popup);

        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        dialog1.show();


        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                AppPreferences.clearAll(SignUpLoginActivity.this);

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

        TextView  no_popup=(TextView)dialog1.findViewById(R.id.noPopup);
        no_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();

            }
        });

    }



    @Override
    protected void onPause() {
        super.onPause();

        try
        {
            mVideoView.stop();
            mVideoView.reset();
            mVideoView.release();
        }
        catch (Exception e)
        {

        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadVideo();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


       /* try {
            mVideoView.release();
        }
        catch (Exception e)
        {

        }*/
    }
}

