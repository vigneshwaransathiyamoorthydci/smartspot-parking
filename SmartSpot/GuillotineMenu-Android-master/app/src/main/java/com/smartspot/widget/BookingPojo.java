package com.smartspot.widget;

/**
 * Created by iyyapparajr on 9/12/2017.
 */

public class BookingPojo {

    String bookinIdPojo;
    String booking_user_idPojo;
    String bookingTimePojo;
    String bookedTill;

    public String getBookedTill() {
        return bookedTill;
    }

    public void setBookedTill(String bookedTill) {
        this.bookedTill = bookedTill;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;
    String bookingLocationNamePojo;
    String bookingLocationIdPojo;
    String bookingSlotIdPojo;
    String bookingPaymentAmountPojo;
    String bookingReservationreservationstatusPojo;
    String bookingreservationCodePojo;
    String bookingCityNamePojo;
    String booking_user_paymentStatusPojo;

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    String booking_status;
    String amount;
    String wallet_date,wallet_money,wallet_type;

    public String getBooking_waiting_until() {
        return booking_waiting_until;
    }

    public void setBooking_waiting_until(String booking_waiting_until) {
        this.booking_waiting_until = booking_waiting_until;
    }

    String booking_waiting_until;
    String booking_barrier_status;
    String r_booking_id;
    String booking_barrier_name,booking_barrier_id;


    String Wallwallet_type,Wallwallet_money,Wallwallet_date;

    public String getWallwallet_type() {
        return Wallwallet_type;
    }

    public void setWallwallet_type(String wallwallet_type) {
        Wallwallet_type = wallwallet_type;
    }

    public String getWallwallet_money() {
        return Wallwallet_money;
    }

    public void setWallwallet_money(String wallwallet_money) {
        Wallwallet_money = wallwallet_money;
    }

    public String getWallwallet_date() {
        return Wallwallet_date;
    }

    public void setWallwallet_date(String wallwallet_date) {
        Wallwallet_date = wallwallet_date;
    }

    String W_booking_id,W_booking_time,W_booking_location_id,W_booking_city_id,W_booking_location_name,
            W_booking_city_name;

    public String getW_booking_id() {
        return W_booking_id;
    }

    public void setW_booking_id(String w_booking_id) {
        W_booking_id = w_booking_id;
    }

    public String getW_booking_time() {
        return W_booking_time;
    }

    public void setW_booking_time(String w_booking_time) {
        W_booking_time = w_booking_time;
    }

    public String getW_booking_location_id() {
        return W_booking_location_id;
    }

    public void setW_booking_location_id(String w_booking_location_id) {
        W_booking_location_id = w_booking_location_id;
    }

    public String getW_booking_city_id() {
        return W_booking_city_id;
    }

    public void setW_booking_city_id(String w_booking_city_id) {
        W_booking_city_id = w_booking_city_id;
    }

    public String getW_booking_location_name() {
        return W_booking_location_name;
    }

    public void setW_booking_location_name(String w_booking_location_name) {
        W_booking_location_name = w_booking_location_name;
    }

    public String getW_booking_city_name() {
        return W_booking_city_name;
    }

    public void setW_booking_city_name(String w_booking_city_name) {
        W_booking_city_name = w_booking_city_name;
    }

    public String getBooking_barrier_name() {
        return booking_barrier_name;
    }

    public void setBooking_barrier_name(String booking_barrier_name) {
        this.booking_barrier_name = booking_barrier_name;
    }

    public String getBooking_barrier_id() {
        return booking_barrier_id;
    }

    public void setBooking_barrier_id(String booking_barrier_id) {
        this.booking_barrier_id = booking_barrier_id;
    }

    String booking_id,booking_user_id,booking_time,booking_location_id,booking_slot_id,booking_payment_amount,booking_location_name,
            booking_city_name,booking_reservation_code,booking_payment_status;

    public String getBooking_barrier_status() {
        return booking_barrier_status;
    }

    public void setBooking_barrier_status(String booking_barrier_status) {
        this.booking_barrier_status = booking_barrier_status;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_user_id() {
        return booking_user_id;
    }

    public void setBooking_user_id(String booking_user_id) {
        this.booking_user_id = booking_user_id;
    }

    public String getBooking_time() {
        return booking_time;
    }

    public void setBooking_time(String booking_time) {
        this.booking_time = booking_time;
    }

    public String getBooking_location_id() {
        return booking_location_id;
    }

    public void setBooking_location_id(String booking_location_id) {
        this.booking_location_id = booking_location_id;
    }

    public String getBooking_slot_id() {
        return booking_slot_id;
    }

    public void setBooking_slot_id(String booking_slot_id) {
        this.booking_slot_id = booking_slot_id;
    }

    public String getBooking_payment_amount() {
        return booking_payment_amount;
    }

    public void setBooking_payment_amount(String booking_payment_amount) {
        this.booking_payment_amount = booking_payment_amount;
    }

    public String getBooking_location_name() {
        return booking_location_name;
    }

    public void setBooking_location_name(String booking_location_name) {
        this.booking_location_name = booking_location_name;
    }

    public String getBooking_city_name() {
        return booking_city_name;
    }

    public void setBooking_city_name(String booking_city_name) {
        this.booking_city_name = booking_city_name;
    }

    public String getBooking_reservation_code() {
        return booking_reservation_code;
    }

    public void setBooking_reservation_code(String booking_reservation_code) {
        this.booking_reservation_code = booking_reservation_code;
    }

    public String getBooking_payment_status() {
        return booking_payment_status;
    }

    public void setBooking_payment_status(String booking_payment_status) {
        this.booking_payment_status = booking_payment_status;
    }

    public String getSlotStatus() {
        return bookinIdPojo;
    }

    public void setSlotStatus(String company) {
        this.bookinIdPojo = company;
    }




    public String getwalletStatus() {
        return wallet_type;
    }

    public void setwalletStatus(String company) {
        this.wallet_type = company;
    }



    public String getWalletMoney() {
        return wallet_money;
    }

    public void setWalletMoney(String company) {
        this.wallet_money = company;
    }


    public String getWalletDAte() {
        return wallet_date;
    }

    public void setWalletDAte(String company) {
        this.wallet_date = company;
    }




    public String getBookingId() {
        return bookinIdPojo;
    }

    public void setBookingId(String company) {
        this.bookinIdPojo = company;
    }


    public String getBookingUserId() {
        return booking_user_idPojo;
    }

    public void setBookingUserId(String company) {
        this.booking_user_idPojo = company;
    }



    public String getBookingPaymentStatus() {
        return booking_user_paymentStatusPojo;
    }

    public void setBookingPaymentStatus(String company) {
        this.booking_user_paymentStatusPojo = company;
    }


    public String getBookingTime() {
        return booking_user_idPojo;
    }

    public void setBookingTime(String company) {
        booking_user_idPojo = company;
    }


    public String getBookingLocationId() {
        return bookingLocationIdPojo;
    }

    public void setBookingLocationId(String company) {
        bookingLocationIdPojo = company;
    }


    public String getBookingCityId() {
        return bookingLocationIdPojo;
    }

    public void setBookingCityId(String company) {
        bookingLocationIdPojo = company;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount1) {
        amount = amount1;
    }


    public String getBookingLocationName() {
        return bookingLocationNamePojo;
    }

    public void setBookingLocationName(String company) {
        bookingLocationNamePojo = company;
    }


    public String getBookingCityName() {
        return bookingCityNamePojo;
    }

    public void setBookingCityName(String company) {
        this.bookingCityNamePojo = company;
    }


    public String getBookingBarrierId() {
        return bookingSlotIdPojo;
    }

    public void setBookingBarrierId(String company) {
        this.bookingSlotIdPojo = company;
    }

    public String getBookingBarrierName() {
        return bookingSlotIdPojo;
    }

    public void setBookingBarrierName(String company) {
        this.bookingSlotIdPojo = company;
    }


    public String getBookingSlotId() {
        return bookingPaymentAmountPojo;
    }

    public void setBookingSlotId(String company) {
        bookingPaymentAmountPojo = company;
    }


    public String getBookingPaymentAmount() {
        return bookingPaymentAmountPojo;
    }




    public void setBookingPaymentAmount(String company) {
        bookingPaymentAmountPojo = company;
    }

    public String getBookingBarrierStatus() {
        return bookingPaymentAmountPojo;
    }




    public void setBookingBarrierStatus(String company) {
        bookingPaymentAmountPojo = company;
    }







    public String getBookingReservationStatus() {
        return bookingReservationreservationstatusPojo;
    }

    public void setBookingReservationStatus(String company) {
        bookingReservationreservationstatusPojo = company;
    }

    public String getBookingReservationcode() {
        return bookingreservationCodePojo;
    }

    public void setBookingreservationCodePojo(String company) {
        bookingreservationCodePojo = company;
    }









}
