package com.smartspot.activity;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.toolbox.Volley;
import com.smartspot.R;
import com.yqritc.scalablevideoview.ScalableVideoView;

/**
 * Created by iyyapparajr on 12/18/2017.
 */

public class testing extends Activity {


    ScalableVideoView mVideoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testing);

        LoadVideo();
    }

    private void LoadVideo(){


        mVideoView = (ScalableVideoView) findViewById(R.id.surfaceViewFrame);
        // findViewById(R.id.btn_next).setOnClickListener(this);

        try {
            mVideoView.setRawData(R.raw.smart);
            mVideoView.setVolume(0, 0);
            mVideoView.setLooping(true);
            mVideoView.prepare(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mVideoView.start();
                }
            });
        } catch (Exception e) {
            //ignore
            Log.e("error",e.getStackTrace().toString());
        }

    }

}
