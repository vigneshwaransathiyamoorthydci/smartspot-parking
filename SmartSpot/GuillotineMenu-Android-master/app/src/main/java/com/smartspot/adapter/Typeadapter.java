package com.smartspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.smartspot.R;

import java.util.ArrayList;

/**
 * Created by iyyapparajr on 12/26/2017.
 */

public class Typeadapter extends ArrayAdapter {



    private Context context;
    private ArrayList<CountryPojo> weightlist;
    Activity act;
    public Typeadapter(Context context, int textViewResourceId, ArrayList<CountryPojo> weightlist) {

        super(context, textViewResourceId,weightlist);
        this.context=context;
        act= (Activity) context;
        this.weightlist=weightlist;

    }
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.carddropdown, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
        //v.setTypeface(myTypeFace);
        make.setText(weightlist.get(position).getCode());
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.downdropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.downdropdownview);
        /*Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");
        v.setTypeface(myTypeFace);
        v.setText(itemList.get(position));*/


        make.setText(weightlist.get(position).getCode());

        return row;
    }

}

