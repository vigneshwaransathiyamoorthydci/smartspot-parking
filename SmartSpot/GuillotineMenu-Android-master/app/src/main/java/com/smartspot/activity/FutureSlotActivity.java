package com.smartspot.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smartspot.R;
import com.smartspot.fragment.NewBookingFragment;
import com.smartspot.fragment.WalletFragment;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.widget.WS_CallService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by iyyapparajr on 1/3/2018.
 */

public class FutureSlotActivity extends Activity{

    TextView msg_txt, ok;
    ImageView backImage;
    String datetimeStr;


    WS_CallService service_BookSlotFromServer;
    ArrayList<NameValuePair> BookSlotFromServer_NVP = new ArrayList<>();
    WS_CallService service_SearchSlots;

    WS_CallService service_Wallet;
    ArrayList<NameValuePair> Wallet_NVP=new ArrayList<>();
    Load_Wallet_WS wallet_Hitservice;
    String user_wallet_amount;
    int Apply_Hourly_Charges;
    int user_free_parking;
    int creditcard;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.future_slot);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        msg_txt=(TextView)findViewById(R.id.msg_txt);
        ok=(TextView)findViewById(R.id.ok);
        backImage=(ImageView)findViewById(R.id.back);

        if(getIntent()!=null){
            datetimeStr=getIntent().getStringExtra("date");
            Apply_Hourly_Charges=getIntent().getIntExtra("Apply_Hourly_Charges",0);
            user_wallet_amount=getIntent().getStringExtra("user_wallet_amount");
            user_free_parking= Integer.parseInt((getIntent().getStringExtra("user_free_parking")));
        }
        String futureAmt=AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_BOOKING_PRICE);
        if (user_free_parking>0 )
        {
            msg_txt.setText("Future Parking spot will be reserved for free , your available free parking"+" "+user_free_parking);
        }
        else {
            msg_txt.setText("Future bookings are charged at Aed " + futureAmt + " for 1 hour from the time selected. Once you arrive,Feel free to park for as long as you like as per the location guidelines.");
        }




        Log.e("dateTimeStr","->"+datetimeStr);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // FutureSlot();
                hitWalletervice();
            }
        });

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FutureSlotActivity.this.finish();
            }
        });

        ObjectAnimator reserveDown = ObjectAnimator.ofPropertyValuesHolder(
                ok,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        reserveDown.setDuration(310);

        reserveDown.setRepeatCount(ObjectAnimator.INFINITE);
        reserveDown.setRepeatMode(ObjectAnimator.REVERSE);

        reserveDown.start();


    }

    private void hitWalletervice() {

        byte[] data;
        try {
//            String login_string = "Function:myWallet|user_id:5";
            String login_string = "Function:myWallet|user_id:"+ AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println("Fucntionals Wallet request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_Wallet = new WS_CallService(FutureSlotActivity.this);
            Wallet_NVP.add(new BasicNameValuePair("WS", base64_register));
            wallet_Hitservice = new Load_Wallet_WS(FutureSlotActivity.this, Wallet_NVP);
            wallet_Hitservice.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    public class Load_Wallet_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;

        Dialog dialog;
        Context context_aact;


        String user_nameService,user_emailService,user_mobile_numberService
                ,user_statusService,user_last_loginService,user_last_logoutService,user_profile_imageService,user_device_idService;
        public Load_Wallet_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(getActivity());
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();


            dialog = new Dialog(FutureSlotActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(FutureSlotActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Wallet = new WS_CallService(context_aact);
                jsonResponseString = service_Wallet.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            try {
                dialog.dismiss();
            }catch (Exception e){}

            System.out.println("POST EXECUTE");
            Log.e("jsonResponse", "WalletAmount" + jsonResponse);
            if(jsonResponse!=null) {
                try {
                    JSONObject jObj = new JSONObject(jsonResponse);

                    String status = jObj.getString("Status");
                    System.out.println("Service Response-->" + jsonResponse);
                    if (status.toString().equalsIgnoreCase("Success")) {


                        JSONObject waallet_amount = jObj.getJSONObject("response");

                        String walStr = waallet_amount.getString("walletAmount");
                        Log.e("String ", "Valuse" + walStr);

                        int WalletAmt= Integer.parseInt(walStr);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY, walStr);
                        Intent in = new Intent("amtReceiver");
                        getApplicationContext().sendBroadcast(in);
                        user_free_parking= Integer.parseInt(waallet_amount.getString("user_free_parking"));
                        AppPreferences.putStringIntoStore(Preference_Constants.CREDIT_CARD_STATUS,waallet_amount.getString("creditsync"));
                        int Futurebookprice = 0;
                        int creditcardSync= Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.CREDIT_CARD_STATUS).toString());
                        if (AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_BOOKING_PRICE).equalsIgnoreCase("")) {

                        }
                        else {
                            Futurebookprice = Integer.parseInt(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FUTUREBOOKING_BOOKING_PRICE));
                        }
                        if (Apply_Hourly_Charges==1)
                        {
                            if (WalletAmt<0 || Integer.parseInt(walStr) < Futurebookprice || walStr.equalsIgnoreCase("")) {
                                if (creditcardSync==0) {
                                    final Dialog Fdialog1 = new Dialog(FutureSlotActivity.this);
                                    if (Build.VERSION.SDK_INT < 16) {
                                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                                WindowManager.LayoutParams.FLAG_FULLSCREEN);

                                    }
                                    Fdialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    Fdialog1.setContentView(R.layout.pop_up_dialog);
                                    Fdialog1.setCancelable(false);
                                    Fdialog1.setCanceledOnTouchOutside(false);
                                    TextView text = (TextView) Fdialog1.findViewById(R.id.text);
                                    text.setText("There is no balance... add balance and proceed with booking.");
                                    text.setTextColor(Color.parseColor("#000000"));
                                    Fdialog1.show();
                                    TextView okay_popup = (TextView) Fdialog1.findViewById(R.id.okay_popup);
                                    okay_popup.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                            Fdialog1.dismiss();
                                            Intent in = new Intent("CurrentWalletReceiver");
                                            getApplicationContext().sendBroadcast(in);

                                        }
                                    });
                                }
                                else
                                {
                                    creditcard=1;
                                    FutureSlot();
                                }

                            } else {
                                creditcard=0;
                                FutureSlot();
                            }
                        }
                        else if (Apply_Hourly_Charges==0)
                        {
                            if (user_free_parking>0)
                            {
                                //hitWalletcheking();
                                creditcard=0;
                                FutureSlot();
                            }
                           else  {
                                if ( (WalletAmt<0 || Integer.parseInt(walStr) < Futurebookprice || walStr.equalsIgnoreCase(""))) {
                                    if (creditcardSync==0) {
                                        final Dialog Fdialog1 = new Dialog(FutureSlotActivity.this);
                                        if (Build.VERSION.SDK_INT < 16) {
                                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

                                        }
                                        Fdialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        Fdialog1.setContentView(R.layout.pop_up_dialog);
                                        Fdialog1.setCancelable(false);
                                        Fdialog1.setCanceledOnTouchOutside(false);

                                        TextView text = (TextView) Fdialog1.findViewById(R.id.text);
                                        text.setText("There is no balance.. add balance and proceed with booking.");


                                        text.setTextColor(Color.parseColor("#000000"));
                                        Fdialog1.show();
                                        TextView okay_popup = (TextView) Fdialog1.findViewById(R.id.okay_popup);
                                        okay_popup.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                finish();
                                                Fdialog1.dismiss();
                                                Intent in = new Intent("CurrentWalletReceiver");
                                                getApplicationContext().sendBroadcast(in);

                                            }
                                        });
                                    }
                                    else
                                    {
                                        creditcard=1;
                                        FutureSlot();
                                    }
                                }
                                else {
                                    creditcard=0;
                                    FutureSlot();
                                }

                            }
                        }


                   /* Intent in = new Intent("BookSuccess");
                    getActivity().getApplicationContext().sendBroadcast(in);
*/
//
                        System.out.println("success");

                    } else {
                        System.out.println("failed");
                        String Message = jObj.getString("Message");

                        Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_LONG).show();

                    }


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Please try again later.", Toast.LENGTH_LONG).show();
                    System.out.println(e.toString() + "zcx");
                }
            }else if(jsonResponse==null){
                Toast.makeText(getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            }
        }

    }


    private void FutureSlot() {

        byte[] data;
        try {

            String CityId = AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_CITY);
            String LocationId=AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_SELECTED_LOCATION);
            String login_string = "Function:FutureBooking|city_id:"+ CityId+"|location_id:"+LocationId+"|user_id:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID)+"|booking_date:"+datetimeStr+"|fcm_key:"+ AppPreferences.getStringFromStoreOne(Preference_Constants.PREF_KEY_FCMTOKEN_NEW)+"|credit_card:"+creditcard;
            System.out.println(Preference_Constants.PREF_KEY_Service + "FutureSlotparam  -->" + login_string);
            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            service_BookSlotFromServer = new WS_CallService(FutureSlotActivity.this);
            BookSlotFromServer_NVP.add(new BasicNameValuePair("WS", base64_register));
            FutureSlot_WS FutureSlot_WS_Slot_WebService = new FutureSlot_WS(FutureSlotActivity.this, BookSlotFromServer_NVP);
            FutureSlot_WS_Slot_WebService.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }




    public class FutureSlot_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        Dialog dialog;

        public FutureSlot_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new Dialog(FutureSlotActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(FutureSlotActivity.this)
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_SearchSlots = new WS_CallService(context_aact);
                jsonResponseString = service_SearchSlots.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
//            CityArrayList.clear();
            System.out.println("POST EXECUTE");
            try {
                dialog.dismiss();
            }catch (Exception e){}
//            Log.e("jsonResponse", "Login" + jsonResponse);
            if(jsonResponse!=null){
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                Log.e("If Monye","FutureSlotResponse"+jsonResponse);

                String status = jObj.getString("Status");

                if(status.equalsIgnoreCase("Success"))
                {
                    String messageResponse="";
                    if(jObj.has("Message")) {
                        messageResponse = jObj.getString("Message");

                        Log.e("messageResponse", "" + messageResponse);

                        SuccessPopup(messageResponse);
                    }
                    //Toast.makeText(getActivity().getApplicationContext(),status,Toast.LENGTH_LONG).show();
                }else
                {
                    if(jObj.has("Message")) {
                        if(jObj.getString("Message").equalsIgnoreCase("No slots available.")) {
                            Popup("Sorry! No Spots available! Booked-full!" );
                        }else if(jObj.getString("Message").equalsIgnoreCase("Insufficient amount in wallet.")) {
                            WalletPopup("" + jObj.getString("Message"));
                        }else{
                            Popup("" + jObj.getString("Message"));
                        }

                    }
                    //   Toast.makeText(getApplicationContext(),status,Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                Popup("Please try again later.");
                System.out.println(e.toString() + "zcx");
            }
            }else if(jsonResponse==null){
                Toast.makeText(getApplicationContext(),"Please check your Data/WiFi connection.",Toast.LENGTH_LONG).show();
            }

        }

    }


    public void SuccessPopup(String message){
        final Dialog dialog1 = new Dialog(FutureSlotActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView   text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(message);
        // text.setTextColor(Color.parseColor("#ffffff"));
        dialog1.show();

        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                FutureSlotActivity.this.finish();
                Intent in = new Intent("BookSuccess");
                getApplicationContext().sendBroadcast(in);
                //getActivity().finish();
            }
        });

    }

    public void WalletPopup(String message){
        final Dialog dialog1 = new Dialog(FutureSlotActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView   text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(message);
        // text.setTextColor(Color.parseColor("#ffffff"));
        dialog1.show();

        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                FutureSlotActivity.this.finish();
                Intent in = new Intent("WalletOpen");
                getApplicationContext().sendBroadcast(in);
                //getActivity().finish();
            }
        });

    }


    public void Popup(String message){
        final Dialog dialog1 = new Dialog(FutureSlotActivity.this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView   text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(message);
        // text.setTextColor(Color.parseColor("#ffffff"));
        dialog1.show();

        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();

               // FutureSlotActivity.this.finish();
            }
        });

    }



}
