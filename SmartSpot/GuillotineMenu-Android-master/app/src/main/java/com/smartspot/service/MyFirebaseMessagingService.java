package com.smartspot.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.smartspot.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.smartspot.activity.MainActivity;
import com.smartspot.notification.Config;
import com.smartspot.notification.NotificationUtils;
import com.smartspot.notification.Notificationcustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;


/**
 * Created by iyyapparajr on 8/1/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyAndroidFCMService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log data to Log Cat
       /* Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        //create notification
        createNotification(remoteMessage.getNotification().getBody());*/
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            if (remoteMessage.getNotification() != null) {
                Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            }


            Map<String, String> params = remoteMessage.getData();
            JSONObject obj = new JSONObject(params);
            parsePushJson(getApplicationContext(), obj);

        }
    }
    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }


    private void parsePushJson(Context context, JSONObject nobj) {
        try {
            Log.e("PushNotJson",nobj.toString());

            ///String alert = json.getString("alert");
            // JSONObject d = new JSONObject(alert);
            // JSONObject data = d.getJSONArray("notifications").getJSONObject(0);
           // Notificationpojo not=new Notificationpojo();

           // {receiver_id=3, type=Group Creation, sender_id=10, navid=212, sound=1, title=SoSellShop, message=dheena Has Added You To The eee Group}



String message="";
            String title="";
          /*  String sender_id="";
            String navid="";
            String title="";
            String message="";*/

                if(nobj.has("title"))
                {
                    title=nobj.getString("title");
                }

                if(nobj.has("message"))
                {
                message=nobj.getString("message");
            }


            Intent resultIntent=null;

                resultIntent = new Intent(context, MainActivity.class);
                resultIntent.putExtra("type",title);
                resultIntent.putExtra("navid",message);
                resultIntent.putExtra("fromnotification",true);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            showNotificationMessage(context, title, message, "", "", resultIntent);


           /* Intent resultIntent=null;*/
            //showNotificationMessage();
           /* if(AppPreferences.getBooleanFromStore(Preference_Constants.PREF_KEY_NOTIFICATION)) {
                resultIntent = new Intent(context, SplashActivity.class);
                resultIntent.putExtra("type",type);
                resultIntent.putExtra("navid",navid);
                resultIntent.putExtra("fromnotification",true);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            }
            else
            {

                if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_TYPE).equalsIgnoreCase("1")) {
                    resultIntent = new Intent("resellernotification");
                }
                else  if(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_TYPE).equalsIgnoreCase("2")) {
                    resultIntent = new Intent("resellernotification");
                }
                else
                {
                    resultIntent = new Intent("resellernotification");

                }
                resultIntent.putExtra("type",type);
                resultIntent.putExtra("navid",navid);
                resultIntent.putExtra("fromnotification",true);

            }

            if(type.equalsIgnoreCase(Amazonclass.type_group)) {
                showNotificationMessage(context, title, message, "", "", resultIntent);

            }
            else  if(type.equalsIgnoreCase(Amazonclass.type_Orderplaced)) {

                showNotificationMessage(context, title, message, "", "", resultIntent);

            }
            else if(type.equalsIgnoreCase(Amazonclass.type_Shipped)) {
                showNotificationMessage(context, title, message, "", "", resultIntent);

            }
            else  if(type.equalsIgnoreCase(Amazonclass.type_supplierproduct)) {
                showNotificationMessage(context, title, message, "", "", resultIntent);

            }
            else  if(type.equalsIgnoreCase(Amazonclass.type_follow)) {
                showNotificationMessage(context, title, message, "", "", resultIntent);

            }
          */ /* String title = json.getString("title");
            String image = json.getString("profile_image");
            String message = json.getString("message");*/
            // String time = data.getString("date");

           /* Log.e("Error", "JSON: " + data.toString());
            Log.e("Error", "JSON: " + message);
            Log.e("Error", "JSON: " + title);*/

         /*   if(d.has("notification_count")) {
                //JSONObject obj = response.getJSONObject("notification_count");
                Intent in=new Intent("notification");
                in.putExtra("size",d.getString("notification_count"));
                context.sendBroadcast(in);

            }*/


            /*if (json.getString("action_id").equals("1")) {
                Intent resultIntent = new Intent(context, MainActivity*//*PostDetailsActivity*//*.class);

                // Intent resultIntent = new Intent(context, ShowProfileActivity.class);
                resultIntent.putExtra("from_notification", "1");
                resultIntent.putExtra("username", json.getString("user_id_comment"));
                resultIntent.putExtra("note",true);

                showNotificationMessage(context, title, message, image, "", resultIntent);
            } else {
                Intent resultIntent = new Intent(context, MainActivity*//*PostDetailsActivity*//*.class);
                resultIntent.putExtra("from_notification", "1");
                resultIntent.putExtra("note",true);
                resultIntent.putExtra("post_id", json.getString("post_id"));
                showNotificationMessage(context, title, message, image, "", resultIntent);
            }*/

        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }
   /* private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }*/

    /**
     * Showing notification with text and image
     */
    /*private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }



    private void showNotificationMessageWithBigImage(Context context, String title, String message, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, intent, imageUrl);
    }*/
    private void showNotificationMessage(Context context, String title, String message,
                                         String image, String time, Intent intent) {
        Intent parseIntent;


      //  com.app.graffitierapp.notification.helper.NotificationUtils notificationUtils = new com.app.graffitierapp.notification.helper.NotificationUtils(context);
        Notificationcustom notificationUtils= new Notificationcustom(context);
       // intent.putExtras(intent.getExtras());



        /*prefs = context.getSharedPreferences("UserPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        if(!prefs.getString("username","null").equals("null")&&!prefs.getString("username","").equals(""))*/

       /* if (!TextUtils.isEmpty(image)) {*/

           // if (ToolsUtils.cloud().url().generate(image) != null && ToolsUtils.cloud().url().generate(image).length() > 4 && Patterns.WEB_URL.matcher(ToolsUtils.cloud().url().generate(image)).matches()) {

                Bitmap bitmap =null/* getBitmapFromURL(ToolsUtils.cloud().url().generate(image))*/;

                if (bitmap != null) {
                    notificationUtils.CustomNotification(bitmap, title, message, time, intent);

                  /*  remoteViews.setImageViewBitmap(R.id.notification_image,bitmap);
                    NotificationManager notificationmanager = (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
                    // Build Notification with Notification Manager
                    notificationmanager.notify(AppConfig.NOTIFICATION_ID, notification);*/
                    //    showBigNotification(bitmap, mBuilder, icon, title, message, resultPendingIntent, alarmSound);
                } else {
                    notificationUtils.CustomNotification(bitmap, title, message, time, intent);

                    // showSmallNotification(mBuilder, icon, title, message, resultPendingIntent, alarmSound);
                  /*  NotificationManager notificationmanager = (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
                    // Build Notification with Notification Manager
                    notificationmanager.notify(AppConfig.NOTIFICATION_ID, notification);*/
                }
           // }
       // }


    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}