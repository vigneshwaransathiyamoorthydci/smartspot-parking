package com.smartspot.fragment;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.android.gms.nearby.messages.internal.Update;
import com.google.android.gms.vision.barcode.Barcode;
import com.smartspot.R;
import com.smartspot.activity.LoginActivity;
import com.smartspot.activity.LoginVerifyOtp;
import com.smartspot.activity.MainActivity;
import com.smartspot.service.WebserviceUrl;
import com.smartspot.sharedpreferences.AppPreferences;
import com.smartspot.sharedpreferences.Preference_Constants;
import com.smartspot.sharedpreferences.SmartSpaceApplication;
import com.smartspot.widget.ButtonInterface;
import com.smartspot.widget.RoundedImageView;
import com.smartspot.widget.WS_CallService;
import com.smartspot.widget.volleyrequestforstring;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

/**
 * Created by iyyapparajr on 11/8/2017.
 */

public class ProfileFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    TextView username,email_address,phonenumber, carno, save_changes;
    EditText username_edit,email_edit,phone_edit, carno_edit;
    RoundedImageView profile;
    String profilepath;
    String sendProfileStr="";
    WS_CallService service_Profile;
    ArrayList<NameValuePair> Profile_NVP=new ArrayList<>();
    Load_Profile_WS profileDetails_WS;

    WS_CallService service_Login;
    ArrayList<NameValuePair> login_NVP = new ArrayList<>();
    Load_Login_WS Login_Hitservice;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static int RESULT_LOAD_IMAGE = 1;
    BroadcastReceiver accountReceiver;
    private OnFragmentInteractionListener mListener;
    volleyrequestforstring request;
    HashMap<String,String> updateParam=new HashMap<>();

    public ProfileFragment() {
        // Required empty public constructor
    }
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.profile_fragment, container, false);

        username=(TextView)view.findViewById(R.id.username);
        email_address=(TextView)view.findViewById(R.id.email_address);
        phonenumber=(TextView)view.findViewById(R.id.phonenumber);
        carno=(TextView)view.findViewById(R.id.carno);
        save_changes=(TextView)view.findViewById(R.id.save_changes);

        username_edit=(EditText)view.findViewById(R.id.usernameedit);
        email_edit=(EditText)view.findViewById(R.id.email_edit);
        phone_edit=(EditText)view.findViewById(R.id.phone_edit);
        carno_edit=(EditText)view.findViewById(R.id.carno_edit);

        profile=(RoundedImageView) view.findViewById(R.id.profile);
        profile.setEnabled(false);
        AppPreferences.putStringIntoStore(Preference_Constants.CURRENT_FRAGMENT,"4");
        if(isInternetOn())
        hitProfileService();

     //   username.setTextColor(Color.parseColor("#ffffff"));
   ////     email_address.setTextColor(Color.parseColor("#ffffff"));
   ///     phonenumber.setTextColor(Color.parseColor("#ffffff"));


//        email_address.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_EMAIL));
//        username.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME));
//        phonenumber.setText(AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_MOBILE));

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent i = new Intent(
                        Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);*/
                cameraPopup();
            }
        });

        save_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if(email_edit.getText().toString().trim().equalsIgnoreCase("")|| email_edit.getText().toString().trim().isEmpty()){
                 //   UpdateValue();
                //}else {
                    if (Validation()) {
                        if(isInternetOn())
                        UpdateValue();
                    }
                //}

            }
        });


        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                save_changes,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.0f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();


        accountReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();
                if (bundle.getString("edit").equalsIgnoreCase("yes")) {
                       editOptions();
                }

            }
        };
        IntentFilter intent2=new IntentFilter("Profile");
        getActivity().registerReceiver(accountReceiver, intent2);

        return view;
    }

    public void editOptions(){
        username.setVisibility(View.GONE);
        email_address.setVisibility(View.GONE);
        phonenumber.setVisibility(View.GONE);
        carno.setVisibility(View.GONE);
        username_edit.setVisibility(View.VISIBLE);
        email_edit.setVisibility(View.VISIBLE);
        phone_edit.setVisibility(View.VISIBLE);
        carno_edit.setVisibility(View.VISIBLE);
        save_changes.setVisibility(View.VISIBLE);
        profile.setEnabled(true);
    }

    public void afterUpdate(){
        username.setVisibility(View.VISIBLE);
        email_address.setVisibility(View.VISIBLE);
        phonenumber.setVisibility(View.VISIBLE);
        carno.setVisibility(View.VISIBLE);
        username_edit.setVisibility(View.GONE);
        email_edit.setVisibility(View.GONE);
        phone_edit.setVisibility(View.GONE);
        carno_edit.setVisibility(View.GONE);
        save_changes.setVisibility(View.GONE);
        profile.setEnabled(false);
    }

    private boolean isValidEmaillId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public boolean Validation(){
        boolean check=true;
        int v=0;
        String error="";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailString = email_edit.getText().toString().trim();
        /*if(email_edit.getText().length()==0 || email_edit.getText().toString().equalsIgnoreCase("") || email_edit.getText().toString().isEmpty()){
            error="Enter a Valid Email";
            check=false;
            Popup(error);
            return check;

        }else*/
        if(username_edit.getText().length()==0 || username_edit.getText().toString().equalsIgnoreCase("") || username_edit.getText().toString().isEmpty()){
            error="Enter a Valid Username";
            check=false;
            Popup(error);
            return check;
        }
        if (!isValidEmaillId(emailString)){
            error="Enter a Valid Email";
            check=false;
            Popup(error);
            return check;
        }
        if(carno_edit.getText().length()==0 || carno_edit.getText().toString().equalsIgnoreCase("") || carno_edit.getText().toString().isEmpty()){
            error="Enter a Valid Car Number";
            check=false;
            Popup(error);
            return check;
        }

        return check;
    }

    private void hitProfileService() {

        Profile_NVP=new ArrayList<>();
        byte[] data;
        try {
//               String login_string ="Function:Login|FbID:226209007875561|MobileNumber:9944550517|EmailID:dhee19naa@gmail.com|LoginType:2";
            String login_string = "Function:Profile|user_id:"+AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_ID);
            System.out.println("Fucntionals login request-->" + login_string);

            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

            service_Profile = new WS_CallService(getActivity());
            Profile_NVP.add(new BasicNameValuePair("WS", base64_register));
            profileDetails_WS = new Load_Profile_WS(getActivity(), Profile_NVP);
            profileDetails_WS.execute();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public class Load_Profile_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
//        ProgressDialog dialog;

        Dialog dialog;
        Context context_aact;

        String user_wallet_money,user_device_type,user_fcm_key,user_id,Noofavailablefreebooking;
        String user_nameService,user_emailService,user_mobile_numberService
                ,user_statusService,user_last_loginService,user_last_logoutService,user_profile_imageService,user_device_idService,user_car_number;
        public Load_Profile_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog = new ProgressDialog(getActivity());
//            dialog.setMessage("Please wait...");
//            dialog.setCancelable(false);
//            dialog.setIndeterminate(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();


            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
//            dialog.setTitle("Title...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

//            isInternetOn();
            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Profile = new WS_CallService(context_aact);
                jsonResponseString = service_Profile.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            try {
                dialog.dismiss();
            } catch (Exception e) {

            }

            if(jsonResponse!=null) {
                System.out.println("POST EXECUTE");
                Log.e("jsonResponse", "userdetail" + jsonResponse);
                try {
                    JSONObject jObj = new JSONObject(jsonResponse);

                    String status = jObj.getString("Status");
                    System.out.println("Service Response-->" + jsonResponse);


                    if (status.toString().equalsIgnoreCase("Success")) {

                        user_nameService = jObj.getJSONArray("response").getJSONObject(0).getString("user_name");
                        user_emailService = jObj.getJSONArray("response").getJSONObject(0).getString("user_email");
                        user_mobile_numberService = jObj.getJSONArray("response").getJSONObject(0).getString("user_mobile_number");
                        user_statusService = jObj.getJSONArray("response").getJSONObject(0).getString("user_status");
                        user_last_loginService = jObj.getJSONArray("response").getJSONObject(0).getString("user_last_login");
                        user_last_logoutService = jObj.getJSONArray("response").getJSONObject(0).getString("user_last_logout");
                        user_profile_imageService = jObj.getJSONArray("response").getJSONObject(0).getString("user_profile_image");
                        user_device_idService = jObj.getJSONArray("response").getJSONObject(0).getString("user_device_id");
                        user_car_number = jObj.getJSONArray("response").getJSONObject(0).getString("user_car_number");

                        user_id = jObj.getJSONArray("response").getJSONObject(0).getString("user_id");
                        user_fcm_key = jObj.getJSONArray("response").getJSONObject(0).getString("user_fcm_key");
                        user_device_type = jObj.getJSONArray("response").getJSONObject(0).getString("user_device_type");
                        user_wallet_money = jObj.getJSONArray("response").getJSONObject(0).getString("user_wallet_money");
                        Noofavailablefreebooking = jObj.getJSONArray("response").getJSONObject(0).getString("Noofavailablefreebooking");

                        Picasso.with(getActivity().getApplicationContext()).load(user_profile_imageService).placeholder(R.drawable.placeholder).into(profile);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY, user_wallet_money);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_EMAIL, user_emailService);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME, user_nameService);
                        Log.e("Service", "webAmount" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));

                        username.setText(user_nameService);
                        email_address.setText(user_emailService);
                        phonenumber.setText(user_mobile_numberService);
                        username_edit.setText(user_nameService);
                        email_edit.setText(user_emailService);
                        phone_edit.setText(user_mobile_numberService);
                        carno.setText(user_car_number);
                        carno_edit.setText(user_car_number);


                        Intent in = new Intent("ProfileOn");
                        in.putExtra("edit", "yes");
                        getActivity().getApplicationContext().sendBroadcast(in);
                        //  AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME,user_nameService);
//                    String Message = jObj.getString("Message");

//                    Toast.makeText(getActivity(),Message,Toast.LENGTH_LONG).show();


                        System.out.println("success");
                    } else {
                        System.out.println("failed");
                        String Message = jObj.getString("Message");

                        Toast.makeText(getActivity().getApplicationContext(), Message, Toast.LENGTH_LONG).show();
                    }


                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please try again later.", Toast.LENGTH_LONG).show();
                    //      System.out.println(e.toString() + "zcx");
                    Log.e("Exception", "->" + e.toString());
                }
            }else{
                Toast.makeText(getActivity().getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            }

        }

    }

    public void UpdateValue(){

        if(profilepath!=null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            BitmapFactory.decodeFile(profilepath).compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] b = baos.toByteArray();
            sendProfileStr = converintobase64string(b);
        }

         login_NVP= new ArrayList<>();
            byte[] data;
            try {
                String login_string = "Function:EditProfile|Mobile Number:" + phone_edit.getText().toString().trim() + "|Name:"+username_edit.getText().toString()+"|Email address:"+email_edit.getText().toString()+"|Profile Image:"+sendProfileStr+"|Car Number:"+carno_edit.getText().toString();
                System.out.println("Fucntionals Update request-->" + login_string);

                Log.e("Login", "Service" + login_string);
                data = login_string.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

                service_Login = new WS_CallService(getActivity().getApplicationContext());
                login_NVP.add(new BasicNameValuePair("WS", base64_register));
                Login_Hitservice = new Load_Login_WS(getActivity(), login_NVP);
                Login_Hitservice.execute();

            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }




    }

    public class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;
        String user_wallet_money,user_device_type,user_fcm_key,user_id,Noofavailablefreebooking;
        String user_nameService,user_emailService,user_mobile_numberService
                ,user_statusService,user_last_loginService,user_last_logoutService,user_profile_imageService,user_device_idService,user_car_number;


        Dialog dialog;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            ImageView image = (ImageView) dialog.findViewById(R.id.custom_progress);

            Glide.with(getActivity())
                    .load(R.drawable.loading)
                    .asGif()
                    .placeholder(R.drawable.loading)
                    .crossFade()
                    .into(image);
            dialog.show();

            System.out.println("PRE EXECUTE" + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.mainurl);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            System.out.println("POST EXECUTE");

            try {
                dialog.dismiss();
            } catch (Exception e) {

            }


            if(jsonResponse!=null) {
                Log.e("jsonResponse", "Update" + jsonResponse);
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);

                    String typeResponse = "";
                    if (jsonObject.has("Status"))
                        typeResponse = jsonObject.getString("Status");

                    if (typeResponse.equalsIgnoreCase("success")) {
                        JSONObject userDetailsObject = null;
                        if (jsonObject.has("UserDetails"))
                            userDetailsObject = jsonObject.getJSONObject("UserDetails");


                        user_nameService = userDetailsObject.getString("user_name");
                        user_emailService = userDetailsObject.getString("user_email");
                        user_mobile_numberService = userDetailsObject.getString("user_mobile_number");
                        user_statusService = userDetailsObject.getString("user_status");
                        user_last_loginService = userDetailsObject.getString("user_last_login");
                        user_last_logoutService = userDetailsObject.getString("user_last_logout");
                        user_profile_imageService = userDetailsObject.getString("user_profile_image");
                        user_device_idService = userDetailsObject.getString("user_device_id");
                        user_car_number = userDetailsObject.getString("user_car_number");

                        user_id = userDetailsObject.getString("user_id");
                        user_fcm_key = userDetailsObject.getString("user_fcm_key");
                        user_device_type = userDetailsObject.getString("user_device_type");
                        user_wallet_money = userDetailsObject.getString("user_wallet_money");
                        //  Noofavailablefreebooking=userDetailsObject.getString("Noofavailablefreebooking");

                        Picasso.with(getActivity().getApplicationContext()).load(user_profile_imageService).placeholder(R.drawable.placeholder).into(profile);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY, user_wallet_money);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_EMAIL, user_emailService);
                        AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_USER_FIRST_NAME, user_nameService);
                        Log.e("Service", "webAmount" + AppPreferences.getStringFromStore(Preference_Constants.PREF_KEY_USER_WALLET_MONEY));

                        username.setText(user_nameService);
                        email_address.setText(user_emailService);
                        phonenumber.setText(user_mobile_numberService);
                        username_edit.setText(user_nameService);
                        email_edit.setText(user_emailService);
                        phone_edit.setText(user_mobile_numberService);
                        carno.setText(user_car_number);
                        carno_edit.setText(user_car_number);


                        Toast.makeText(getActivity().getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                        afterUpdate();
                        Intent in = new Intent("ProfileOn");
                        in.putExtra("edit", "yes");
                        getActivity().getApplicationContext().sendBroadcast(in);

//                    hitProfileService();
                    }



                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please try again later.", Toast.LENGTH_LONG).show();
                    System.out.println(e.toString() + "zcx");
                }
            }else{
                Toast.makeText(getActivity().getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_LONG).show();
            }

        }

    }


    public void UpdateValueVolley()
    {
        //isInternetOn();
        final ProgressDialog dia=new ProgressDialog(getActivity());
        dia.setMessage("Loading...");
        dia.setCancelable(false);
        dia.show();

        byte[] data;

        try {
            String login_string = "Function:EditProfile|Mobile Number:" + phone_edit.getText().toString().trim() + "|Name:"+username_edit.getText().toString()+"|Email address:"+email_edit.getText().toString()+"|Profile Image:"+sendProfileStr+"|Car Number:";
            System.out.println("Fucntionals Update request-->" + login_string);

            Log.e("Login", "Service" + login_string);
            data = login_string.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);

           updateParam.clear();
           updateParam.put("WS",base64_register);

        Log.e("UpdateParam","->"+updateParam.toString());

        }catch (Exception e){
            Log.e("Exception","->"+e.toString());
        }

        request  = new volleyrequestforstring(Request.Method.POST,
                WebserviceUrl.mainurl,updateParam,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String result) {
                        Log.d("UpdateResponse", result.toString());
                        dia.dismiss();

                        if(result!=null) {
                            try {

                                JSONObject jsonObject= new JSONObject(result);

                                String typeResponse="";
                                if(jsonObject.has("Status"))
                                    typeResponse=jsonObject.getString("Status");

                                if(typeResponse.equalsIgnoreCase("success")){
                                    Toast.makeText(getActivity().getApplicationContext(),"Update Successfully",Toast.LENGTH_LONG).show();
                                    afterUpdate();
                                    Intent in = new Intent("ProfileOn");
                                    in.putExtra("edit","yes");
                                    getActivity().getApplicationContext().sendBroadcast(in);

                                    hitProfileService();
                                }


                            } catch (Exception e) {
                                Log.e("ex", e.toString());
                                //  validatePopup("Error:"+e.toString(),"");

                            }

                        }else if(result==null){
                            //validatePopup("Something went wrong. Please try again","");

                        }

                        // pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                dia.dismiss();

                Log.e("Exception",""+ error.toString());
                // VolleyLog.d(TAG, "Error: " + error.getMessage());
                //pDialog.hide();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> headers = new HashMap<String, String>();
                String username="ck_5360f98f4d5117380e025cbfc997631349cf1d8a";
                String password="cs_d308bd539a20479501975e9dfda93f03c6fdb9d0";
                String auth =new String(Base64.encode(( username + ":" + password).getBytes(),Base64.URL_SAFE|Base64.NO_WRAP));
                // String auth =new String(Base64.encode(( username + ":" + password).getBytes(),Base64.URL_SAFE|Base64.NO_WRAP));

                headers.put("WSH","U2VjcmV0OlBhcmtpbmdBcHB8UGFzc3dvcmQ6RkJGLklTSEpOLlhraw==");

                for (Map.Entry<String, String> entry :updateParam.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    headers.put(key,value);
                   /* if(entry.getKey().equalsIgnoreCase("page")){
                        entry.setValue(""+pagecount);
                    }*/
                    // ...
                }

                //headers.put("Content-Type", "application/json");
                // headers.put("apiKey", "xxxxxxxxxxxxxxx");
                return headers;
                // return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(15000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        SmartSpaceApplication.getInstance().addToRequestQueue(request);
        //  SoSellApplication.getInstance().addToRequestQueue(jsonObjReq, "String");
    }


    public String converintobase64string(byte[] bm) {
        byte[] b = bm;
        Bitmap bit = BitmapFactory.decodeByteArray(b, 0, b.length);
        Bitmap scaled = Bitmap.createScaledBitmap(bit, 480, 825, false);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        scaled.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        byte[] b2 = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b2, Base64.DEFAULT);
        return encodedImage;
    }

    public void cameraPopup()
    {
        try {
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.cameradialog);
            dialog.setCancelable(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            TextView titletext = (TextView) dialog.findViewById(R.id.barottastext);
            TextView contenttext = (TextView) dialog.findViewById(R.id.contenttext);
            TextView negativebuttontext = (TextView) dialog.findViewById(R.id.negativebutton);
            TextView postivebuttontext = (TextView) dialog.findViewById(R.id.postivebutton);

            negativebuttontext.setVisibility(View.VISIBLE);
            postivebuttontext.setText("Camera");
            negativebuttontext.setText("Gallery");

            contenttext.setText("Take picture from");

            //   negativebuttontext.setText(neagtive);
            dialog.show();

            postivebuttontext.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    dialog.dismiss();

                    Intent intent = new Intent(
                            MediaStore.ACTION_IMAGE_CAPTURE);

                    int filename= (int) System.currentTimeMillis();

                    camertakenpath=""+filename+".jpg";

                    File f = new File(Environment
                            .getExternalStorageDirectory(), camertakenpath);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(f));

                    startActivityForResult(intent,
                            2);
                }

            });
            negativebuttontext.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();

                    Intent getIntent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    getIntent.setType("image/*");
                    startActivityForResult(getIntent, 1);
                }

            });
        }catch(Exception e){
            Log.e("Ex",""+e.toString());
        }

    }
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null) {
            try {
                Uri chosenImageUri = data.getData();
                bm =  MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), chosenImageUri);
                //   gal_image.setImageBitmap(bm);
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(chosenImageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                ActionPicturePath(picturePath);
                cursor.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    String camertakenpath;
    private void onCaptureImageResult(Intent data) {
/*
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        gal_image.setImageBitmap(thumbnail);
*/
        int filename= (int) System.currentTimeMillis();
        File f = new File(Environment.getExternalStorageDirectory()
                .toString());
        for (File temp : f.listFiles()) {
            if (temp.getName().equals(camertakenpath)) {
                f = temp;
                break;
            }
        }

        if (!f.exists()) {

            Toast.makeText(getActivity().getBaseContext(),

                    "Error while capturing image.", Toast.LENGTH_LONG)

                    .show();

        }

        try {


            Bitmap bitmap1 = BitmapFactory.decodeFile(f.getAbsolutePath());
            //Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

            String picturePath = f.getAbsolutePath();

            //gal_image.setImageBitmap(bitmap1);
            ActionPicturePath(picturePath);


            bitmap1 = Bitmap.createScaledBitmap(bitmap1, 400, 400, true);
                        /*ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        BitmapFactory.decodeFile(picturePath).compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] b = baos.toByteArray();

                        profileimage = converintobase64string(b);*/
            int rotate = 0;
            try {
                ExifInterface exif = new ExifInterface(f.getAbsolutePath());
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Matrix matrix = new Matrix();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void ActionPicturePath(String picturePath){

        if(!picturePath.equalsIgnoreCase("")) {
            profilepath=picturePath;
            Log.e("activityResultpath",profilepath);

        File file = new File(profilepath);
        long length = file.length()/1024;
        Log.e("imagesize","->"+length);
        if(length<=1024) {
            profile.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }else{
            Popup("Please select below size 1MB ");
        }

        }
       /* try {
            if (picturePath != ""){}
                profile.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }catch (Exception e){

        }*/

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if(resultCode!=0)
        {
            String picturePath="";

            if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
            }

            if(!picturePath.equalsIgnoreCase("")) {
                profilepath=picturePath;
                Log.e("activityResultpath",profilepath);
                //AppPreferences.putStringIntoStore(Preference_Constants.PREF_KEY_SELECT_PROFILE_PATH,picturePath);

            }
            //try {
            File file = new File(profilepath);
            long length = file.length()/1024;
            Log.e("imagesize","->"+length);
            if(length<=1024) {
                profile.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            }else{
                Popup("Please select below size 1MB ");
            }

        }
        else
        {
            //    drawView.setVisibility(View.VISIBLE);
        }
        */

        if(resultCode!=0 ) {
            if (requestCode == 1) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == 2) {
                onCaptureImageResult(data);
            }
        }
    }

    public void Popup(String message){
        final Dialog dialog1 = new Dialog(getActivity());
        if (Build.VERSION.SDK_INT < 16) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.pop_up_dialog);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);

        TextView   text=(TextView)dialog1.findViewById(R.id.text);
        text.setText(message);
        // text.setTextColor(Color.parseColor("#ffffff"));
        dialog1.show();

        TextView  okay_popup=(TextView)dialog1.findViewById(R.id.okay_popup);
        okay_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
                //getActivity().finish();
            }
        });

    }

    public final boolean isInternetOn() {

        ConnectivityManager connec =
                (ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

//            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(getActivity().getApplicationContext(), "Please check your Data/WiFi connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return false;
    }
}

